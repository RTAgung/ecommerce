package com.rama.ecommerce.repository

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.repository.WishlistRepository
import com.rama.ecommerce.data.repository.WishlistRepositoryImpl
import com.rama.ecommerce.data.source.local.room.dao.WishlistDao
import com.rama.ecommerce.utils.dummy.dummyWishlistList
import com.rama.ecommerce.utils.extension.toWishlist
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class WishlistRepositoryTest {

    private lateinit var wishlistRepository: WishlistRepository
    private lateinit var wishlistDao: WishlistDao

    @Before
    fun setup() {
        wishlistDao = mock()
        wishlistRepository = WishlistRepositoryImpl(
            wishlistDao
        )
    }

    @Test
    fun getWishlistData_thenReturnWishlistData() = runTest {
        whenever(wishlistDao.getData()).thenReturn(flowOf(dummyWishlistList))
        val actualData = wishlistRepository.getWishlistData().first()
        val expectedData = dummyWishlistList.map { it.toWishlist() }.reversed()

        assertEquals(expectedData, actualData)
    }

    @Test
    fun getWishlistDataSize_thenReturnWishlistSize() = runTest {
        whenever(wishlistDao.getDataSize()).thenReturn(flowOf(dummyWishlistList.size))
        val actualData = wishlistRepository.getWishlistDataSize().first()
        val expectedData = dummyWishlistList.size

        assertEquals(expectedData, actualData)
    }

    @Test
    fun checkExistWishlistData_thenReturnTrue() = runTest {
        whenever(wishlistDao.checkExistData("id")).thenReturn(true)
        val actualData = wishlistRepository.checkExistWishlistData("id")

        assertTrue(actualData)
    }

    @Test
    fun insertWishlist_thenVerify() = runTest {
        wishlistRepository.insertWishlist(dummyWishlistList[0].toWishlist())
        verify(wishlistDao).insert(dummyWishlistList[0])
    }

    @Test
    fun deleteWishlist_thenVerify() = runTest {
        wishlistRepository.deleteWishlist(dummyWishlistList[0].toWishlist())
        verify(wishlistDao).delete(dummyWishlistList[0])
    }
}
