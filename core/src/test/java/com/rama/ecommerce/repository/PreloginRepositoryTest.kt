package com.rama.ecommerce.repository

import app.cash.turbine.test
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.firebase.AppFirebaseMessaging
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.PreloginRepository
import com.rama.ecommerce.data.repository.PreloginRepositoryImpl
import com.rama.ecommerce.data.source.local.preferences.AppPreferences
import com.rama.ecommerce.data.source.local.room.dao.CartDao
import com.rama.ecommerce.data.source.local.room.dao.NotificationDao
import com.rama.ecommerce.data.source.local.room.dao.WishlistDao
import com.rama.ecommerce.data.source.remote.request.AuthRequest
import com.rama.ecommerce.data.source.remote.service.ApiService
import com.rama.ecommerce.utils.dummy.dummyUserPreferences
import com.rama.ecommerce.utils.dummy.response.dummyLoginResponse
import com.rama.ecommerce.utils.dummy.response.dummyProfileResponse
import com.rama.ecommerce.utils.dummy.response.dummyRegisterResponse
import com.rama.ecommerce.utils.extension.toUser
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class PreloginRepositoryTest {

    private lateinit var preloginRepository: PreloginRepository
    private lateinit var appPreferences: AppPreferences
    private lateinit var apiService: ApiService
    private lateinit var wishlistDao: WishlistDao
    private lateinit var cartDao: CartDao
    private lateinit var notificationDao: NotificationDao
    private lateinit var firebaseMessaging: AppFirebaseMessaging

    @Before
    fun setup() {
        appPreferences = mock()
        apiService = mock()
        wishlistDao = mock()
        cartDao = mock()
        notificationDao = mock()
        firebaseMessaging = mock()
        preloginRepository = PreloginRepositoryImpl(
            appPreferences,
            apiService,
            wishlistDao,
            cartDao,
            notificationDao,
            firebaseMessaging
        )
    }

    @Test
    fun setFirstTimeRunAppToFalse_thenVerify() = runTest {
        preloginRepository.setFirstTimeRunApp(false)
        verify(appPreferences).setFirstTimeRunApp(false)
    }

    @Test
    fun getFirstTimeRunApp_thenReturnFalse() = runTest {
        whenever(appPreferences.getFirstTimeRunApp()).thenReturn(flowOf(false))
        val actualData = preloginRepository.getFirstTimeRunApp().first()

        assertFalse(actualData)
    }

    @Test
    fun doSuccessRegister_thenReturnTrue() = runTest {
        whenever(firebaseMessaging.getToken()).thenReturn("token")
        whenever(apiService.register(AuthRequest("email", "password", "token")))
            .thenReturn(dummyRegisterResponse)

        preloginRepository.register("email", "password").test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(ResultState.Success(true), awaitItem())
            awaitComplete()
        }

        verify(appPreferences).setUserDataSession(dummyRegisterResponse.data!!.toUser())
        verify(appPreferences).setUserAuthorization(true)
        verify(firebaseMessaging).subscribeToTopic("promo")
    }

    @Test
    fun doFailedRegister_thenReturnError() = runTest {
        whenever(firebaseMessaging.getToken()).thenReturn("token")
        whenever(apiService.register(AuthRequest("email", "password", "token")))
            .thenReturn(dummyRegisterResponse.copy(data = null))

        preloginRepository.register("email", "password").test {
            assertEquals(ResultState.Loading, awaitItem())
            assertTrue(awaitItem() is ResultState.Error)
            awaitComplete()
        }
    }

    @Test
    fun doSuccessLogin_thenReturnTrue() = runTest {
        whenever(firebaseMessaging.getToken()).thenReturn("token")
        whenever(apiService.login(AuthRequest("email", "password", "token")))
            .thenReturn(dummyLoginResponse)

        preloginRepository.login("email", "password").test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(ResultState.Success(true), awaitItem())
            awaitComplete()
        }

        verify(appPreferences).setUserDataSession(dummyLoginResponse.data!!.toUser())
        verify(appPreferences).setUserAuthorization(true)
        verify(firebaseMessaging).subscribeToTopic("promo")
    }

    @Test
    fun doFailedLogin_thenReturnError() = runTest {
        whenever(firebaseMessaging.getToken()).thenReturn("token")
        whenever(apiService.login(AuthRequest("email", "password", "token")))
            .thenReturn(dummyLoginResponse.copy(data = null))

        preloginRepository.login("email", "password").test {
            assertEquals(ResultState.Loading, awaitItem())
            assertTrue(awaitItem() is ResultState.Error)
            awaitComplete()
        }
    }

    @Test
    fun doLogout_thenVerify() = runTest {
        preloginRepository.logout()
        verify(wishlistDao).clearTable()
        verify(cartDao).clearTable()
        verify(notificationDao).clearTable()
        verify(firebaseMessaging).unsubscribeFromTopic("promo")
        verify(appPreferences).clearAllDataSession()
    }

    @Test
    fun getUserDataSession_thenReturnUserData() = runTest {
        whenever(appPreferences.getUserDataSession()).thenReturn(flowOf(dummyUserPreferences))
        val actualData = preloginRepository.getUserDataSession().first()

        assertEquals(dummyUserPreferences, actualData)
    }

    @Test
    fun checkUserAuthorization_thenReturnTrue() = runTest {
        whenever(appPreferences.checkUserAuthorization()).thenReturn(flowOf(true))
        val actualData = preloginRepository.checkUserAuthorization().first()

        assertEquals(true, actualData)
    }

    @Test
    fun doSuccessUploadProfile_thenReturnSuccessTrue() = runTest {
        val userNameRequestBody = "name".toRequestBody("text/plain".toMediaType())
        whenever(
            apiService.profile(userNameRequestBody, null)
        ).thenReturn(dummyProfileResponse)

        preloginRepository.uploadProfile(userNameRequestBody, null).test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(ResultState.Success(true), awaitItem())
            awaitComplete()
        }

        verify(appPreferences).setUserDataSession(dummyProfileResponse.data!!.toUser())
    }

    @Test
    fun doFailedUploadProfile_thenReturnError() = runTest {
        val userNameRequestBody = "name".toRequestBody("text/plain".toMediaType())
        whenever(
            apiService.profile(userNameRequestBody, null)
        ).thenReturn(dummyProfileResponse.copy(data = null))

        preloginRepository.uploadProfile(userNameRequestBody, null).test {
            assertEquals(ResultState.Loading, awaitItem())
            assertTrue(awaitItem() is ResultState.Error)
            awaitComplete()
        }
    }

    @Test
    fun setAppTheme_thenVerify() = runTest {
        preloginRepository.setAppTheme(true)
        verify(appPreferences).setAppTheme(true)
    }

    @Test
    fun getAppTheme_thenReturnTrue() = runTest {
        whenever(appPreferences.getAppTheme()).thenReturn(flowOf(true))
        val actualData = preloginRepository.getAppTheme().first()

        assertEquals(true, actualData)
    }

    @Test
    fun setAppLanguage_thenVerify() = runTest {
        preloginRepository.setAppLanguage("id")
        verify(appPreferences).setAppLanguage("id")
    }

    @Test
    fun getAppLanguage_thenReturnIdLang() = runTest {
        whenever(appPreferences.getAppLanguage()).thenReturn(flowOf("id"))
        val actualData = preloginRepository.getAppLanguage().first()

        assertEquals("id", actualData)
    }
}
