package com.rama.ecommerce.repository

import app.cash.turbine.test
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.FulfillmentRepository
import com.rama.ecommerce.data.repository.FulfillmentRepositoryImpl
import com.rama.ecommerce.data.source.remote.request.FulfillmentRequest
import com.rama.ecommerce.data.source.remote.request.RatingRequest
import com.rama.ecommerce.data.source.remote.service.ApiService
import com.rama.ecommerce.utils.dummy.dummyCartList
import com.rama.ecommerce.utils.dummy.response.dummyFulfillmentResponse
import com.rama.ecommerce.utils.dummy.response.dummyRatingResponse
import com.rama.ecommerce.utils.dummy.response.dummyTransactionResponse
import com.rama.ecommerce.utils.extension.toCart
import com.rama.ecommerce.utils.extension.toFulfillment
import com.rama.ecommerce.utils.extension.toFulfillmentRequestItem
import com.rama.ecommerce.utils.extension.toTransaction
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class FulfillmentRepositoryTest {

    private lateinit var fulfillmentRepository: FulfillmentRepository
    private lateinit var apiService: ApiService

    @Before
    fun setup() {
        apiService = mock()
        fulfillmentRepository = FulfillmentRepositoryImpl(
            apiService
        )
    }

    @Test
    fun doSuccessFulfillment_thenReturnFulfillmentData() = runTest {
        val payment = "Bank BCA"
        val cartList = dummyCartList.map { it.toCart() }
        val fulfillmentItem = cartList.map { it.toFulfillmentRequestItem() }

        whenever(apiService.fulfillment(FulfillmentRequest(payment, fulfillmentItem)))
            .thenReturn(dummyFulfillmentResponse)

        fulfillmentRepository.fulfillment(payment, cartList).test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(
                ResultState.Success(dummyFulfillmentResponse.data!!.toFulfillment()),
                awaitItem()
            )
            awaitComplete()
        }
    }

    @Test
    fun doFailedFulfillment_thenReturnError() = runTest {
        val payment = "Bank BCA"
        val cartList = dummyCartList.map { it.toCart() }
        val fulfillmentItem = cartList.map { it.toFulfillmentRequestItem() }

        whenever(apiService.fulfillment(FulfillmentRequest(payment, fulfillmentItem)))
            .thenReturn(dummyFulfillmentResponse.copy(data = null))

        fulfillmentRepository.fulfillment(payment, cartList).test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(
                "No data",
                (awaitItem() as ResultState.Error).error.message
            )
            awaitComplete()
        }
    }

    @Test
    fun postSuccessRating_thenReturnTrue() = runTest {
        val invoiceId = "invoiceId"
        val rating = 3
        val review = "review"

        whenever(apiService.rating(RatingRequest(invoiceId, rating, review)))
            .thenReturn(dummyRatingResponse)

        fulfillmentRepository.rating(invoiceId, rating, review).test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(ResultState.Success(true), awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun postFailedRating_thenReturnError() = runTest {
        val invoiceId = "invoiceId"
        val rating = 3
        val review = "review"

        whenever(apiService.rating(RatingRequest(invoiceId, rating, review)))
            .thenReturn(dummyRatingResponse.copy(code = 404))

        fulfillmentRepository.rating(invoiceId, rating, review).test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(
                "Failed",
                (awaitItem() as ResultState.Error).error.message
            )
            awaitComplete()
        }
    }

    @Test
    fun getTransaction_thenReturnTransactionData() = runTest {
        whenever(apiService.transaction())
            .thenReturn(dummyTransactionResponse)

        fulfillmentRepository.getTransaction().test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(
                ResultState.Success(dummyTransactionResponse.data!!.map { it.toTransaction() }),
                awaitItem()
            )
            awaitComplete()
        }
    }

    @Test
    fun getTransaction_thenReturnError() = runTest {
        whenever(apiService.transaction())
            .thenReturn(dummyTransactionResponse.copy(data = null))

        fulfillmentRepository.getTransaction().test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(
                "No data",
                (awaitItem() as ResultState.Error).error.message
            )
            awaitComplete()
        }
    }
}
