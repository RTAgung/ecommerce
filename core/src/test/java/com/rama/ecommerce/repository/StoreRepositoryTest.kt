package com.rama.ecommerce.repository

import app.cash.turbine.test
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.StoreRepository
import com.rama.ecommerce.data.repository.StoreRepositoryImpl
import com.rama.ecommerce.data.source.remote.service.ApiService
import com.rama.ecommerce.utils.dummy.response.dummyProductsDetailResponse
import com.rama.ecommerce.utils.dummy.response.dummyProductsReviewResponse
import com.rama.ecommerce.utils.dummy.response.dummySearchResponse
import com.rama.ecommerce.utils.extension.toDetailProduct
import com.rama.ecommerce.utils.extension.toReview
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class StoreRepositoryTest {

    private lateinit var storeRepository: StoreRepository
    private lateinit var apiService: ApiService

    @Before
    fun setup() {
        apiService = mock()
        storeRepository = StoreRepositoryImpl(
            apiService
        )
    }

    @Test
    fun searchProducts_withQuery_thenReturnSearchData() = runTest {
        whenever(apiService.search("query")).thenReturn(dummySearchResponse)
        storeRepository.searchProducts("query").test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(ResultState.Success(dummySearchResponse.data), awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun searchProducts_withNoQuery_thenReturnEmptyData() = runTest {
        storeRepository.searchProducts("").test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(ResultState.Success<List<String>>(emptyList()), awaitItem())
            awaitComplete()
        }
    }

    @Test
    fun searchProducts_withError_thenReturnError() = runTest {
        whenever(apiService.search("query")).then { throw Exception("Error") }
        storeRepository.searchProducts("query").test {
            assertEquals(ResultState.Loading, awaitItem())
            assertTrue(awaitItem() is ResultState.Error)
            awaitComplete()
        }
    }

    @Test
    fun getDetailProducts_thenReturnProductDetail() = runTest {
        whenever(apiService.detailProducts("id")).thenReturn(dummyProductsDetailResponse)
        storeRepository.detailProducts("id").test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(
                ResultState.Success(dummyProductsDetailResponse.data!!.toDetailProduct()),
                awaitItem()
            )
            awaitComplete()
        }
    }

    @Test
    fun getDetailProducts_thenReturnError() = runTest {
        whenever(apiService.detailProducts("id"))
            .thenReturn(dummyProductsDetailResponse.copy(data = null))
        storeRepository.detailProducts("id").test {
            assertEquals(ResultState.Loading, awaitItem())
            assertTrue(awaitItem() is ResultState.Error)
            awaitComplete()
        }
    }

    @Test
    fun getReviewProducts_thenReturnReviewData() = runTest {
        whenever(apiService.reviewProducts("id")).thenReturn(dummyProductsReviewResponse)
        storeRepository.reviewProducts("id").test {
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(
                ResultState.Success(dummyProductsReviewResponse.data!!.map { it.toReview() }),
                awaitItem()
            )
            awaitComplete()
        }
    }

    @Test
    fun getReviewProducts_thenReturnError() = runTest {
        whenever(apiService.reviewProducts("id"))
            .thenReturn(dummyProductsReviewResponse.copy(data = null))
        storeRepository.reviewProducts("id").test {
            assertEquals(ResultState.Loading, awaitItem())
            assertTrue(awaitItem() is ResultState.Error)
            awaitComplete()
        }
    }
}
