package com.rama.ecommerce.repository

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.repository.NotificationRepository
import com.rama.ecommerce.data.repository.NotificationRepositoryImpl
import com.rama.ecommerce.data.source.local.room.dao.NotificationDao
import com.rama.ecommerce.utils.dummy.dummyNotificationList
import com.rama.ecommerce.utils.extension.toNotification
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class NotificationRepositoryTest {

    private lateinit var notificationRepository: NotificationRepository
    private lateinit var notificationDao: NotificationDao

    @Before
    fun setup() {
        notificationDao = mock()
        notificationRepository = NotificationRepositoryImpl(
            notificationDao
        )
    }

    @Test
    fun getNotificationData_thenReturnNotificationData() = runTest {
        whenever(notificationDao.getData()).thenReturn(flowOf(dummyNotificationList))

        val actualData = notificationRepository.getNotificationData().first()
        val expectedData = dummyNotificationList.map { it.toNotification() }.reversed()

        assertEquals(expectedData, actualData)
    }

    @Test
    fun getNotificationDataSize_thenReturnNotificationSize() = runTest {
        whenever(notificationDao.getDataSize()).thenReturn(flowOf(dummyNotificationList.size))

        val actualData = notificationRepository.getNotificationDataSize().first()
        val expectedData = dummyNotificationList.size

        assertEquals(expectedData, actualData)
    }

    @Test
    fun insertNotification_thenVerify() = runTest {
        notificationRepository.insertNotification(dummyNotificationList[0].toNotification())
        verify(notificationDao).insert(dummyNotificationList[0])
    }

    @Test
    fun updateNotification_thenVerify() = runTest {
        notificationRepository.updateNotification(dummyNotificationList[0].toNotification())
        verify(notificationDao).update(dummyNotificationList[0])
    }
}
