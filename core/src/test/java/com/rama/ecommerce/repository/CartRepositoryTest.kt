package com.rama.ecommerce.repository

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.repository.CartRepository
import com.rama.ecommerce.data.repository.CartRepositoryImpl
import com.rama.ecommerce.data.source.local.room.dao.CartDao
import com.rama.ecommerce.utils.dummy.dummyCartList
import com.rama.ecommerce.utils.extension.toCart
import com.rama.ecommerce.utils.extension.toCartEntity
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class CartRepositoryTest {

    private lateinit var cartRepository: CartRepository
    private lateinit var cartDao: CartDao

    @Before
    fun setup() {
        cartDao = mock()
        cartRepository = CartRepositoryImpl(
            cartDao
        )
    }

    @Test
    fun getCartData_thenReturnCartData() = runTest {
        whenever(cartDao.getData()).thenReturn(flowOf(dummyCartList))
        val actualData = cartRepository.getCartData().first()
        val expectedData = dummyCartList.map { it.toCart() }.reversed()

        assertEquals(expectedData, actualData)
    }

    @Test
    fun getCartDataSize_thenReturnCartSize() = runTest {
        whenever(cartDao.getDataSize()).thenReturn(flowOf(dummyCartList.size))
        val actualData = cartRepository.getCartDataSize().first()
        val expectedData = dummyCartList.size

        assertEquals(expectedData, actualData)
    }

    @Test
    fun checkStockReady_whenQuantityIsNullAndNotExist_thenReturnTrue() = runTest {
        val cart = dummyCartList[0].toCart().copy(quantity = null, stock = 5)
        whenever(cartDao.checkExistData(cart.productId)).thenReturn(false)

        val actualData = cartRepository.isStockReady(cart)

        assertTrue(actualData)
    }

    @Test
    fun checkStockReady_whenQuantityIsNullAndExist_thenReturnTrue() = runTest {
        val cart = dummyCartList[0].toCart().copy(quantity = null, stock = 5)
        whenever(cartDao.checkExistData(cart.productId)).thenReturn(true)
        whenever(cartDao.getDetailData(cart.productId)).thenReturn(flowOf(cart.toCartEntity()))

        val actualData = cartRepository.isStockReady(cart)

        assertTrue(actualData)
    }

    @Test
    fun checkStockReady_whenQuantityIsNotNull_thenReturnFalse() = runTest {
        val cart = dummyCartList[0].toCart().copy(quantity = 5, stock = 5)

        val actualData = cartRepository.isStockReady(cart)
        assertFalse(actualData)
    }

    @Test
    fun insertCart_thenVerify() = runTest {
        val cart = dummyCartList[0].toCart()
        whenever(cartDao.checkExistData(cart.productId)).thenReturn(false)
        cartRepository.insertCart(cart)

        verify(cartDao).insert(cart.toCartEntity())
    }

    @Test
    fun updateCartQuantity_whenQuantityIsNull_thenVerify() = runTest {
        val cart = dummyCartList[0].toCart().copy(quantity = null)
        val cartEntity = cart.toCartEntity()
        whenever(cartDao.getDetailData(cart.productId)).thenReturn(flowOf(cart.toCartEntity()))
        cartRepository.updateCartQuantity(cart, true)

        verify(cartDao).update(cartEntity.copy(quantity = cartEntity.quantity + 1))
    }

    @Test
    fun updateCartQuantity_whenQuantityIsNotNull_thenVerify() = runTest {
        val cart = dummyCartList[0].toCart().copy(quantity = 2)
        val cartEntity = cart.toCartEntity()
        cartRepository.updateCartQuantity(cart, true)

        verify(cartDao).update(cartEntity.copy(quantity = cartEntity.quantity + 1))
    }

    @Test
    fun updateCartChecked_thenVerify() = runTest {
        val cartList = dummyCartList.filter {
            (it.productId.toInt() % 2) == 0
        }.map { it.toCart() }

        cartList.map { cart ->
            cartRepository.updateCartChecked(true, cart)
        }

        val cartCheckedEntityList = cartList.map { it.toCartEntity().copy(isChecked = true) }
        cartCheckedEntityList.map { cartEntity ->
            verify(cartDao).update(cartEntity)
        }
    }

    @Test
    fun deleteCart_thenVerify() = runTest {
        val cartList = dummyCartList.filter {
            (it.productId.toInt() % 2) == 0
        }.map { it.toCart() }
        cartList.map { cart ->
            cartRepository.deleteCart(cart)
        }

        val cartCheckedEntityList = cartList.map { it.toCartEntity() }
        cartCheckedEntityList.map { cartEntity ->
            verify(cartDao).delete(cartEntity)
        }
    }
}
