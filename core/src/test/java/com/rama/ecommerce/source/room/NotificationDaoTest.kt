package com.rama.ecommerce.source.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.rama.ecommerce.data.source.local.room.AppDatabase
import com.rama.ecommerce.data.source.local.room.dao.NotificationDao
import com.rama.ecommerce.data.source.local.room.entity.NotificationEntity
import com.rama.ecommerce.utils.dummy.dummyNotificationList
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class NotificationDaoTest {

    private lateinit var context: Context
    private lateinit var appDatabase: AppDatabase
    private lateinit var notificationDao: NotificationDao

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
        appDatabase = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).allowMainThreadQueries().build()
        notificationDao = appDatabase.notificationDao()
    }

    @After
    fun teardown() {
        appDatabase.close()
    }

    @Test
    fun insertAndGetData_thenReturnNotificationData() = runTest {
        dummyNotificationList.map {
            notificationDao.insert(it)
        }

        val actualData = notificationDao.getData().first()
        val expectedData = dummyNotificationList

        assertEquals(expectedData, actualData)
    }

    @Test
    fun insertAndGetDataSize_thenReturnNotificationSize() = runTest {
        dummyNotificationList.map {
            notificationDao.insert(it)
        }

        val actualData = notificationDao.getDataSize().first()
        val expectedData = dummyNotificationList.size

        assertEquals(expectedData, actualData)
    }

    @Test
    fun updateAndGetDetailData_thenReturnNotificationDetail() = runTest {
        dummyNotificationList.map {
            notificationDao.insert(it)
        }
        val dummyNotification = dummyNotificationList[0]
        notificationDao.update(dummyNotification.copy(isRead = true))

        val actualData = notificationDao.getData().first()
        val expectedData = dummyNotificationList.map {
            if (it.id == dummyNotification.id) {
                it.copy(isRead = true)
            } else {
                it
            }
        }

        assertEquals(expectedData, actualData)
    }

    @Test
    fun clearAndGetData_thenReturnNoData() = runTest {
        dummyNotificationList.map {
            notificationDao.insert(it)
        }
        notificationDao.clearTable()

        val actualData = notificationDao.getData().first()

        assertEquals(ArrayList<NotificationEntity>(), actualData)
        assertEquals(0, actualData.size)
    }
}
