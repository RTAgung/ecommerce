package com.rama.ecommerce.source.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.rama.ecommerce.data.source.local.room.AppDatabase
import com.rama.ecommerce.data.source.local.room.dao.WishlistDao
import com.rama.ecommerce.data.source.local.room.entity.WishlistEntity
import com.rama.ecommerce.utils.dummy.dummyWishlistList
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class WishlistDaoTest {

    private lateinit var context: Context
    private lateinit var appDatabase: AppDatabase
    private lateinit var wishlistDao: WishlistDao

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
        appDatabase = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).allowMainThreadQueries().build()
        wishlistDao = appDatabase.wishlistDao()
    }

    @After
    fun teardown() {
        appDatabase.close()
    }

    @Test
    fun insertAndGetData_thenReturnWishlistData() = runTest {
        dummyWishlistList.map {
            wishlistDao.insert(it)
        }

        val actualData = wishlistDao.getData().first()
        val expectedData = dummyWishlistList

        assertEquals(expectedData, actualData)
    }

    @Test
    fun insertAndGetDataSize_thenReturnWishlistSize() = runTest {
        dummyWishlistList.map {
            wishlistDao.insert(it)
        }

        val actualData = wishlistDao.getDataSize().first()
        val expectedData = dummyWishlistList.size

        assertEquals(expectedData, actualData)
    }

    @Test
    fun insertAndCheckExistData_thenReturnTrue() = runTest {
        dummyWishlistList.map {
            wishlistDao.insert(it)
        }

        val dataId = dummyWishlistList[0].productId

        assertTrue(wishlistDao.checkExistData(dataId))
    }

    @Test
    fun deleteAndGetData_thenReturnWishlistDataAndSize() = runTest {
        dummyWishlistList.map {
            wishlistDao.insert(it)
        }
        wishlistDao.delete(dummyWishlistList[0])

        val actualData = wishlistDao.getData().first()
        val expectedData = dummyWishlistList
        expectedData.remove(dummyWishlistList[0])

        assertEquals(expectedData, actualData)
        assertEquals(expectedData.size, actualData.size)
    }

    @Test
    fun clearAndGetData_thenReturnNoData() = runTest {
        dummyWishlistList.map {
            wishlistDao.insert(it)
        }
        wishlistDao.clearTable()

        val actualData = wishlistDao.getData().first()

        assertEquals(ArrayList<WishlistEntity>(), actualData)
        assertEquals(0, actualData.size)
    }
}
