package com.rama.ecommerce.source.room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.rama.ecommerce.data.source.local.room.AppDatabase
import com.rama.ecommerce.data.source.local.room.dao.CartDao
import com.rama.ecommerce.data.source.local.room.entity.CartEntity
import com.rama.ecommerce.utils.dummy.dummyCartList
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class CartDaoTest {

    private lateinit var context: Context
    private lateinit var appDatabase: AppDatabase
    private lateinit var cartDao: CartDao

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
        appDatabase = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).allowMainThreadQueries().build()
        cartDao = appDatabase.cartDao()
    }

    @After
    fun teardown() {
        appDatabase.close()
    }

    @Test
    fun insertAndGetData_thenReturnCartData() = runTest {
        dummyCartList.map {
            cartDao.insert(it)
        }

        val actualData = cartDao.getData().first()
        val expectedData = dummyCartList

        assertEquals(expectedData, actualData)
    }

    @Test
    fun insertAndGetDetailData_thenReturnCartDetailData() = runTest {
        dummyCartList.map {
            cartDao.insert(it)
        }

        val actualData = cartDao.getDetailData(dummyCartList[0].productId).first()
        val expectedData = dummyCartList[0]

        assertEquals(expectedData, actualData)
    }

    @Test
    fun insertAndGetDataSize_thenReturnCartSize() = runTest {
        dummyCartList.map {
            cartDao.insert(it)
        }

        val actualData = cartDao.getDataSize().first()
        val expectedData = dummyCartList.size

        assertEquals(expectedData, actualData)
    }

    @Test
    fun insertAndCheckExistData_thenReturnTrue() = runTest {
        dummyCartList.map {
            cartDao.insert(it)
        }

        val dataId = dummyCartList[0].productId

        assertTrue(cartDao.checkExistData(dataId))
    }

    @Test
    fun updateAndGetDetailData_thenReturnUpdatedCartData() = runTest {
        dummyCartList.map {
            cartDao.insert(it)
        }
        val dummyCart = dummyCartList[0]
        cartDao.update(dummyCart.copy(quantity = 10))

        val actualData = cartDao.getDetailData(dummyCart.productId).first()
        val expectedData = dummyCart.copy(quantity = 10)

        assertEquals(expectedData, actualData)
    }

    @Test
    fun deleteAndGetData_thenReturnCartDataAndSize() = runTest {
        dummyCartList.map {
            cartDao.insert(it)
        }
        cartDao.delete(dummyCartList[0])

        val actualData = cartDao.getData().first()
        val expectedData = dummyCartList
        expectedData.remove(dummyCartList[0])

        assertEquals(expectedData, actualData)
        assertEquals(expectedData.size, actualData.size)
    }

    @Test
    fun clearAndGetData_thenReturnNoData() = runTest {
        dummyCartList.map {
            cartDao.insert(it)
        }
        cartDao.clearTable()

        val actualData = cartDao.getData().first()

        assertEquals(ArrayList<CartEntity>(), actualData)
        assertEquals(0, actualData.size)
    }
}
