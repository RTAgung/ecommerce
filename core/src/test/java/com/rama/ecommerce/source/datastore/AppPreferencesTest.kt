package com.rama.ecommerce.source.datastore

import android.content.Context
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.preferencesDataStoreFile
import androidx.test.core.app.ApplicationProvider
import com.rama.ecommerce.data.model.User
import com.rama.ecommerce.data.source.local.preferences.AppPreferences
import com.rama.ecommerce.utils.dummy.dummyUserPreferences
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class AppPreferencesTest {
    private lateinit var context: Context
    private lateinit var preferences: AppPreferences

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
        preferences = AppPreferences(
            PreferenceDataStoreFactory.create(produceFile = {
                context.preferencesDataStoreFile(
                    "test_app_preferences"
                )
            })
        )
    }

    @Test
    fun setAndGetFirstTimeRunApp_thenReturnFalse() = runTest {
        preferences.setFirstTimeRunApp(false)
        val actualData = preferences.getFirstTimeRunApp().first()

        assertFalse(actualData)
    }

    @Test
    fun setAndGetUserDataSession_thenReturnUserData() = runTest {
        preferences.setUserDataSession(dummyUserPreferences)

        val actualData = preferences.getUserDataSession().first()

        assertEquals(dummyUserPreferences, actualData)
    }

    @Test
    fun setAndGetUserAuthorization_thenReturnTrue() = runTest {
        preferences.setUserAuthorization(true)

        val actualData = preferences.checkUserAuthorization().first()

        assertTrue(actualData)
    }

    @Test
    fun setAndGetAppTheme_thenReturnTrue() = runTest {
        preferences.setAppTheme(true)

        val actualData = preferences.getAppTheme().first()
        assertTrue(actualData)
    }

    @Test
    fun setAndGetAppLanguage_thenReturnIdLang() = runTest {
        preferences.setAppLanguage("id")

        val actualData = preferences.getAppLanguage().first()
        val expectedData = "id"

        assertEquals(expectedData, actualData)
    }

    @Test
    fun clearAndGetAllDataSession_thenReturnUserDataNull() = runTest {
        preferences.setUserDataSession(dummyUserPreferences)
        preferences.clearAllDataSession()

        val actualData = preferences.getUserDataSession().first()

        assertEquals(User(), actualData)
    }
}
