package com.rama.ecommerce.source.retrofit

import com.rama.ecommerce.data.source.remote.request.AuthRequest
import com.rama.ecommerce.data.source.remote.request.FulfillmentRequest
import com.rama.ecommerce.data.source.remote.request.RatingRequest
import com.rama.ecommerce.data.source.remote.request.RefreshRequest
import com.rama.ecommerce.data.source.remote.service.ApiService
import com.rama.ecommerce.utils.Helper
import com.rama.ecommerce.utils.dummy.response.dummyFulfillmentResponse
import com.rama.ecommerce.utils.dummy.response.dummyLoginResponse
import com.rama.ecommerce.utils.dummy.response.dummyProductsDetailResponse
import com.rama.ecommerce.utils.dummy.response.dummyProductsResponse
import com.rama.ecommerce.utils.dummy.response.dummyProductsReviewResponse
import com.rama.ecommerce.utils.dummy.response.dummyProfileResponse
import com.rama.ecommerce.utils.dummy.response.dummyRatingResponse
import com.rama.ecommerce.utils.dummy.response.dummyRefreshResponse
import com.rama.ecommerce.utils.dummy.response.dummyRegisterResponse
import com.rama.ecommerce.utils.dummy.response.dummySearchResponse
import com.rama.ecommerce.utils.dummy.response.dummyTransactionResponse
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

@RunWith(JUnit4::class)
class ApiServiceTest {

    private lateinit var server: MockWebServer
    private lateinit var apiService: ApiService

    @Before
    fun setup() {
        server = MockWebServer()
        server.start()

        apiService = Retrofit.Builder()
            .baseUrl(server.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().build())
            .build()
            .create(ApiService::class.java)
    }

    @After
    fun teardown() {
        server.shutdown()
    }

    @Test
    fun register() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("auth/register.json"))
        server.enqueue(response)

        val actualData = apiService.register(AuthRequest("", "", ""))
        val expectedData = dummyRegisterResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun login() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("auth/login.json"))
        server.enqueue(response)

        val actualData = apiService.login(AuthRequest("", "", ""))
        val expectedData = dummyLoginResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun refresh() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("auth/refresh.json"))
        server.enqueue(response)

        val actualData = apiService.refresh(RefreshRequest(""))
        val expectedData = dummyRefreshResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun profile() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("main/profile.json"))
        server.enqueue(response)

        val actualData = apiService.profile("".toRequestBody("text/plain".toMediaType()), null)
        val expectedData = dummyProfileResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun products() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("main/products.json"))
        server.enqueue(response)

        val actualData = apiService.products(mapOf("" to ""))
        val expectedData = dummyProductsResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun search() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("main/search.json"))
        server.enqueue(response)

        val actualData = apiService.search("")
        val expectedData = dummySearchResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun detailProducts() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("fulfillment/products_detail.json"))
        server.enqueue(response)

        val actualData = apiService.detailProducts("")
        val expectedData = dummyProductsDetailResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun reviewProducts() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("fulfillment/products_review.json"))
        server.enqueue(response)

        val actualData = apiService.reviewProducts("")
        val expectedData = dummyProductsReviewResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun fulfillment() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("fulfillment/fulfillment.json"))
        server.enqueue(response)

        val actualData = apiService.fulfillment(
            FulfillmentRequest(
                "",
                arrayListOf(FulfillmentRequest.Item("", "", 0))
            )
        )
        val expectedData = dummyFulfillmentResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun rating() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("fulfillment/rating.json"))
        server.enqueue(response)

        val actualData = apiService.rating(RatingRequest("", null, null))
        val expectedData = dummyRatingResponse

        assertEquals(expectedData, actualData)
    }

    @Test
    fun transaction() = runTest {
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(Helper.readTestResourceFile("main/transaction.json"))
        server.enqueue(response)

        val actualData = apiService.transaction()
        val expectedData = dummyTransactionResponse

        assertEquals(expectedData, actualData)
    }
}
