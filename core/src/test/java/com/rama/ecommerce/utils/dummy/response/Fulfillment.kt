package com.rama.ecommerce.utils.dummy.response

import com.rama.ecommerce.data.model.Payment
import com.rama.ecommerce.data.source.remote.response.FulfillmentResponse
import com.rama.ecommerce.data.source.remote.response.FulfillmentResponseData
import com.rama.ecommerce.data.source.remote.response.ProductVariantItem
import com.rama.ecommerce.data.source.remote.response.ProductsDetailResponse
import com.rama.ecommerce.data.source.remote.response.ProductsDetailResponseData
import com.rama.ecommerce.data.source.remote.response.RatingResponse
import com.rama.ecommerce.data.source.remote.response.ReviewResponse
import com.rama.ecommerce.data.source.remote.response.ReviewResponseItem

val dummyFulfillmentResponse = FulfillmentResponse(
    code = 200,
    message = "OK",
    data = FulfillmentResponseData(
        invoiceId = "ba47402c-d263-49d3-a1f8-759ae59fa4a1",
        status = true,
        date = "09 Jun 2023",
        time = "08:53",
        payment = "Bank BCA",
        total = 48998000
    )
)

val dummyPaymentList = ArrayList<Payment>().apply {
    repeat(6) { paymentIndex ->
        add(
            Payment(
                title = paymentIndex.toString(),
                item = ArrayList<Payment.PaymentItem>().apply {
                    repeat(6) { itemIndex ->
                        add(
                            Payment.PaymentItem(
                                image = "image.png",
                                label = itemIndex.toString(),
                                status = true
                            )
                        )
                    }
                }
            )
        )
    }
}

val dummyProductsDetailResponse = ProductsDetailResponse(
    code = 200,
    message = "OK",
    data = ProductsDetailResponseData(
        productId = "17b4714d-527a-4be2-84e2-e4c37c2b3292",
        productName = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
        productPrice = 24499000,
        image = arrayListOf(
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
            "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
        ),
        brand = "Asus",
        description = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H",
        store = "AsusStore",
        sale = 12,
        stock = 2,
        totalRating = 7,
        totalReview = 5,
        totalSatisfaction = 100,
        productRating = 5.0F,
        productVariant = arrayListOf(
            ProductVariantItem(
                variantName = "RAM 16GB",
                variantPrice = 0
            ),
            ProductVariantItem(
                variantName = "RAM 32GB",
                variantPrice = 1000000
            ),
        )
    )
)

val dummyProductsReviewResponse = ReviewResponse(
    code = 200,
    message = "OK",
    data = arrayListOf(
        ReviewResponseItem(
            userName = "John",
            userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:",
            userRating = 4,
            userReview = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
        ),
        ReviewResponseItem(
            userName = "Doe",
            userImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn:",
            userRating = 5,
            userReview = "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
        )
    )
)

val dummyRatingResponse = RatingResponse(
    code = 200,
    message = "Fulfillment rating and review success"
)
