package com.rama.ecommerce.utils.dummy

import com.rama.ecommerce.data.source.local.room.entity.CartEntity
import com.rama.ecommerce.data.source.local.room.entity.NotificationEntity
import com.rama.ecommerce.data.source.local.room.entity.WishlistEntity

val dummyCartList = ArrayList<CartEntity>().apply {
    repeat(6) {
        add(
            CartEntity(
                productId = it.toString(),
                productName = "Asus ROG",
                productPrice = 1000000,
                image = "image.png",
                stock = 10,
                sale = 100,
                store = "Toko Komputer",
                productRating = 5F,
                description = "Lorem ipsum",
                totalRating = 20,
                totalSatisfaction = 100,
                brand = "Asus",
                totalReview = 12,
                variantName = "RAM 16GB",
                variantPrice = 0,
                quantity = 2,
                isChecked = false
            )
        )
    }
}

val dummyNotificationList = ArrayList<NotificationEntity>().apply {
    repeat(6) {
        add(
            NotificationEntity(
                id = it,
                image = "image.png",
                title = "Title",
                body = "Lorem ipsum",
                date = "12 Oct 2023",
                isRead = false,
                time = "09:32",
                type = "Info"
            )
        )
    }
}

val dummyWishlistList = ArrayList<WishlistEntity>().apply {
    repeat(6) {
        add(
            WishlistEntity(
                productId = it.toString(),
                productName = "Asus ROG",
                productPrice = 1000000,
                image = "image.png",
                stock = 10,
                sale = 100,
                store = "Toko Komputer",
                productRating = 5F,
                description = "Lorem ipsum",
                totalRating = 20,
                totalSatisfaction = 100,
                brand = "Asus",
                totalReview = 12,
                variantName = "RAM 16GB",
                variantPrice = 0,
            )
        )
    }
}
