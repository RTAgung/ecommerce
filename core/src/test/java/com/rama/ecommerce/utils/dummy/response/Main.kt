package com.rama.ecommerce.utils.dummy.response

import com.rama.ecommerce.data.source.remote.response.AuthDataResponse
import com.rama.ecommerce.data.source.remote.response.AuthResponse
import com.rama.ecommerce.data.source.remote.response.ProductsResponse
import com.rama.ecommerce.data.source.remote.response.ProductsResponseData
import com.rama.ecommerce.data.source.remote.response.ProductsResponseItem
import com.rama.ecommerce.data.source.remote.response.SearchResponse
import com.rama.ecommerce.data.source.remote.response.TransactionResponse
import com.rama.ecommerce.data.source.remote.response.TransactionResponseData
import com.rama.ecommerce.data.source.remote.response.TransactionResponseItem

val dummyProfileResponse = AuthResponse(
    code = 200,
    message = "OK",
    data = AuthDataResponse(
        userName = "Test",
        userImage = "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png"
    )
)

val dummyProductsResponse = ProductsResponse(
    code = 200,
    message = "OK",
    data = ProductsResponseData(
        itemsPerPage = 10,
        currentItemCount = 10,
        pageIndex = 1,
        totalPages = 3,
        items = arrayListOf(
            ProductsResponseItem(
                productId = "601bb59a-4170-4b0a-bd96-f34538922c7c",
                productName = "Lenovo Legion 3",
                productPrice = 10000000,
                image = "image1",
                brand = "Lenovo",
                store = "LenovoStore",
                sale = 2,
                productRating = 4.0F
            ),
            ProductsResponseItem(
                productId = "3134a179-dff6-464f-b76e-d7507b06887b",
                productName = "Lenovo Legion 5",
                productPrice = 15000000,
                image = "image1",
                brand = "Lenovo",
                store = "LenovoStore",
                sale = 4,
                productRating = 4.0F
            )
        )
    )
)

val dummySearchResponse = SearchResponse(
    code = 200,
    message = "OK",
    data = arrayListOf(
        "Lenovo Legion 3",
        "Lenovo Legion 5",
        "Lenovo Legion 7",
        "Lenovo Ideapad 3",
        "Lenovo Ideapad 5",
        "Lenovo Ideapad 7"
    )
)

val dummyTransactionResponse = TransactionResponse(
    code = 200,
    message = "OK",
    data = arrayListOf(
        TransactionResponseData(
            invoiceId = "8cad85b1-a28f-42d8-9479-72ce4b7f3c7d",
            status = true,
            date = "09 Jun 2023",
            time = "09:05",
            payment = "Bank BCA",
            total = 48998000,
            items = arrayListOf(
                TransactionResponseItem(
                    productId = "bee98108-660c-4ac0-97d3-63cdc1492f53",
                    variantName = "RAM 16GB",
                    quantity = 2
                )
            ),
            rating = 4,
            review = "LGTM",
            image = "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/",
            name = "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray"
        )
    )
)
