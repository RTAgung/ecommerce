package com.rama.ecommerce.utils.dummy.response

import com.rama.ecommerce.data.source.remote.response.AuthDataResponse
import com.rama.ecommerce.data.source.remote.response.AuthResponse

val dummyRegisterResponse = AuthResponse(
    code = 200,
    message = "OK",
    data = AuthDataResponse(
        accessToken = "LuAUTzCWhjMcQelP8MjfnVDqKSZj2LaqHv3TY08AB7TQ",
        refreshToken = "pTGOEcIcoFCv2n9IEWS0gqxNnDaNf3sXBm7JHCxFexB5FGRgQ",
        expiresAt = 600
    )
)

val dummyLoginResponse = AuthResponse(
    code = 200,
    message = "OK",
    data = AuthDataResponse(
        userName = "Test Subianto",
        userImage = "image.png",
        accessToken = "LuAUTzCWhjMcQelP8MjfnVDqKSZj2LaqHv3TY08AB7TQ",
        refreshToken = "pTGOEcIcoFCv2n9IEWS0gqxNnDaNf3sXBm7JHCxFexB5FGRgQ",
        expiresAt = 600
    )
)

val dummyRefreshResponse = AuthResponse(
    code = 200,
    message = "OK",
    data = AuthDataResponse(
        accessToken = "LuAUTzCWhjMcQelP8MjfnVDqKSZj2LaqHv3TY08AB7TQ",
        refreshToken = "pTGOEcIcoFCv2n9IEWS0gqxNnDaNf3sXBm7JHCxFexB5FGRgQ",
        expiresAt = 600
    )
)
