package com.rama.ecommerce.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.rama.ecommerce.data.model.DetailProduct
import com.rama.ecommerce.data.model.Product
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.model.Review
import com.rama.ecommerce.data.source.ProductsPagingSource
import com.rama.ecommerce.data.source.remote.request.ProductsQuery
import com.rama.ecommerce.data.source.remote.service.ApiService
import com.rama.ecommerce.utils.extension.toDetailProduct
import com.rama.ecommerce.utils.extension.toProduct
import com.rama.ecommerce.utils.extension.toReview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class StoreRepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : StoreRepository {
    override fun getProducts(query: ProductsQuery): Flow<PagingData<Product>> {
        return Pager(
            config = PagingConfig(pageSize = 10, initialLoadSize = 10, prefetchDistance = 1),
            pagingSourceFactory = {
                ProductsPagingSource(apiService, query)
            },
        ).flow.map {
            it.map { productsResponseItem ->
                productsResponseItem.toProduct()
            }
        }
    }

    override fun searchProducts(query: String): Flow<ResultState<List<String>>> = flow {
        emit(ResultState.Loading)
        try {
            if (query.isNotEmpty()) {
                val response = apiService.search(query)
                val listSearch = response.data ?: emptyList()
                emit(ResultState.Success(listSearch))
            } else {
                emit(ResultState.Success(emptyList()))
            }
        } catch (e: Exception) {
            emit(ResultState.Error(e))
        }
    }

    override fun detailProducts(id: String): Flow<ResultState<DetailProduct>> = flow {
        emit(ResultState.Loading)
        try {
            val response = apiService.detailProducts(id)
            val data = response.data?.toDetailProduct()
            if (data != null) {
                emit(ResultState.Success(data))
            } else {
                throw Exception("No data")
            }
        } catch (e: Exception) {
            emit(ResultState.Error(e))
        }
    }

    override fun reviewProducts(id: String): Flow<ResultState<List<Review>>> = flow {
        emit(ResultState.Loading)
        try {
            val response = apiService.reviewProducts(id)
            val data = response.data?.map { it.toReview() }
            if (data != null) {
                emit(ResultState.Success(data))
            } else {
                throw Exception("No data")
            }
        } catch (e: Exception) {
            emit(ResultState.Error(e))
        }
    }
}
