package com.rama.ecommerce.data.repository

import com.rama.ecommerce.data.model.Cart
import com.rama.ecommerce.data.model.Fulfillment
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.model.Transaction
import com.rama.ecommerce.data.source.remote.request.FulfillmentRequest
import com.rama.ecommerce.data.source.remote.request.RatingRequest
import com.rama.ecommerce.data.source.remote.service.ApiService
import com.rama.ecommerce.utils.extension.toFulfillment
import com.rama.ecommerce.utils.extension.toFulfillmentRequestItem
import com.rama.ecommerce.utils.extension.toTransaction
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.net.HttpURLConnection
import javax.inject.Inject

class FulfillmentRepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : FulfillmentRepository {

    override fun fulfillment(
        payment: String,
        listItem: List<Cart>
    ): Flow<ResultState<Fulfillment>> = flow {
        emit(ResultState.Loading)
        try {
            val listItemRequest = listItem.map { it.toFulfillmentRequestItem() }
            val request = FulfillmentRequest(payment, listItemRequest)

            val response = apiService.fulfillment(request)

            val data = response.data?.toFulfillment()
            if (data != null) {
                emit(ResultState.Success(data))
            } else {
                throw Exception("No data")
            }
        } catch (e: Exception) {
            emit(ResultState.Error(e))
        }
    }

    override fun rating(
        invoiceId: String,
        rating: Int?,
        review: String?
    ): Flow<ResultState<Boolean>> = flow {
        emit(ResultState.Loading)
        try {
            val response = apiService.rating(RatingRequest(invoiceId, rating, review))
            if (response.code == HttpURLConnection.HTTP_OK) {
                emit(ResultState.Success(true))
            } else {
                throw Exception("Failed")
            }
        } catch (e: Exception) {
            emit(ResultState.Error(e))
        }
    }

    override fun getTransaction(): Flow<ResultState<List<Transaction>>> = flow {
        emit(ResultState.Loading)
        try {
            val response = apiService.transaction()
            val listData = response.data?.map { it.toTransaction() }
            if (listData != null) {
                emit(ResultState.Success(listData.reversed()))
            } else {
                throw Exception("No data")
            }
        } catch (e: Exception) {
            emit(ResultState.Error(e))
        }
    }
}
