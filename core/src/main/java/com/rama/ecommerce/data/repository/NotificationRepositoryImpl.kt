package com.rama.ecommerce.data.repository

import com.rama.ecommerce.data.model.Notification
import com.rama.ecommerce.data.source.local.room.dao.NotificationDao
import com.rama.ecommerce.data.source.local.room.entity.NotificationEntity
import com.rama.ecommerce.utils.extension.toNotification
import com.rama.ecommerce.utils.extension.toNotificationEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class NotificationRepositoryImpl @Inject constructor(
    private val notificationDao: NotificationDao
) : NotificationRepository {
    override fun getNotificationData(): Flow<List<Notification>> =
        notificationDao.getData().map { value: List<NotificationEntity> ->
            value.map { it.toNotification() }.reversed()
        }

    override fun getNotificationDataSize(): Flow<Int> = notificationDao.getDataSize()

    override suspend fun insertNotification(notification: Notification) {
        notificationDao.insert(notification.toNotificationEntity())
    }

    override suspend fun updateNotification(notification: Notification) {
        notificationDao.update(notification.toNotificationEntity())
    }
}
