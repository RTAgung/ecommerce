package com.rama.ecommerce.data.model

data class Review(
    val userImage: String,
    val userName: String,
    val userReview: String,
    val userRating: Int
)
