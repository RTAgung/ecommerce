package com.rama.ecommerce.data.repository

import com.rama.ecommerce.data.model.Wishlist
import kotlinx.coroutines.flow.Flow

interface WishlistRepository {
    fun getWishlistData(): Flow<List<Wishlist>>
    fun getWishlistDataSize(): Flow<Int>
    suspend fun checkExistWishlistData(wishlistId: String): Boolean
    suspend fun insertWishlist(wishlist: Wishlist)
    suspend fun deleteWishlist(wishlist: Wishlist)
}
