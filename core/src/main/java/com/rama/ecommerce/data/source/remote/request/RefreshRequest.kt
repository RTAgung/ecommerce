package com.rama.ecommerce.data.source.remote.request

data class RefreshRequest(
    val token: String,
)
