package com.rama.ecommerce.data.source.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rama.ecommerce.data.source.local.room.dao.CartDao
import com.rama.ecommerce.data.source.local.room.dao.NotificationDao
import com.rama.ecommerce.data.source.local.room.dao.WishlistDao
import com.rama.ecommerce.data.source.local.room.entity.CartEntity
import com.rama.ecommerce.data.source.local.room.entity.NotificationEntity
import com.rama.ecommerce.data.source.local.room.entity.WishlistEntity

@Database(
    entities = [WishlistEntity::class, CartEntity::class, NotificationEntity::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun wishlistDao(): WishlistDao
    abstract fun cartDao(): CartDao
    abstract fun notificationDao(): NotificationDao
}
