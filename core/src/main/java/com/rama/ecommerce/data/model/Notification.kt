package com.rama.ecommerce.data.model

data class Notification(
    val id: Int? = null,
    val image: String,
    val title: String,
    val body: String,
    val type: String,
    val date: String,
    val time: String,
    val isRead: Boolean = false
)
