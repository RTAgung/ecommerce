package com.rama.ecommerce.data.repository

import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.model.User
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface PreloginRepository {
    suspend fun setFirstTimeRunApp(isFirstTime: Boolean)
    fun getFirstTimeRunApp(): Flow<Boolean>
    fun register(email: String, password: String): Flow<ResultState<Boolean>>
    fun login(email: String, password: String): Flow<ResultState<Boolean>>
    suspend fun logout()
    fun getUserDataSession(): Flow<User>
    fun checkUserAuthorization(): Flow<Boolean>
    fun uploadProfile(
        userName: RequestBody,
        userImage: MultipartBody.Part?
    ): Flow<ResultState<Boolean>>

    suspend fun setAppTheme(isDarkTheme: Boolean)
    fun getAppTheme(): Flow<Boolean>
    suspend fun setAppLanguage(appLanguage: String?)
    fun getAppLanguage(): Flow<String>
}
