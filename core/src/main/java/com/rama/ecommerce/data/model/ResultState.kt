package com.rama.ecommerce.data.model

sealed class ResultState<out T> {
    data class Success<T>(val data: T) : ResultState<T>()
    data class Error(val error: Throwable) : ResultState<Nothing>()
    data object Loading : ResultState<Nothing>()
}
