package com.rama.ecommerce.data.repository

import com.rama.ecommerce.data.model.Cart
import kotlinx.coroutines.flow.Flow

interface CartRepository {
    fun getCartData(): Flow<List<Cart>>
    fun getCartDataSize(): Flow<Int>
    suspend fun isStockReady(cart: Cart): Boolean
    suspend fun insertCart(cart: Cart)
    suspend fun updateCartQuantity(cart: Cart, isInsert: Boolean)
    suspend fun updateCartChecked(isChecked: Boolean, cart: Cart)
    suspend fun deleteCart(cart: Cart)
}
