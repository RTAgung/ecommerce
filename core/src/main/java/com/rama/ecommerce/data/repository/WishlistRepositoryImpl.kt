package com.rama.ecommerce.data.repository

import com.rama.ecommerce.data.model.Wishlist
import com.rama.ecommerce.data.source.local.room.dao.WishlistDao
import com.rama.ecommerce.data.source.local.room.entity.WishlistEntity
import com.rama.ecommerce.utils.extension.toWishlist
import com.rama.ecommerce.utils.extension.toWishlistEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class WishlistRepositoryImpl @Inject constructor(
    private val wishlistDao: WishlistDao
) : WishlistRepository {
    override fun getWishlistData(): Flow<List<Wishlist>> =
        wishlistDao.getData().map { value: List<WishlistEntity> ->
            value.map { it.toWishlist() }.reversed()
        }

    override fun getWishlistDataSize(): Flow<Int> = wishlistDao.getDataSize()

    override suspend fun checkExistWishlistData(wishlistId: String): Boolean =
        wishlistDao.checkExistData(wishlistId)

    override suspend fun insertWishlist(wishlist: Wishlist) {
        wishlistDao.insert(wishlist.toWishlistEntity())
    }

    override suspend fun deleteWishlist(wishlist: Wishlist) {
        wishlistDao.delete(wishlist.toWishlistEntity())
    }
}
