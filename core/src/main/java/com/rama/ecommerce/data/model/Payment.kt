package com.rama.ecommerce.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class Payment(
    @SerializedName("title")
    val title: String,

    @SerializedName("item")
    val item: List<PaymentItem>
) {
    @Parcelize
    data class PaymentItem(
        @SerializedName("image")
        val image: String,

        @SerializedName("label")
        val label: String,

        @SerializedName("status")
        val status: Boolean
    ) : Parcelable
}
