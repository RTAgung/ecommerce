package com.rama.ecommerce.data.repository

import com.rama.ecommerce.data.model.Cart
import com.rama.ecommerce.data.model.Fulfillment
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.model.Transaction
import kotlinx.coroutines.flow.Flow

interface FulfillmentRepository {
    fun fulfillment(payment: String, listItem: List<Cart>): Flow<ResultState<Fulfillment>>
    fun rating(
        invoiceId: String,
        rating: Int? = null,
        review: String? = null
    ): Flow<ResultState<Boolean>>

    fun getTransaction(): Flow<ResultState<List<Transaction>>>
}
