package com.rama.ecommerce.data.source

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.rama.ecommerce.data.source.remote.request.ProductsQuery
import com.rama.ecommerce.data.source.remote.response.ProductsResponseItem
import com.rama.ecommerce.data.source.remote.service.ApiService
import com.rama.ecommerce.utils.extension.toQueryMap

class ProductsPagingSource(
    private val apiService: ApiService,
    private val productsQuery: ProductsQuery
) : PagingSource<Int, ProductsResponseItem>() {
    override fun getRefreshKey(state: PagingState<Int, ProductsResponseItem>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProductsResponseItem> {
        return try {
            val position = params.key ?: 1
            val response = apiService.products(
                productsQuery.copy(
                    page = position,
                    limit = params.loadSize
                ).toQueryMap()
            )
            LoadResult.Page(
                data = response.data?.items ?: emptyList(),
                prevKey = null,
                nextKey = if (position == response.data?.totalPages) null else position + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}
