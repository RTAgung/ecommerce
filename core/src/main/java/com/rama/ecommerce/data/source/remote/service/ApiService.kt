package com.rama.ecommerce.data.source.remote.service

import com.rama.ecommerce.data.source.remote.request.AuthRequest
import com.rama.ecommerce.data.source.remote.request.FulfillmentRequest
import com.rama.ecommerce.data.source.remote.request.RatingRequest
import com.rama.ecommerce.data.source.remote.request.RefreshRequest
import com.rama.ecommerce.data.source.remote.response.AuthResponse
import com.rama.ecommerce.data.source.remote.response.FulfillmentResponse
import com.rama.ecommerce.data.source.remote.response.ProductsDetailResponse
import com.rama.ecommerce.data.source.remote.response.ProductsResponse
import com.rama.ecommerce.data.source.remote.response.RatingResponse
import com.rama.ecommerce.data.source.remote.response.ReviewResponse
import com.rama.ecommerce.data.source.remote.response.SearchResponse
import com.rama.ecommerce.data.source.remote.response.TransactionResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface ApiService {
    @POST("register")
    suspend fun register(
        @Body authRequest: AuthRequest
    ): AuthResponse

    @POST("login")
    suspend fun login(
        @Body authRequest: AuthRequest
    ): AuthResponse

    @POST("refresh")
    suspend fun refresh(
        @Body refreshRequest: RefreshRequest
    ): AuthResponse

    @Multipart
    @POST("profile")
    suspend fun profile(
        @Part("userName") userName: RequestBody,
        @Part userImage: MultipartBody.Part?
    ): AuthResponse

    @POST("products")
    suspend fun products(
        @QueryMap query: Map<String, String>
    ): ProductsResponse

    @POST("search")
    suspend fun search(
        @Query("query") query: String
    ): SearchResponse

    @GET("products/{id}")
    suspend fun detailProducts(
        @Path("id") id: String
    ): ProductsDetailResponse

    @GET("review/{id}")
    suspend fun reviewProducts(
        @Path("id") id: String
    ): ReviewResponse

    @POST("fulfillment")
    suspend fun fulfillment(
        @Body fulfillmentRequest: FulfillmentRequest
    ): FulfillmentResponse

    @POST("rating")
    suspend fun rating(
        @Body ratingRequest: RatingRequest
    ): RatingResponse

    @GET("transaction")
    suspend fun transaction(): TransactionResponse
}
