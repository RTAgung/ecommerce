package com.rama.ecommerce.data.source.remote.request

data class RatingRequest(
    val invoiceId: String,
    val rating: Int?,
    val review: String?
)
