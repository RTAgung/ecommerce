package com.rama.ecommerce.data.repository

import androidx.paging.PagingData
import com.rama.ecommerce.data.model.DetailProduct
import com.rama.ecommerce.data.model.Product
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.model.Review
import com.rama.ecommerce.data.source.remote.request.ProductsQuery
import kotlinx.coroutines.flow.Flow

interface StoreRepository {
    fun getProducts(query: ProductsQuery): Flow<PagingData<Product>>
    fun searchProducts(query: String): Flow<ResultState<List<String>>>
    fun detailProducts(id: String): Flow<ResultState<DetailProduct>>
    fun reviewProducts(id: String): Flow<ResultState<List<Review>>>
}
