package com.rama.ecommerce.data.source.local.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.rama.ecommerce.data.source.local.room.entity.NotificationEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NotificationDao {
    @Query("SELECT * FROM notification")
    fun getData(): Flow<List<NotificationEntity>>

    @Query("SELECT COUNT(id) FROM notification WHERE is_read = 0")
    fun getDataSize(): Flow<Int>

    @Update
    suspend fun update(notification: NotificationEntity)

    @Insert
    suspend fun insert(notification: NotificationEntity)

    @Query("DELETE FROM notification")
    suspend fun clearTable()
}
