package com.rama.ecommerce.data.source.remote.request

data class AuthRequest(
    val email: String,
    val password: String,
    val firebaseToken: String
)
