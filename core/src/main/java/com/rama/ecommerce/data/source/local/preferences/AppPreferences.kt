package com.rama.ecommerce.data.source.local.preferences

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import com.rama.ecommerce.data.model.User
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class AppPreferences @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {
    suspend fun setFirstTimeRunApp(isFirstTime: Boolean) {
        dataStore.edit { preferences ->
            preferences[FIRST_TIME_RUNNER_KEY] = isFirstTime
        }
    }

    fun getFirstTimeRunApp(): Flow<Boolean> = dataStore.data.map { preferences ->
        preferences[FIRST_TIME_RUNNER_KEY] ?: true
    }

    suspend fun setUserDataSession(user: User) {
        dataStore.edit { preferences ->
            user.userName?.let { preferences[USER_NAME_KEY] = it }
            user.userImage?.let { preferences[USER_IMAGE_KEY] = it }
            user.accessToken?.let { preferences[USER_ACCESS_TOKEN_KEY] = it }
            user.refreshToken?.let { preferences[USER_REFRESH_TOKEN_KEY] = it }
            user.expiresAt?.let { preferences[USER_EXPIRES_KEY] = it }
        }
    }

    fun getUserDataSession(): Flow<User> = dataStore.data.map { preferences ->
        User(
            preferences[USER_NAME_KEY],
            preferences[USER_IMAGE_KEY],
            preferences[USER_ACCESS_TOKEN_KEY],
            preferences[USER_REFRESH_TOKEN_KEY],
            preferences[USER_EXPIRES_KEY]
        )
    }

    suspend fun setUserAuthorization(isLogin: Boolean) {
        dataStore.edit { preferences ->
            preferences[USER_AUTHORIZE_KEY] = isLogin
        }
    }

    fun checkUserAuthorization(): Flow<Boolean> = dataStore.data.map { preferences ->
        preferences[USER_AUTHORIZE_KEY] ?: false
    }

    suspend fun clearAllDataSession() {
        dataStore.edit { preferences ->
            preferences.remove(USER_NAME_KEY)
            preferences.remove(USER_IMAGE_KEY)
            preferences.remove(USER_ACCESS_TOKEN_KEY)
            preferences.remove(USER_REFRESH_TOKEN_KEY)
            preferences.remove(USER_EXPIRES_KEY)
            preferences.remove(USER_AUTHORIZE_KEY)
        }
    }

    suspend fun setAppTheme(isDarkTheme: Boolean) {
        dataStore.edit { preferences ->
            preferences[IS_DARK_THEME_KEY] = isDarkTheme
        }
    }

    fun getAppTheme(): Flow<Boolean> = dataStore.data.map { preferences ->
        preferences[IS_DARK_THEME_KEY] ?: false
    }

    suspend fun setAppLanguage(appLanguage: String?) {
        dataStore.edit { preferences ->
            preferences[APP_LANGUAGE_KEY] = appLanguage ?: "en"
        }
    }

    fun getAppLanguage(): Flow<String> = dataStore.data.map { preferences ->
        preferences[APP_LANGUAGE_KEY] ?: "en"
    }

    companion object {
        private val FIRST_TIME_RUNNER_KEY = booleanPreferencesKey("is_first_time")

        private val USER_NAME_KEY = stringPreferencesKey("user_name")
        private val USER_IMAGE_KEY = stringPreferencesKey("user_image")
        private val USER_ACCESS_TOKEN_KEY = stringPreferencesKey("user_access_token")
        private val USER_REFRESH_TOKEN_KEY = stringPreferencesKey("user_refresh_token")
        private val USER_EXPIRES_KEY = intPreferencesKey("user_expires")
        private val USER_AUTHORIZE_KEY = booleanPreferencesKey("user_authorize")

        private val IS_DARK_THEME_KEY = booleanPreferencesKey("is_dark_theme")
        private val APP_LANGUAGE_KEY = stringPreferencesKey("app_language")
    }
}
