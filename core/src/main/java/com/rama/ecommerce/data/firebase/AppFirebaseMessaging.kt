package com.rama.ecommerce.data.firebase

import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class AppFirebaseMessaging @Inject constructor(
    private val firebaseMessaging: FirebaseMessaging
) {
    suspend fun getToken(): String = firebaseMessaging.token.await()

    fun subscribeToTopic(topic: String) {
        firebaseMessaging.subscribeToTopic(topic)
    }

    fun unsubscribeFromTopic(topic: String) {
        firebaseMessaging.unsubscribeFromTopic(topic)
    }
}
