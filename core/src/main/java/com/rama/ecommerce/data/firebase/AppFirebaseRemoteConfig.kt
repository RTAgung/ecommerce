package com.rama.ecommerce.data.firebase

import com.google.firebase.remoteconfig.ConfigUpdate
import com.google.firebase.remoteconfig.ConfigUpdateListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigException
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rama.ecommerce.data.model.Payment
import com.rama.ecommerce.data.model.ResultState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class AppFirebaseRemoteConfig @Inject constructor(
    private val remoteConfig: FirebaseRemoteConfig
) {
    fun fetchPaymentMethod(): Flow<ResultState<List<Payment>>> = flow {
        emit(ResultState.Loading)
        try {
            remoteConfig.fetchAndActivate().await()

            val dataJson = remoteConfig.getString(REMOTE_DATA_KEY)
            val data = extractJsonToObject(dataJson)

            emit(ResultState.Success(data))
        } catch (e: Exception) {
            emit(ResultState.Error(e))
        }
    }

    fun onConfigUpdate(callback: (ResultState<List<Payment>>) -> Unit) {
        remoteConfig.addOnConfigUpdateListener(object : ConfigUpdateListener {
            override fun onUpdate(configUpdate: ConfigUpdate) {
                if (configUpdate.updatedKeys.contains("data")) {
                    remoteConfig.activate().addOnCompleteListener {
                        val dataJson = remoteConfig.getString(REMOTE_DATA_KEY)
                        val data = extractJsonToObject(dataJson)

                        callback(ResultState.Success(data))
                    }
                }
            }

            override fun onError(error: FirebaseRemoteConfigException) {
                callback(ResultState.Error(error))
            }
        })
    }

    private fun extractJsonToObject(dataJson: String): ArrayList<Payment> {
        return Gson().fromJson(
            dataJson,
            object : TypeToken<ArrayList<Payment>>() {}.type
        ) ?: ArrayList()
    }

    companion object {
        private const val REMOTE_DATA_KEY = "data"
    }
}
