package com.rama.ecommerce.data.source.remote.request

data class FulfillmentRequest(
    val payment: String,
    val items: List<Item>,
) {
    data class Item(
        val productId: String,
        val variantName: String,
        val quantity: Int
    )
}
