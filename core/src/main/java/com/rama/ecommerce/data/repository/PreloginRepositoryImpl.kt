package com.rama.ecommerce.data.repository

import com.rama.ecommerce.data.firebase.AppFirebaseMessaging
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.model.User
import com.rama.ecommerce.data.source.local.preferences.AppPreferences
import com.rama.ecommerce.data.source.local.room.dao.CartDao
import com.rama.ecommerce.data.source.local.room.dao.NotificationDao
import com.rama.ecommerce.data.source.local.room.dao.WishlistDao
import com.rama.ecommerce.data.source.remote.request.AuthRequest
import com.rama.ecommerce.data.source.remote.response.AuthResponse
import com.rama.ecommerce.data.source.remote.service.ApiService
import com.rama.ecommerce.utils.extension.toUser
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class PreloginRepositoryImpl @Inject constructor(
    private val appPreferences: AppPreferences,
    private val apiService: ApiService,
    private val wishlistDao: WishlistDao,
    private val cartDao: CartDao,
    private val notificationDao: NotificationDao,
    private val firebaseMessaging: AppFirebaseMessaging
) : PreloginRepository {
    override suspend fun setFirstTimeRunApp(isFirstTime: Boolean) {
        appPreferences.setFirstTimeRunApp(isFirstTime)
    }

    override fun getFirstTimeRunApp(): Flow<Boolean> = appPreferences.getFirstTimeRunApp()

    override fun register(email: String, password: String): Flow<ResultState<Boolean>> = flow {
        emit(ResultState.Loading)
        try {
            val firebaseToken = firebaseMessaging.getToken()
            val request = AuthRequest(email, password, firebaseToken)
            val response = apiService.register(request)
            val user = response.data?.toUser()
            if (user != null) {
                appPreferences.setUserDataSession(user)
                appPreferences.setUserAuthorization(true)
                firebaseMessaging.subscribeToTopic("promo")
                emit(ResultState.Success(true))
            } else {
                throw Exception("Failed")
            }
        } catch (e: Exception) {
            emit(ResultState.Error(e))
        }
    }

    override fun login(email: String, password: String): Flow<ResultState<Boolean>> = flow {
        emit(ResultState.Loading)
        try {
            val firebaseToken = firebaseMessaging.getToken()
            val request = AuthRequest(email, password, firebaseToken)
            val response = apiService.login(request)
            val user = response.data?.toUser()
            if (user != null) {
                appPreferences.setUserDataSession(user)
                appPreferences.setUserAuthorization(true)
                firebaseMessaging.subscribeToTopic("promo")
                emit(ResultState.Success(true))
            } else {
                throw Exception("Failed")
            }
        } catch (e: Exception) {
            emit(ResultState.Error(e))
        }
    }

    override suspend fun logout() {
        wishlistDao.clearTable()
        cartDao.clearTable()
        notificationDao.clearTable()
        firebaseMessaging.unsubscribeFromTopic("promo")
        appPreferences.clearAllDataSession()
    }

    override fun getUserDataSession(): Flow<User> = appPreferences.getUserDataSession()

    override fun checkUserAuthorization(): Flow<Boolean> = appPreferences.checkUserAuthorization()

    override fun uploadProfile(
        userName: RequestBody,
        userImage: MultipartBody.Part?
    ): Flow<ResultState<Boolean>> =
        flow {
            emit(ResultState.Loading)
            try {
                val response: AuthResponse = apiService.profile(userName, userImage)

                val user = response.data?.toUser()
                if (user != null) {
                    appPreferences.setUserDataSession(user)
                    emit(ResultState.Success(true))
                } else {
                    throw Exception("Failed")
                }
            } catch (e: Exception) {
                emit(ResultState.Error(e))
            }
        }

    override suspend fun setAppTheme(isDarkTheme: Boolean) {
        appPreferences.setAppTheme(isDarkTheme)
    }

    override fun getAppTheme(): Flow<Boolean> = appPreferences.getAppTheme()
    override suspend fun setAppLanguage(appLanguage: String?) {
        appPreferences.setAppLanguage(appLanguage)
    }

    override fun getAppLanguage(): Flow<String> = appPreferences.getAppLanguage()
}
