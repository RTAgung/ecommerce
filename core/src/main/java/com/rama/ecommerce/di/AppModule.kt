package com.rama.ecommerce.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStoreFile
import androidx.room.Room
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.chuckerteam.chucker.api.RetentionManager
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.remoteConfigSettings
import com.rama.ecommerce.R
import com.rama.ecommerce.data.firebase.AppFirebaseMessaging
import com.rama.ecommerce.data.firebase.AppFirebaseRemoteConfig
import com.rama.ecommerce.data.repository.CartRepository
import com.rama.ecommerce.data.repository.CartRepositoryImpl
import com.rama.ecommerce.data.repository.FulfillmentRepository
import com.rama.ecommerce.data.repository.FulfillmentRepositoryImpl
import com.rama.ecommerce.data.repository.NotificationRepository
import com.rama.ecommerce.data.repository.NotificationRepositoryImpl
import com.rama.ecommerce.data.repository.PreloginRepository
import com.rama.ecommerce.data.repository.PreloginRepositoryImpl
import com.rama.ecommerce.data.repository.StoreRepository
import com.rama.ecommerce.data.repository.StoreRepositoryImpl
import com.rama.ecommerce.data.repository.WishlistRepository
import com.rama.ecommerce.data.repository.WishlistRepositoryImpl
import com.rama.ecommerce.data.source.local.preferences.AppPreferences
import com.rama.ecommerce.data.source.local.room.AppDatabase
import com.rama.ecommerce.data.source.local.room.dao.CartDao
import com.rama.ecommerce.data.source.local.room.dao.NotificationDao
import com.rama.ecommerce.data.source.local.room.dao.WishlistDao
import com.rama.ecommerce.data.source.remote.service.ApiService
import com.rama.ecommerce.utils.Constant
import com.rama.ecommerce.utils.Constant.DATASTORE_NAME
import com.rama.ecommerce.utils.httpclient.HeaderInterceptor
import com.rama.ecommerce.utils.httpclient.SupportAuthenticator
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideDataStore(@ApplicationContext context: Context): DataStore<Preferences> =
        PreferenceDataStoreFactory.create(produceFile = {
            context.preferencesDataStoreFile(
                DATASTORE_NAME
            )
        })

    @Singleton
    @Provides
    fun providePreferences(dataStore: DataStore<Preferences>): AppPreferences =
        AppPreferences(dataStore)

    @Singleton
    @Provides
    fun provideChuckerInterceptor(@ApplicationContext context: Context): ChuckerInterceptor =
        ChuckerInterceptor.Builder(context)
            .collector(
                ChuckerCollector(
                    context = context,
                    showNotification = true,
                    retentionPeriod = RetentionManager.Period.ONE_HOUR
                )
            )
            .build()

    @Singleton
    @Provides
    fun provideHeaderInterceptor(appPreferences: AppPreferences): HeaderInterceptor =
        HeaderInterceptor(appPreferences)

    @Singleton
    @Provides
    fun provideSupportAuthenticator(
        @ApplicationContext context: Context,
        appPreferences: AppPreferences
    ): SupportAuthenticator =
        SupportAuthenticator(context, appPreferences)

    @Provides
    @Singleton
    fun provideOkHttpClient(
        chuckerInterceptor: ChuckerInterceptor,
        headerInterceptor: HeaderInterceptor,
        supportAuthenticator: SupportAuthenticator
    ): OkHttpClient = OkHttpClient.Builder()
        .apply {
            addInterceptor(chuckerInterceptor)
            addInterceptor(headerInterceptor)
            authenticator(supportAuthenticator)
        }
        .build()

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit = Retrofit.Builder().baseUrl(Constant.API_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase =
        Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            Constant.DATABASE_NAME
        ).allowMainThreadQueries().build()

    @Provides
    @Singleton
    fun provideWishlistDao(appDatabase: AppDatabase): WishlistDao = appDatabase.wishlistDao()

    @Provides
    @Singleton
    fun provideCartDao(appDatabase: AppDatabase): CartDao = appDatabase.cartDao()

    @Provides
    @Singleton
    fun provideNotificationDao(appDatabase: AppDatabase): NotificationDao =
        appDatabase.notificationDao()

    @Provides
    @Singleton
    fun provideFirebaseRemoteConfig(): FirebaseRemoteConfig =
        Firebase.remoteConfig.apply {
            val configSettings = remoteConfigSettings {
                minimumFetchIntervalInSeconds = 1
            }
            setConfigSettingsAsync(configSettings)
            setDefaultsAsync(R.xml.remote_config_defaults)
        }

    @Singleton
    @Provides
    fun provideAppFirebaseRemoteConfig(
        firebaseRemoteConfig: FirebaseRemoteConfig
    ): AppFirebaseRemoteConfig =
        AppFirebaseRemoteConfig(firebaseRemoteConfig)

    @Singleton
    @Provides
    fun provideAppFirebaseMessaging(): AppFirebaseMessaging =
        AppFirebaseMessaging(FirebaseMessaging.getInstance())

    @Singleton
    @Provides
    fun providePreloginRepository(
        appPreferences: AppPreferences,
        apiService: ApiService,
        wishlistDao: WishlistDao,
        cartDao: CartDao,
        notificationDao: NotificationDao,
        firebaseMessaging: AppFirebaseMessaging
    ): PreloginRepository =
        PreloginRepositoryImpl(
            appPreferences,
            apiService,
            wishlistDao,
            cartDao,
            notificationDao,
            firebaseMessaging
        )

    @Singleton
    @Provides
    fun provideStoreRepository(
        apiService: ApiService
    ): StoreRepository = StoreRepositoryImpl(apiService)

    @Singleton
    @Provides
    fun provideWishlistRepository(
        wishlistDao: WishlistDao
    ): WishlistRepository = WishlistRepositoryImpl(wishlistDao)

    @Singleton
    @Provides
    fun provideCartRepository(
        cartDao: CartDao
    ): CartRepository = CartRepositoryImpl(cartDao)

    @Singleton
    @Provides
    fun provideFulfillmentRepository(
        apiService: ApiService
    ): FulfillmentRepository = FulfillmentRepositoryImpl(apiService)

    @Singleton
    @Provides
    fun provideNotificationRepository(
        notificationDao: NotificationDao
    ): NotificationRepository = NotificationRepositoryImpl(notificationDao)
}
