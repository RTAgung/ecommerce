package com.rama.ecommerce.utils.extension

import com.rama.ecommerce.data.model.Cart
import com.rama.ecommerce.data.model.DetailProduct
import com.rama.ecommerce.data.model.Fulfillment
import com.rama.ecommerce.data.model.Notification
import com.rama.ecommerce.data.model.Product
import com.rama.ecommerce.data.model.Review
import com.rama.ecommerce.data.model.Transaction
import com.rama.ecommerce.data.model.User
import com.rama.ecommerce.data.model.Wishlist
import com.rama.ecommerce.data.source.local.room.entity.CartEntity
import com.rama.ecommerce.data.source.local.room.entity.NotificationEntity
import com.rama.ecommerce.data.source.local.room.entity.WishlistEntity
import com.rama.ecommerce.data.source.remote.request.FulfillmentRequest
import com.rama.ecommerce.data.source.remote.request.ProductsQuery
import com.rama.ecommerce.data.source.remote.response.AuthDataResponse
import com.rama.ecommerce.data.source.remote.response.FulfillmentResponseData
import com.rama.ecommerce.data.source.remote.response.ProductVariantItem
import com.rama.ecommerce.data.source.remote.response.ProductsDetailResponseData
import com.rama.ecommerce.data.source.remote.response.ProductsResponseItem
import com.rama.ecommerce.data.source.remote.response.ReviewResponseItem
import com.rama.ecommerce.data.source.remote.response.TransactionResponseData
import com.rama.ecommerce.data.source.remote.response.TransactionResponseItem
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

fun AuthDataResponse.toUser(): User =
    User(
        userName = userName,
        userImage = userImage,
        accessToken = accessToken,
        refreshToken = refreshToken,
        expiresAt = expiresAt
    )

fun File.toMultipartBody(field: String): MultipartBody.Part {
    val requestImageFile = this.asRequestBody("image/*".toMediaType())
    return MultipartBody.Part.createFormData(
        field,
        this.name,
        requestImageFile
    )
}

fun String.toBearerToken(): String = "Bearer $this"

fun ProductsQuery.toQueryMap(): Map<String, String> {
    val map = HashMap<String, String>()
    search?.let { map.put("search", it) }
    brand?.let { map.put("brand", it) }
    lowest?.let { map.put("lowest", it) }
    highest?.let { map.put("highest", it) }
    sort?.let { map.put("sort", it) }
    limit?.let { map.put("limit", it.toString()) }
    page?.let { map.put("page", it.toString()) }
    return map
}

fun ProductsResponseItem.toProduct(): Product =
    Product(
        productId ?: "",
        productName ?: "",
        productPrice ?: 0,
        image ?: "",
        brand ?: "",
        store ?: "",
        sale ?: 0,
        productRating ?: 0F
    )

fun ProductsDetailResponseData.toDetailProduct(): DetailProduct =
    DetailProduct(
        image ?: emptyList(),
        productId ?: "",
        description ?: "",
        totalRating ?: 0,
        store ?: "",
        productName ?: "",
        totalSatisfaction ?: 0,
        sale ?: 0,
        productVariant?.map { it.toProductVariant() } ?: emptyList(),
        stock ?: 0,
        productRating ?: 0F,
        brand ?: "",
        productPrice ?: 0,
        totalReview ?: 0
    )

private fun ProductVariantItem.toProductVariant(): DetailProduct.ProductVariant =
    DetailProduct.ProductVariant(
        variantPrice ?: 0,
        variantName ?: ""
    )

fun ReviewResponseItem.toReview(): Review =
    Review(
        userImage ?: "",
        userName ?: "",
        userReview ?: "",
        userRating ?: 0
    )

fun WishlistEntity.toWishlist(): Wishlist =
    Wishlist(
        productId,
        productName,
        productPrice,
        image,
        stock,
        store,
        sale,
        productRating,
        description,
        totalRating,
        totalSatisfaction,
        brand,
        totalReview,
        variantName,
        variantPrice
    )

fun Wishlist.toWishlistEntity(): WishlistEntity =
    WishlistEntity(
        productId,
        productName,
        productPrice,
        image,
        stock,
        store,
        sale,
        productRating,
        description,
        totalRating,
        totalSatisfaction,
        brand,
        totalReview,
        variantName,
        variantPrice
    )

fun Cart.toCartEntity(): CartEntity =
    CartEntity(
        productId,
        productName,
        productPrice,
        image,
        stock,
        store,
        sale,
        productRating,
        description,
        totalRating,
        totalSatisfaction,
        brand,
        totalReview,
        variantName,
        variantPrice,
        quantity ?: 1,
        isChecked,
    )

fun CartEntity.toCart(): Cart =
    Cart(
        productId,
        productName,
        productPrice,
        image,
        stock,
        store,
        sale,
        productRating,
        description,
        totalRating,
        totalSatisfaction,
        brand,
        totalReview,
        variantName,
        variantPrice,
        quantity,
        isChecked,
    )

fun DetailProduct.toWishlist(variant: DetailProduct.ProductVariant): Wishlist =
    Wishlist(
        productId,
        productName,
        productPrice,
        image[0],
        stock,
        store,
        sale,
        productRating,
        description,
        totalRating,
        totalSatisfaction,
        brand,
        totalReview,
        variant.variantName,
        variant.variantPrice
    )

fun DetailProduct.toCart(variant: DetailProduct.ProductVariant): Cart =
    Cart(
        productId,
        productName,
        productPrice,
        image[0],
        stock,
        store,
        sale,
        productRating,
        description,
        totalRating,
        totalSatisfaction,
        brand,
        totalReview,
        variant.variantName,
        variant.variantPrice,
    )

fun Wishlist.toCart(): Cart =
    Cart(
        productId,
        productName,
        productPrice,
        image,
        stock,
        store,
        sale,
        productRating,
        description,
        totalRating,
        totalSatisfaction,
        brand,
        totalReview,
        variantName,
        variantPrice,
    )

fun FulfillmentResponseData.toFulfillment(): Fulfillment =
    Fulfillment(
        date ?: "",
        total ?: 0,
        invoiceId ?: "",
        payment ?: "",
        time ?: "",
        status ?: false
    )

fun Cart.toFulfillmentRequestItem(): FulfillmentRequest.Item =
    FulfillmentRequest.Item(
        productId,
        variantName,
        quantity ?: 1
    )

fun TransactionResponseData.toTransaction(): Transaction =
    Transaction(
        date ?: "",
        image ?: "",
        total ?: 0,
        review ?: "",
        rating ?: 0,
        name ?: "",
        invoiceId ?: "",
        payment ?: "",
        time ?: "",
        items?.map { it.toTransactionItem() } ?: ArrayList(),
        status ?: false
    )

fun TransactionResponseItem.toTransactionItem(): Transaction.TransactionItem =
    Transaction.TransactionItem(
        quantity ?: 0,
        productId ?: "",
        variantName ?: ""
    )

fun Transaction.toFulfillment(): Fulfillment =
    Fulfillment(
        date,
        total,
        invoiceId,
        payment,
        time,
        status
    )

fun NotificationEntity.toNotification(): Notification =
    Notification(
        id,
        image,
        title,
        body,
        type,
        date,
        time,
        isRead,
    )

fun Notification.toNotificationEntity(): NotificationEntity =
    NotificationEntity(
        id,
        image,
        title,
        body,
        type,
        date,
        time,
        isRead
    )
