package com.rama.ecommerce.utils

import com.rama.ecommerce.BuildConfig

object Constant {
    const val DATASTORE_NAME = "app_preferences"

    const val DATABASE_NAME = "app_database"

    const val API_BASE_URL = BuildConfig.BASE_URL
    const val API_KEY = BuildConfig.API_KEY

    val PRELOGIN_ENDPOINT = listOf(
        "${API_BASE_URL}login",
        "${API_BASE_URL}register",
        "${API_BASE_URL}refresh"
    )

    const val THRESHOLD_STOCK = 10
    const val TRANSPARENT_ALPHA = 0.5F
    const val MAX_RATING = 5
    const val DEBOUNCE_DURATION = 1000L
    const val MINIMUM_PASSWORD = 8
    const val IMAGE_COMPRESS_QUALITY = 100
    const val IMAGE_COMPRESS_STEP = 5
    const val DIVIDER_THICKNESS = 12
    const val SNACKBAR_DELAY = 300L
}
