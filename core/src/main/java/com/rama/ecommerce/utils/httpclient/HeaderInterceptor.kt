package com.rama.ecommerce.utils.httpclient

import com.rama.ecommerce.data.source.local.preferences.AppPreferences
import com.rama.ecommerce.utils.Constant
import com.rama.ecommerce.utils.extension.toBearerToken
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class HeaderInterceptor @Inject constructor(private val appPreferences: AppPreferences) :
    Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val requestUrl = originalRequest.url.toString()
        val isPreloginRequest = Constant.PRELOGIN_ENDPOINT.contains(requestUrl)

        val newRequest = originalRequest.newBuilder().apply {
            if (isPreloginRequest) {
                addHeader("API_KEY", Constant.API_KEY)
            } else {
                val user = runBlocking { appPreferences.getUserDataSession().first() }
                val bearerToken: String = user.accessToken?.toBearerToken() ?: ""
                addHeader("Authorization", bearerToken)
            }
        }.build()
        return chain.proceed(newRequest)
    }
}
