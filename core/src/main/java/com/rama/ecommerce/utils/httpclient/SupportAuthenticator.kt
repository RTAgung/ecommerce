package com.rama.ecommerce.utils.httpclient

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.chuckerteam.chucker.api.RetentionManager
import com.rama.ecommerce.data.model.User
import com.rama.ecommerce.data.source.local.preferences.AppPreferences
import com.rama.ecommerce.data.source.remote.request.RefreshRequest
import com.rama.ecommerce.data.source.remote.service.ApiService
import com.rama.ecommerce.utils.Constant
import com.rama.ecommerce.utils.extension.toBearerToken
import com.rama.ecommerce.utils.extension.toUser
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class SupportAuthenticator @Inject constructor(
    private val context: Context,
    private val appPreferences: AppPreferences
) : Authenticator {

    private val apiService: ApiService by lazy {
        val okHttpClient = OkHttpClient.Builder()
            .apply {
                addInterceptor(
                    ChuckerInterceptor.Builder(context)
                        .collector(
                            ChuckerCollector(
                                context = context,
                                showNotification = true,
                                retentionPeriod = RetentionManager.Period.ONE_HOUR
                            )
                        )
                        .build()
                )
                addInterceptor(HeaderInterceptor(appPreferences))
            }
            .build()

        val retrofit = Retrofit.Builder().baseUrl(Constant.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build()

        val apiService = retrofit.create(ApiService::class.java)

        apiService
    }

    override fun authenticate(route: Route?, response: Response): Request? {
        synchronized(this) {
            try {
                val newToken = getNewToken()

                return response.request.newBuilder()
                    .header("Authorization", newToken.toBearerToken())
                    .build()
            } catch (_: Exception) {
                runBlocking { appPreferences.setUserAuthorization(false) }
            }

            return null
        }
    }

    private fun getNewToken(): String {
        val user =
            runBlocking { appPreferences.getUserDataSession().first() }
        val refreshToken = user.refreshToken ?: ""

        val newToken = runBlocking {
            val refreshResponse = apiService.refresh(RefreshRequest(refreshToken))
            appPreferences.setUserDataSession(refreshResponse.data?.toUser() ?: User())

            return@runBlocking appPreferences.getUserDataSession().first<User>().accessToken
        }

        return newToken ?: ""
    }
}
