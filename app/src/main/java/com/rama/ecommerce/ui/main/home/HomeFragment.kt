package com.rama.ecommerce.ui.main.home

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.fragment.app.viewModels
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.databinding.FragmentHomeBinding
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.Helper
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    private val viewModel: HomeViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initSwitchButton()
        setAction()
        playAnimation()
    }

    private fun playAnimation() {
        val listLayout = listOf(
            binding.animHello,
            binding.btnLogout,
            binding.switchLangLayout,
            binding.switchThemeLayout
        )
        val animSet = AnimatorSet()
        var animBefore: ObjectAnimator? = null
        listLayout.reversed().forEach { view ->
            val animAlpha = ObjectAnimator.ofFloat(view, View.ALPHA, 1f).apply {
                duration = 300
                interpolator = AccelerateInterpolator()
            }
            val animTranslation = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, 1f).apply {
                duration = 300
                interpolator = DecelerateInterpolator()
            }
            if (animBefore != null) {
                animSet.play(animAlpha).before(animBefore)
            } else {
                animSet.play(animAlpha)
            }
            animSet.play(animTranslation).with(animAlpha)
            animBefore = animAlpha
        }
        animSet.start()
    }

    private fun initSwitchButton() {
        binding.switchLang.isChecked = runBlocking {
            viewModel.getAppLanguage().first() == "id"
        }
        binding.switchTheme.isChecked = runBlocking {
            viewModel.getAppTheme().first()
        }
    }

    private fun setAction() {
        binding.btnLogout.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Home: Logout")
            viewModel.makeLogout()
        }
        binding.switchTheme.setOnCheckedChangeListener { _, isDarkTheme ->
            firebaseAnalytics.sendLogButtonClicked("Home: Theme")
            viewModel.setAppTheme(isDarkTheme)
            Helper.setAppTheme(requireActivity(), isDarkTheme)
        }
        binding.switchLang.setOnCheckedChangeListener { _, isChecked ->
            firebaseAnalytics.sendLogButtonClicked("Home: Language")
            viewModel.setAppLanguage(isChecked)
            val appLanguage = if (isChecked) "id" else "en"
            Helper.setAppLanguage(appLanguage)
        }
    }
}
