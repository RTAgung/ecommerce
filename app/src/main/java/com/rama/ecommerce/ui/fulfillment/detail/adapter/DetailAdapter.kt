package com.rama.ecommerce.ui.fulfillment.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rama.ecommerce.R
import com.rama.ecommerce.databinding.DetailItemImageBinding

class DetailAdapter :
    ListAdapter<String, DetailAdapter.DetailViewHolder>(DetailImageComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailViewHolder {
        val binding =
            DetailItemImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DetailViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DetailViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class DetailViewHolder(private val binding: DetailItemImageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: String) {
            binding.ivDetailImage.load(data) {
                crossfade(true)
                placeholder(R.drawable.image_placeholder)
                error(R.drawable.image_placeholder)
            }
        }
    }
}
