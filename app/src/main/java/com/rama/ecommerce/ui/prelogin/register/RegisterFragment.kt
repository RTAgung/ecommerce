package com.rama.ecommerce.ui.prelogin.register

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.core.view.isInvisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import com.google.firebase.analytics.FirebaseAnalytics.Param
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.databinding.FragmentRegisterBinding
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.FormValidation
import com.rama.ecommerce.utils.Helper
import com.rama.ecommerce.utils.extension.getErrorMessage
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RegisterFragment : BaseFragment<FragmentRegisterBinding>(FragmentRegisterBinding::inflate) {

    private val viewModel: RegisterViewModel by viewModels()
    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAppbar()
        setPrivacyPolicyText()
        setFormValidation()
        setAction()
        observeRegister()
        playAnimation()
    }

    private fun playAnimation() {
        val listLayout = listOf(
            binding.layoutTextEmail,
            binding.layoutTextPassword,
            binding.btnRegister,
            binding.textOptionLayout,
            binding.btnLogin,
            binding.tvPrivacyPolicy,
        )
        val animSet = AnimatorSet()
        var animBefore: ObjectAnimator? = null
        listLayout.reversed().forEach { view ->
            val animAlpha = ObjectAnimator.ofFloat(view, View.ALPHA, 1f).apply {
                duration = 300
                interpolator = AccelerateInterpolator()
            }
            val animTranslation = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, 1f).apply {
                duration = 300
                interpolator = DecelerateInterpolator()
            }
            if (animBefore != null) {
                animSet.play(animAlpha).before(animBefore)
            } else {
                animSet.play(animAlpha)
            }
            animSet.play(animTranslation).with(animAlpha)
            animBefore = animAlpha
        }
        animSet.start()
    }

    private fun setAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = resources.getString(R.string.register)
        toolbar.isTitleCentered = true
    }

    private fun observeRegister() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.registerState.collect { resultState ->
                    if (resultState != null) {
                        when (resultState) {
                            is ResultState.Loading -> {
                                showRegisterLoading(true)
                            }

                            is ResultState.Success -> {
                                showRegisterLoading(false)
                                val isSuccessful = resultState.data
                                if (isSuccessful) {
                                    firebaseAnalytics.logEvent(Event.SIGN_UP) {
                                        param(Param.METHOD, "email")
                                    }
                                    findNavController().navigate(R.id.action_global_main_navigation)
                                }
                            }

                            is ResultState.Error -> {
                                showRegisterLoading(false)
                                val errorMessage = resultState.error.getErrorMessage()
                                binding.root.showSnackbar(errorMessage)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun showRegisterLoading(isLoading: Boolean) {
        binding.loadingRegister.isInvisible = !isLoading
        binding.btnRegister.isInvisible = isLoading
    }

    private fun setAction() {
        binding.btnRegister.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Register: Register")
            makeRegister()
        }
        binding.btnLogin.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Register: Login")
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
    }

    private fun makeRegister() {
        val email = binding.editTextEmail.text.toString()
        val password = binding.editTextPassword.text.toString()
        viewModel.makeRegister(email, password)
    }

    private fun setFormValidation() {
        with(binding) {
            editTextEmail.doOnTextChanged { text, _, _, _ ->
                val email = text.toString()
                layoutTextEmail.error =
                    if (!FormValidation.isEmailValid(email) && email.isNotEmpty()) {
                        getString(R.string.email_invalid)
                    } else {
                        null
                    }
                setButtonEnable()
            }
            editTextPassword.doOnTextChanged { text, _, _, _ ->
                val password = text.toString()
                layoutTextPassword.error =
                    if (!FormValidation.isPasswordValid(password) && password.isNotEmpty()) {
                        getString(R.string.password_invalid)
                    } else {
                        null
                    }
                setButtonEnable()
            }
        }
    }

    private fun setButtonEnable() {
        val textEmail = binding.editTextEmail.text.toString()
        val textPassword = binding.editTextPassword.text.toString()
        binding.btnRegister.isEnabled =
            FormValidation.isEmailValid(textEmail) && FormValidation.isPasswordValid(
                textPassword
            )
    }

    private fun setPrivacyPolicyText() {
        binding.tvPrivacyPolicy.text = Helper.getTncPrivacyPolicySpanText(requireActivity())
    }
}
