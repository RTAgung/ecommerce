package com.rama.ecommerce.ui.fulfillment.detail.review.adapter

import androidx.recyclerview.widget.DiffUtil
import com.rama.ecommerce.data.model.Review

object ReviewComparator : DiffUtil.ItemCallback<Review>() {
    override fun areItemsTheSame(oldItem: Review, newItem: Review): Boolean =
        oldItem.userName == newItem.userName

    override fun areContentsTheSame(oldItem: Review, newItem: Review): Boolean =
        oldItem == newItem
}
