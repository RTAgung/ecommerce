package com.rama.ecommerce.ui.main.store

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.rama.ecommerce.data.model.Product
import com.rama.ecommerce.data.repository.StoreRepository
import com.rama.ecommerce.data.source.remote.request.ProductsQuery
import com.rama.ecommerce.ui.main.store.adapter.ProductAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(
    private val storeRepository: StoreRepository
) : ViewModel() {

    var recyclerViewType = ProductAdapter.ONE_COLUMN_VIEW_TYPE

    var resSortFilterProduct: Int? = null
    var resBrandFilterProduct: Int? = null

    var productsQuery: MutableStateFlow<ProductsQuery> = MutableStateFlow(ProductsQuery())

    val productsData: Flow<PagingData<Product>> = productsQuery.flatMapLatest { query ->
        storeRepository.getProducts(query)
    }.cachedIn(viewModelScope)

    fun getFilterData(query: ProductsQuery) {
        viewModelScope.launch {
            productsQuery.update {
                query.copy(search = it.search)
            }
        }
    }

    fun getSearchData(query: String?) {
        viewModelScope.launch {
            productsQuery.update {
                it.copy(search = query)
            }
        }
    }

    fun resetData() {
        viewModelScope.launch {
            productsQuery.value = ProductsQuery()
            resSortFilterProduct = null
            resBrandFilterProduct = null
        }
    }
}
