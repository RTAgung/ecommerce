package com.rama.ecommerce.ui.prelogin.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rama.ecommerce.databinding.OnboardingItemImageBinding

class OnboardingAdapter :
    ListAdapter<Int, OnboardingAdapter.OnboardingViewHolder>(OnboardingDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnboardingViewHolder {
        val binding =
            OnboardingItemImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OnboardingViewHolder(binding)
    }

    override fun onBindViewHolder(holder: OnboardingViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class OnboardingViewHolder(private val binding: OnboardingItemImageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Int) {
            binding.ivItemOnboarding.setImageResource(data)
        }
    }

    class OnboardingDiffUtil : DiffUtil.ItemCallback<Int>() {
        override fun areItemsTheSame(oldItem: Int, newItem: Int): Boolean = oldItem == newItem

        override fun areContentsTheSame(oldItem: Int, newItem: Int): Boolean = oldItem == newItem
    }
}
