package com.rama.ecommerce.ui.fulfillment.notification.adapter

import androidx.recyclerview.widget.DiffUtil
import com.rama.ecommerce.data.model.Notification

object NotificationComparator : DiffUtil.ItemCallback<Notification>() {
    override fun areItemsTheSame(oldItem: Notification, newItem: Notification): Boolean {
        val title = oldItem.title == newItem.title
        val body = oldItem.body == newItem.body
        val date = oldItem.date == newItem.date
        val time = oldItem.time == newItem.time
        return title && body && date && time
    }

    override fun areContentsTheSame(oldItem: Notification, newItem: Notification): Boolean {
        return oldItem == newItem
    }
}
