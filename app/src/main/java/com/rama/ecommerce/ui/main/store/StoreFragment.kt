package com.rama.ecommerce.ui.main.store

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.Navigation
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import com.google.firebase.analytics.FirebaseAnalytics.Param
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.Product
import com.rama.ecommerce.data.source.remote.request.ProductsQuery
import com.rama.ecommerce.databinding.FragmentStoreBinding
import com.rama.ecommerce.ui.fulfillment.detail.DetailFragmentCompose
import com.rama.ecommerce.ui.main.store.adapter.LoadingStateAdapter
import com.rama.ecommerce.ui.main.store.adapter.ProductAdapter
import com.rama.ecommerce.ui.main.store.search.SearchProductFragment
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.extension.getErrorAction
import com.rama.ecommerce.utils.extension.getErrorSubtitle
import com.rama.ecommerce.utils.extension.getErrorTitle
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class StoreFragment : BaseFragment<FragmentStoreBinding>(FragmentStoreBinding::inflate) {

    private val viewModel: StoreViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    private val productAdapter by lazy {
        ProductAdapter { product ->
            sendLogSelectItem(product)
            Navigation.findNavController(requireActivity(), R.id.nav_host_fragment).navigate(
                R.id.action_mainFragment_to_detailFragmentCompose,
                bundleOf(DetailFragmentCompose.BUNDLE_PRODUCT_ID_KEY to product.productId)
            )
        }
    }

    private fun sendLogSelectItem(product: Product) {
        firebaseAnalytics.logEvent(Event.SELECT_ITEM) {
            val bundleProduct = Bundle().apply {
                putString(Param.ITEM_ID, product.productId)
                putString(Param.ITEM_NAME, product.productName)
                putString(Param.ITEM_BRAND, product.brand)
            }
            param(Param.ITEMS, arrayOf(bundleProduct))
        }
    }

    private val loadingStateAdapter = LoadingStateAdapter()

    private lateinit var gridLayoutManager: GridLayoutManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupFragmentResultListener()
        setupView()
        setupAction()
        observeProducts()
    }

    private fun setupView() {
        setAdapter()
        setImageBtnChangeRecyclerView()
        changeRecyclerViewType()
        setChipView()
        setSearchEditTextView()
    }

    private fun setAdapter() {
        gridLayoutManager = GridLayoutManager(requireActivity(), 1)

        binding.includeContent.rvStore.adapter =
            productAdapter.withLoadStateFooter(loadingStateAdapter)
        binding.includeContent.rvStore.layoutManager = gridLayoutManager

        productAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                super.onItemRangeMoved(fromPosition, toPosition, itemCount)
                binding.includeContent.rvStore.scrollToPosition(0)
            }

            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                sendLogViewItemList(productAdapter.snapshot().items)
            }
        })

        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (viewModel.recyclerViewType == ProductAdapter.MORE_COLUMN_VIEW_TYPE) {
                    if (position == productAdapter.itemCount && loadingStateAdapter.itemCount > 0) {
                        2
                    } else {
                        1
                    }
                } else {
                    1
                }
            }
        }
    }

    private fun setSearchEditTextView() {
        binding.editTextSearch.setText(viewModel.productsQuery.value.search ?: "")
    }

    private fun setupFragmentResultListener() {
        getFilterFragmentResult()
        getSearchFragmentResult()
    }

    private fun getSearchFragmentResult() {
        childFragmentManager.setFragmentResultListener(
            SearchProductFragment.FRAGMENT_REQUEST_KEY,
            viewLifecycleOwner
        ) { _, bundle ->
            val query = bundle.getString(SearchProductFragment.BUNDLE_QUERY_KEY)
            viewModel.getSearchData(query)
            setSearchEditTextView()
        }
    }

    private fun getFilterFragmentResult() {
        childFragmentManager.setFragmentResultListener(
            FilterSheetFragment.FRAGMENT_REQUEST_KEY,
            viewLifecycleOwner
        ) { _, bundle ->
            val productFilter = ProductsQuery(
                sort = bundle.getString(FilterSheetFragment.BUNDLE_SORT_KEY),
                brand = bundle.getString(FilterSheetFragment.BUNDLE_BRAND_KEY),
                lowest = bundle.getString(FilterSheetFragment.BUNDLE_LOWEST_KEY),
                highest = bundle.getString(FilterSheetFragment.BUNDLE_HIGHEST_KEY)
            )
            viewModel.resSortFilterProduct =
                bundle.getInt(FilterSheetFragment.BUNDLE_SORT_RES_ID_KEY, -1).let {
                    if (it == -1) {
                        null
                    } else {
                        it
                    }
                }
            viewModel.resBrandFilterProduct =
                bundle.getInt(FilterSheetFragment.BUNDLE_BRAND_RES_ID_KEY, -1).let {
                    if (it == -1) {
                        null
                    } else {
                        it
                    }
                }

            viewModel.getFilterData(productFilter)
            setChipView()
        }
    }

    private fun setupAction() {
        binding.includeContent.btnChangeRecyclerView.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Store: Change Recycler View")
            actionChangeRecyclerView()
        }
        binding.includeContent.chipFilter.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Store: Filter")
            actionOpenFilter()
        }
        binding.editTextSearch.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Store: Search")
            actionOpenSearch()
        }
        binding.swipeRefresh.setOnRefreshListener {
            firebaseAnalytics.sendLogButtonClicked("Store: Refresh")
            productAdapter.refresh()
            binding.swipeRefresh.isRefreshing = false
        }
    }

    private fun actionOpenSearch() {
        val searchFragment = SearchProductFragment.newInstance(viewModel.productsQuery.value.search)
        searchFragment.show(childFragmentManager, SearchProductFragment.TAG)
    }

    private fun actionOpenFilter() {
        with(viewModel.productsQuery.value) {
            val filterSheetFragment = FilterSheetFragment.newInstance(
                viewModel.resSortFilterProduct?.let { getString(it) },
                viewModel.resBrandFilterProduct?.let { getString(it) },
                lowest,
                highest
            )
            filterSheetFragment.show(childFragmentManager, FilterSheetFragment.TAG)
        }
    }

    private fun actionChangeRecyclerView() {
        viewModel.recyclerViewType =
            if (viewModel.recyclerViewType == ProductAdapter.ONE_COLUMN_VIEW_TYPE) {
                ProductAdapter.MORE_COLUMN_VIEW_TYPE
            } else {
                ProductAdapter.ONE_COLUMN_VIEW_TYPE
            }
        changeRecyclerViewType()
        setImageBtnChangeRecyclerView()
    }

    private fun setImageBtnChangeRecyclerView() {
        binding.includeContent.btnChangeRecyclerView.setImageResource(
            if (viewModel.recyclerViewType == ProductAdapter.ONE_COLUMN_VIEW_TYPE) {
                R.drawable.ic_grid_view_24
            } else {
                R.drawable.ic_format_list_bulleted_24
            }
        )
    }

    private fun changeRecyclerViewType() {
        if (viewModel.recyclerViewType == ProductAdapter.ONE_COLUMN_VIEW_TYPE) {
            productAdapter.viewType = ProductAdapter.ONE_COLUMN_VIEW_TYPE
            gridLayoutManager.spanCount = 1
        } else {
            productAdapter.viewType = ProductAdapter.MORE_COLUMN_VIEW_TYPE
            gridLayoutManager.spanCount = 2
        }
    }

    private fun observeProducts() {
        productAdapter.addLoadStateListener { loadState ->
            val state = loadState.refresh
            showShimmerLoading(state is LoadState.Loading)
            if (state is LoadState.Error) showErrorView(state.error)
        }
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.productsData.collect { pagingData ->
                    productAdapter.submitData(pagingData)
                }
            }
        }
    }

    private fun sendLogViewItemList(productList: List<Product>) {
        firebaseAnalytics.logEvent(Event.VIEW_ITEM_LIST) {
            val bundle = ArrayList<Bundle>()
            productList.map { product ->
                val itemBundle = Bundle().apply {
                    putString(Param.ITEM_ID, product.productId)
                    putString(Param.ITEM_NAME, product.productName)
                    putString(Param.ITEM_BRAND, product.brand)
                }
                bundle.add(itemBundle)
            }
            param(Param.ITEMS, bundle.toTypedArray())
        }
    }

    private fun showErrorView(error: Throwable) {
        val title = error.getErrorTitle(requireActivity())
        val subtitle = error.getErrorSubtitle(requireActivity())
        val action = error.getErrorAction(requireActivity())

        binding.includeContent.layoutContent.isVisible = false
        with(binding.includeError) {
            layoutError.isVisible = true

            tvErrorTitle.text = title
            tvErrorSubtitle.text = subtitle
            btnErrorAction.text = action

            btnErrorAction.setOnClickListener {
                when (action) {
                    getString(R.string.reset_error_action) -> {
                        viewModel.resetData()
                        setChipView()
                        setSearchEditTextView()
                    }

                    getString(R.string.refresh_error_action) -> {
                        productAdapter.refresh()
                    }
                }
            }
        }
    }

    private fun showShimmerLoading(isLoading: Boolean) {
        with(binding) {
            includeLoading.shimmerList.isVisible =
                viewModel.recyclerViewType == ProductAdapter.ONE_COLUMN_VIEW_TYPE
            includeLoading.shimmerGrid.isVisible =
                viewModel.recyclerViewType == ProductAdapter.MORE_COLUMN_VIEW_TYPE

            includeLoading.loadingShimmer.isVisible = isLoading
            includeContent.layoutContent.isVisible = !isLoading
            includeError.layoutError.isVisible = false

            if (isLoading) {
                includeLoading.loadingShimmer.startShimmer()
            } else if (includeLoading.loadingShimmer.isShimmerStarted) {
                includeLoading.loadingShimmer.stopShimmer()
            }
        }
    }

    private fun setChipView() {
        binding.includeContent.chipFilterGroup.removeAllViews()

        val filterList = arrayListOf<String>()
        with(viewModel.productsQuery.value) {
            this.sort?.let { filterList.add(getString(viewModel.resSortFilterProduct!!)) }
            this.brand?.let { filterList.add(getString(viewModel.resBrandFilterProduct!!)) }
            this.lowest?.let { filterList.add("> ${it.toInt().toCurrencyFormat()}") }
            this.highest?.let { filterList.add("< ${it.toInt().toCurrencyFormat()}") }
        }

        for (filter in filterList) {
            val chip = Chip(binding.includeContent.chipFilterGroup.context)
            chip.setTextAppearanceResource(R.style.TextAppearance_Medium)
            chip.text = filter
            chip.isClickable = false
            chip.isCheckable = false
            binding.includeContent.chipFilterGroup.addView(chip)
        }
    }
}
