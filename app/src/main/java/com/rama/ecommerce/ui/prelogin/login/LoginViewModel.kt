package com.rama.ecommerce.ui.prelogin.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.PreloginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val preloginRepository: PreloginRepository) :
    ViewModel() {
    private val _loginState = MutableStateFlow<ResultState<Boolean>?>(null)
    val loginState: StateFlow<ResultState<Boolean>?> = _loginState

    fun makeLogin(email: String, password: String) {
        viewModelScope.launch {
            preloginRepository.login(email, password).collect { resultState ->
                _loginState.value = resultState
            }
        }
    }

    fun getFirstTimeRunApp() = preloginRepository.getFirstTimeRunApp()
}
