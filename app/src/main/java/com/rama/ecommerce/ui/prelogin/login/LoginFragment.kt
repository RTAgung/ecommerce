package com.rama.ecommerce.ui.prelogin.login

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.core.view.isInvisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import com.google.firebase.analytics.FirebaseAnalytics.Param
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.databinding.FragmentLoginBinding
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.FormValidation.isEmailValid
import com.rama.ecommerce.utils.FormValidation.isPasswordValid
import com.rama.ecommerce.utils.Helper
import com.rama.ecommerce.utils.extension.getErrorMessage
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {

    private val viewModel: LoginViewModel by viewModels()
    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkShowOnboardingPage()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAppbar()
        setPrivacyPolicyText()
        setFormValidation()
        setAction()
        observeLogin()
        playAnimation()
    }

    private fun playAnimation() {
        val listLayout = listOf(
            binding.layoutTextEmail,
            binding.layoutTextPassword,
            binding.btnLogin,
            binding.textOptionLayout,
            binding.btnRegister,
            binding.tvPrivacyPolicy,
        )
        val animSet = AnimatorSet()
        var animBefore: ObjectAnimator? = null
        listLayout.reversed().forEach { view ->
            val animAlpha = ObjectAnimator.ofFloat(view, View.ALPHA, 1f).apply {
                duration = 300
                interpolator = AccelerateInterpolator()
            }
            val animTranslation = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, 1f).apply {
                duration = 300
                interpolator = DecelerateInterpolator()
            }
            if (animBefore != null) {
                animSet.play(animAlpha).before(animBefore)
            } else {
                animSet.play(animAlpha)
            }
            animSet.play(animTranslation).with(animAlpha)
            animBefore = animAlpha
        }
        animSet.start()
    }

    private fun setAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = resources.getString(R.string.login)
        toolbar.isTitleCentered = true
    }

    private fun observeLogin() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.loginState.collect { resultState ->
                    if (resultState != null) {
                        when (resultState) {
                            is ResultState.Loading -> {
                                showLoginLoading(true)
                            }

                            is ResultState.Success -> {
                                showLoginLoading(false)
                                val isSuccessful = resultState.data
                                if (isSuccessful) {
                                    firebaseAnalytics.logEvent(Event.LOGIN) {
                                        param(Param.METHOD, "email")
                                    }
                                    findNavController().navigate(R.id.action_global_main_navigation)
                                }
                            }

                            is ResultState.Error -> {
                                showLoginLoading(false)
                                val errorMessage = resultState.error.getErrorMessage()
                                binding.root.showSnackbar(errorMessage)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun checkShowOnboardingPage() {
        val isFirstTime = runBlocking { viewModel.getFirstTimeRunApp().first() }
        if (isFirstTime) {
            findNavController()
                .navigate(R.id.action_loginFragment_to_onboardingFragment)
        }
    }

    private fun setAction() {
        binding.btnLogin.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Login: Login")
            makeLogin()
        }
        binding.btnRegister.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Login: Register")
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    private fun makeLogin() {
        val email = binding.editTextEmail.text.toString()
        val password = binding.editTextPassword.text.toString()
        viewModel.makeLogin(email, password)
    }

    private fun showLoginLoading(isLoading: Boolean) {
        binding.loadingLogin.isInvisible = !isLoading
        binding.btnLogin.isInvisible = isLoading
    }

    private fun setFormValidation() {
        binding.editTextEmail.doOnTextChanged { text, _, _, _ ->
            val email = text.toString()
            binding.layoutTextEmail.error =
                if (!isEmailValid(email) && email.isNotEmpty()) getString(R.string.email_invalid) else null
            setButtonEnable()
        }
        binding.editTextPassword.doOnTextChanged { text, _, _, _ ->
            val password = text.toString()
            binding.layoutTextPassword.error =
                if (!isPasswordValid(password) && password.isNotEmpty()) getString(R.string.password_invalid) else null
            setButtonEnable()
        }
    }

    private fun setButtonEnable() {
        val textEmail = binding.editTextEmail.text.toString()
        val textPassword = binding.editTextPassword.text.toString()
        binding.btnLogin.isEnabled = isEmailValid(textEmail) && isPasswordValid(textPassword)
    }

    private fun setPrivacyPolicyText() {
        binding.tvPrivacyPolicy.text = Helper.getTncPrivacyPolicySpanText(requireActivity())
    }
}
