package com.rama.ecommerce.ui.main

import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.window.layout.WindowMetricsCalculator
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.databinding.FragmentMainBinding
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@androidx.annotation.OptIn(ExperimentalBadgeUtils::class)
@AndroidEntryPoint
class MainFragment : BaseFragment<FragmentMainBinding>(FragmentMainBinding::inflate) {

    private val viewModel: MainViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!viewModel.checkUserHasLogin()) {
            findNavController().navigate(R.id.action_global_prelogin_navigation)
        } else if (!viewModel.checkUserDataCompleted()) {
            findNavController().navigate(R.id.action_mainFragment_to_profileFragment)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setAppbar()
        setNavBar()
        observeWishlist()
        observeCart()
        observeNotification()
    }

    private fun observeWishlist() {
        val badgeCompact = binding.bottomNav?.getOrCreateBadge(R.id.wishlistFragment)
        val badgeMedium = binding.navRail?.getOrCreateBadge(R.id.wishlistFragment)
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.getWishlistSize().collect { size ->
                    if (size >= 1) {
                        badgeCompact?.isVisible = true
                        badgeCompact?.number = size
                        badgeMedium?.isVisible = true
                        badgeMedium?.number = size
                    } else {
                        badgeCompact?.isVisible = false
                        badgeMedium?.isVisible = false
                    }
                }
            }
        }
    }

    private fun observeCart() {
        val badgeDrawable = BadgeDrawable.create(requireActivity())
        BadgeUtils.attachBadgeDrawable(
            badgeDrawable,
            binding.appBarLayout.toolbar,
            R.id.menu_cart
        )
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.getCartSize().collect { size ->
                    if (size >= 1) {
                        badgeDrawable.isVisible = true
                        badgeDrawable.number = size
                    } else {
                        badgeDrawable.isVisible = false
                    }
                }
            }
        }
    }

    private fun observeNotification() {
        val badgeDrawable = BadgeDrawable.create(requireActivity())
        BadgeUtils.attachBadgeDrawable(
            badgeDrawable,
            binding.appBarLayout.toolbar,
            R.id.menu_notification
        )
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.getNotificationSize().collect { size ->
                    if (size >= 1) {
                        badgeDrawable.isVisible = true
                        badgeDrawable.number = size
                    } else {
                        badgeDrawable.isVisible = false
                    }
                }
            }
        }
    }

    private fun setAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = viewModel.user.userName
        toolbar.navigationIcon = ContextCompat.getDrawable(
            requireActivity(),
            R.drawable.ic_account_24
        )
        toolbar.inflateMenu(R.menu.main_menu)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.menu_notification -> {
                    firebaseAnalytics.sendLogButtonClicked("Main: Notification")
                    findNavController().navigate(R.id.action_mainFragment_to_notificationFragment)
                }

                R.id.menu_cart -> {
                    firebaseAnalytics.sendLogButtonClicked("Main: Cart")
                    findNavController().navigate(R.id.action_mainFragment_to_cartFragment)
                }

                R.id.menu_option -> {
                    firebaseAnalytics.sendLogButtonClicked("Main: Menu")
                    binding.root.showSnackbar("Option menu")
                }

                else -> return@setOnMenuItemClickListener false
            }
            return@setOnMenuItemClickListener true
        }
    }

    private fun setNavBar() {
        val navHostFragment =
            childFragmentManager.findFragmentById(R.id.fragment_container) as NavHostFragment
        val navController = navHostFragment.findNavController()

        with(binding) {
            bottomNav?.setupWithNavController(navController)
            navRail?.setupWithNavController(navController)
            navView?.setupWithNavController(navController)

            val container: ViewGroup = container
            container.addView(object : View(requireActivity()) {
                override fun onConfigurationChanged(newConfig: Configuration?) {
                    super.onConfigurationChanged(newConfig)
                    WindowMetricsCalculator.getOrCreate()
                        .computeCurrentWindowMetrics(requireActivity())
                }
            })
            WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity())

            if (viewModel.shouldMoveToTransaction) {
                bottomNav?.selectedItemId = R.id.transactionFragment
                navRail?.selectedItemId = R.id.transactionFragment
                navView?.menu?.performIdentifierAction(R.id.transactionFragment, 0)
                viewModel.shouldMoveToTransaction = false
            }
        }
    }

    companion object {
        const val MOVE_TRANSACTION_BUNDLE_KEY = "move_transaction_bundle_key"
    }
}
