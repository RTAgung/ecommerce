package com.rama.ecommerce.ui.fulfillment.detail.review

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.model.Review
import com.rama.ecommerce.data.repository.StoreRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReviewViewModel @Inject constructor(
    private val storeRepository: StoreRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    var productId: String = savedStateHandle[ReviewFragment.BUNDLE_PRODUCT_ID_KEY] ?: ""

    private val _reviewProductState = MutableStateFlow<ResultState<List<Review>>?>(null)
    val reviewProductState: StateFlow<ResultState<List<Review>>?> = _reviewProductState

    init {
        getListReview()
    }

    fun getListReview() {
        viewModelScope.launch {
            storeRepository.reviewProducts(productId).collect { resultState ->
                _reviewProductState.value = resultState
            }
        }
    }
}
