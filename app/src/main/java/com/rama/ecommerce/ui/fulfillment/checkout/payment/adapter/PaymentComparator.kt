package com.rama.ecommerce.ui.fulfillment.checkout.payment.adapter

import androidx.recyclerview.widget.DiffUtil
import com.rama.ecommerce.data.model.Payment

object PaymentComparator : DiffUtil.ItemCallback<Payment>() {
    override fun areItemsTheSame(oldItem: Payment, newItem: Payment): Boolean =
        oldItem.title == newItem.title

    override fun areContentsTheSame(oldItem: Payment, newItem: Payment): Boolean =
        oldItem == newItem
}
