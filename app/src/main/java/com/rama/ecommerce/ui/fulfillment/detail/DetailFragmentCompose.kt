package com.rama.ecommerce.ui.fulfillment.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationVector1D
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DismissValue
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilterChip
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SwipeToDismiss
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.rememberDismissState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import com.google.firebase.analytics.FirebaseAnalytics.Param
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.DetailProduct
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.ui.fulfillment.checkout.CheckoutFragment
import com.rama.ecommerce.ui.fulfillment.detail.review.ReviewFragmentCompose
import com.rama.ecommerce.utils.Constant
import com.rama.ecommerce.utils.compose.AppTheme
import com.rama.ecommerce.utils.compose.poppinsFamily
import com.rama.ecommerce.utils.extension.getErrorSubtitle
import com.rama.ecommerce.utils.extension.getErrorTitle
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.toCart
import com.rama.ecommerce.utils.extension.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.Locale

@AndroidEntryPoint
class DetailFragmentCompose : Fragment() {

    private val viewModel: DetailComposeViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                AppTheme {
                    DetailFragmentScreen(
                        firebaseAnalytics = firebaseAnalytics,
                        viewModel = viewModel,
                        onBackPressed = {
                            findNavController().navigateUp()
                        },
                        onClickBuyNow = {
                            firebaseAnalytics.sendLogButtonClicked("Detail: Buy Now")
                            val product = viewModel.composeState.value.detailProduct
                            val variant = viewModel.composeState.value.productVariant
                            val data = product!!.toCart(variant!!)
                            val bundle = Bundle().apply {
                                putParcelableArrayList(CheckoutFragment.ARG_DATA, arrayListOf(data))
                            }
                            findNavController().navigate(
                                R.id.action_detailFragmentCompose_to_checkoutFragment,
                                bundle
                            )
                        },
                        onErrorAction = { viewModel.getDetailProduct() },
                        onMoreClicked = {
                            firebaseAnalytics.sendLogButtonClicked("Detail: Review")
                            findNavController().navigate(
                                R.id.action_detailFragmentCompose_to_reviewFragmentCompose,
                                bundleOf(ReviewFragmentCompose.BUNDLE_PRODUCT_ID_KEY to viewModel.productId)
                            )
                        }
                    )
                }
            }
        }
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "product_id"
    }
}

data class AnimationState(
    val imageAlpha: Animatable<Float, AnimationVector1D> = Animatable(0f),
    val sectionTitleAlpha: Animatable<Float, AnimationVector1D> = Animatable(0f),
    val sectionVariantAlpha: Animatable<Float, AnimationVector1D> = Animatable(0f),
    val sectionDescAlpha: Animatable<Float, AnimationVector1D> = Animatable(0f),
    val sectionReviewAlpha: Animatable<Float, AnimationVector1D> = Animatable(0f),
    val imageTranslation: Animatable<Float, AnimationVector1D> = Animatable(-12f),
    val sectionTitleTranslation: Animatable<Float, AnimationVector1D> = Animatable(-12f),
    val sectionVariantTranslation: Animatable<Float, AnimationVector1D> = Animatable(-12f),
    val sectionDescTranslation: Animatable<Float, AnimationVector1D> = Animatable(-12f),
    val sectionReviewTranslation: Animatable<Float, AnimationVector1D> = Animatable(-12f)
)

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailFragmentScreen(
    firebaseAnalytics: FirebaseAnalytics,
    viewModel: DetailComposeViewModel,
    onBackPressed: () -> Unit,
    onClickBuyNow: () -> Unit,
    onErrorAction: () -> Unit,
    onMoreClicked: () -> Unit
) {
    val context = LocalContext.current

    val state = viewModel.composeState.collectAsState()
    val resultState = state.value.resultState

    val scope = rememberCoroutineScope()
    val snackbarHostState = remember { SnackbarHostState() }
    val dismissSnackbarState = rememberDismissState()

    LaunchedEffect(dismissSnackbarState.currentValue) {
        if (dismissSnackbarState.currentValue != DismissValue.Default) {
            snackbarHostState.currentSnackbarData?.dismiss()
            delay(Constant.SNACKBAR_DELAY)
            dismissSnackbarState.snapTo(DismissValue.Default)
        }
    }

    var doAnimation by remember { mutableStateOf(false) }
    val animState = remember { AnimationState() }

    LaunchedEffect(doAnimation) {
        val animSpec = tween<Float>(durationMillis = 300, easing = FastOutSlowInEasing)
        if (doAnimation) {
            launch {
                animState.imageAlpha.animateTo(1f, animationSpec = animSpec)
                animState.sectionTitleAlpha.animateTo(1f, animationSpec = animSpec)
                animState.sectionVariantAlpha.animateTo(1f, animationSpec = animSpec)
                animState.sectionDescAlpha.animateTo(1f, animationSpec = animSpec)
                animState.sectionReviewAlpha.animateTo(1f, animationSpec = animSpec)
            }
            launch {
                animState.imageTranslation.animateTo(0f, animationSpec = animSpec)
                animState.sectionTitleTranslation.animateTo(0f, animationSpec = animSpec)
                animState.sectionVariantTranslation.animateTo(0f, animationSpec = animSpec)
                animState.sectionDescTranslation.animateTo(0f, animationSpec = animSpec)
                animState.sectionReviewTranslation.animateTo(0f, animationSpec = animSpec)
            }
        }
    }

    Scaffold(
        topBar = { DetailAppBar(onBackPressed) },
        bottomBar = {
            if (resultState is ResultState.Success) {
                BottomButton(
                    onClickBuyNow = { onClickBuyNow() },
                    onClickAddCart = {
                        firebaseAnalytics.sendLogButtonClicked("Detail: Add Cart")
                        if (viewModel.insertCart()) {
                            sendLogWishlistOrCart(firebaseAnalytics, Event.ADD_TO_CART, state.value)
                            scope.launch {
                                snackbarHostState.currentSnackbarData?.dismiss()
                                snackbarHostState.showSnackbar(
                                    context.getString(R.string.cart_insert_successful)
                                )
                            }
                        } else {
                            scope.launch {
                                snackbarHostState.currentSnackbarData?.dismiss()
                                snackbarHostState.showSnackbar(
                                    context.getString(R.string.stock_is_unavailable)
                                )
                            }
                        }
                    },
                )
            }
        },
        snackbarHost = {
            SwipeToDismiss(
                state = dismissSnackbarState,
                background = {},
                dismissContent = {
                    SnackbarHost(hostState = snackbarHostState, modifier = Modifier.imePadding())
                }
            )
        }
    ) { innerPadding ->
        Column(
            Modifier
                .padding(innerPadding)
                .fillMaxSize()
        ) {
            when (resultState) {
                is ResultState.Loading -> Loading(Modifier.fillMaxSize())
                is ResultState.Success -> DetailContent(
                    dataState = state.value,
                    animState = animState,
                    onSharedClick = {
                        firebaseAnalytics.sendLogButtonClicked("Detail: Share")
                        actionShare(context, state.value)
                    },
                    onWishlistClick = {
                        firebaseAnalytics.sendLogButtonClicked("Detail: Wishlist")
                        if (state.value.isWishlist) {
                            viewModel.deleteWishlist()
                            scope.launch {
                                snackbarHostState.currentSnackbarData?.dismiss()
                                snackbarHostState.showSnackbar(
                                    context.getString(R.string.wishlist_remove_successful)
                                )
                            }
                        } else {
                            viewModel.insertWishlist()
                            sendLogWishlistOrCart(
                                firebaseAnalytics,
                                Event.ADD_TO_WISHLIST,
                                state.value
                            )
                            scope.launch {
                                snackbarHostState.currentSnackbarData?.dismiss()
                                snackbarHostState.showSnackbar(
                                    context.getString(R.string.wishlist_insert_successful)
                                )
                            }
                        }
                    },
                    onVariantClicked = { viewModel.updateVariant(it) },
                    onMoreClicked = { onMoreClicked() },
                    modifier = Modifier
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState())
                )

                is ResultState.Error -> Error(
                    Modifier
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState()),
                    resultState.error,
                    onErrorAction
                )

                else -> {}
            }

            if (resultState is ResultState.Success) {
                sendLogViewItem(firebaseAnalytics, resultState.data)
                doAnimation = true
            }
        }
    }
}

private fun sendLogViewItem(firebaseAnalytics: FirebaseAnalytics, data: DetailProduct) {
    firebaseAnalytics.logEvent(Event.VIEW_ITEM) {
        val bundleProduct = Bundle().apply {
            putString(Param.ITEM_ID, data.productId)
            putString(Param.ITEM_NAME, data.productName)
            putString(Param.ITEM_BRAND, data.brand)
        }
        param(Param.ITEMS, arrayOf(bundleProduct))
    }
}

private fun sendLogWishlistOrCart(
    firebaseAnalytics: FirebaseAnalytics,
    event: String,
    state: DetailComposeState
) {
    firebaseAnalytics.logEvent(event) {
        val bundleProduct = Bundle().apply {
            putString(Param.ITEM_ID, state.detailProduct!!.productId)
            putString(Param.ITEM_NAME, state.detailProduct.productName)
            putString(Param.ITEM_BRAND, state.detailProduct.brand)
            putString(Param.ITEM_VARIANT, state.productVariant!!.variantName)
        }
        param(Param.ITEMS, arrayOf(bundleProduct))
    }
}

private fun actionShare(context: Context, state: DetailComposeState) {
    val sendIntent: Intent = Intent().apply {
        val text = """
                Name : ${state.detailProduct?.productName}
                Price : ${state.detailProduct?.productPrice?.toCurrencyFormat()}
                Link : https://ecommerce.rama.com/products/${state.productId}
        """.trimIndent()
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT, text)
        type = "text/plain"
    }

    val shareIntent = Intent.createChooser(sendIntent, null)
    context.startActivity(shareIntent)
}

@Composable
fun Error(modifier: Modifier = Modifier, error: Throwable, onErrorAction: () -> Unit) {
    val title = error.getErrorTitle(LocalContext.current)
    val subtitle = error.getErrorSubtitle(LocalContext.current)
    val action = stringResource(id = R.string.refresh_error_action)
    Column(
        modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.store_error_image),
            contentDescription = null,
            modifier = Modifier.size(128.dp)
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = title,
            fontSize = 32.sp,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Medium,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
        Spacer(modifier = Modifier.height(4.dp))
        Text(
            text = subtitle,
            fontSize = 16.sp,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Normal,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
        Spacer(modifier = Modifier.height(8.dp))
        Button(onClick = { onErrorAction() }) {
            Text(
                text = action,
                fontSize = 14.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Medium,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                ),
            )
        }
    }
}

@Composable
fun Loading(modifier: Modifier = Modifier) {
    Column(
        modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun DetailContent(
    dataState: DetailComposeState,
    animState: AnimationState,
    onSharedClick: () -> Unit,
    onWishlistClick: () -> Unit,
    onVariantClicked: (DetailProduct.ProductVariant) -> Unit,
    onMoreClicked: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(modifier) {
        ImageBanner(dataState.detailProduct!!.image, animState)
        TitleSection(
            dataState.detailProduct,
            dataState.productVariant!!,
            dataState.isWishlist,
            onSharedClick,
            onWishlistClick,
            animState,
            Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
        )
        VariantSection(
            dataState.detailProduct,
            dataState.productVariant,
            onVariantClicked,
            animState,
            Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
        )
        DescriptionSection(
            dataState.detailProduct,
            animState,
            Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
        )
        ReviewSection(
            dataState.detailProduct,
            onMoreClicked,
            animState,
            Modifier.padding(horizontal = 16.dp, vertical = 12.dp)
        )
    }
}

@Composable
fun ReviewSection(
    product: DetailProduct,
    onMoreClicked: () -> Unit,
    animState: AnimationState,
    modifier: Modifier = Modifier
) {
    val alpha = animState.sectionReviewAlpha.value
    val translationX = animState.sectionReviewTranslation.value
    Divider(Modifier.graphicsLayer(alpha = alpha, translationX = translationX))
    Column(modifier.graphicsLayer(alpha = alpha, translationX = translationX)) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = stringResource(id = R.string.buyer_review),
                fontSize = 16.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Medium,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                ),
            )
            TextButton(
                onClick = { onMoreClicked() },
                contentPadding = PaddingValues(vertical = 0.dp, horizontal = 16.dp),
                modifier = Modifier.height(22.dp),
            ) {
                Text(
                    text = stringResource(id = R.string.see_all),
                    fontSize = 12.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Medium,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    ),
                )
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
        Row(verticalAlignment = Alignment.CenterVertically) {
            Icon(painter = painterResource(id = R.drawable.ic_star_24), contentDescription = null)
            Spacer(modifier = Modifier.width(2.dp))
            Text(
                text = String.format(Locale.getDefault(), "%.1f", product.productRating),
                fontSize = 20.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.SemiBold,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                ),
            )
            Text(
                text = stringResource(id = R.string.max_rating),
                fontSize = 10.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Normal,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                ),
                modifier = Modifier.padding(top = 10.dp)
            )
            Spacer(modifier = Modifier.width(32.dp))
            Column {
                Text(
                    text = stringResource(
                        id = R.string.text_satisfaction,
                        product.totalSatisfaction
                    ),
                    fontSize = 12.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.SemiBold,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    ),
                )
                Text(
                    text = stringResource(
                        id = R.string.total_rating_and_review,
                        product.totalRating,
                        product.totalReview
                    ),
                    fontSize = 12.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Normal,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    ),
                )
            }
        }
    }
}

@Composable
fun DescriptionSection(
    product: DetailProduct,
    animState: AnimationState,
    modifier: Modifier = Modifier
) {
    val alpha = animState.sectionDescAlpha.value
    val translationX = animState.sectionDescTranslation.value
    Divider(Modifier.graphicsLayer(alpha = alpha, translationX = translationX))
    Column(modifier.graphicsLayer(alpha = alpha, translationX = translationX)) {
        Text(
            text = stringResource(id = R.string.product_description),
            fontSize = 16.sp,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Medium,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = product.description,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Normal,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
    }
}

@OptIn(ExperimentalLayoutApi::class, ExperimentalMaterial3Api::class)
@Composable
fun VariantSection(
    product: DetailProduct,
    variant: DetailProduct.ProductVariant,
    onVariantClicked: (DetailProduct.ProductVariant) -> Unit,
    animState: AnimationState,
    modifier: Modifier = Modifier
) {
    val alpha = animState.sectionVariantAlpha.value
    val translationX = animState.sectionVariantTranslation.value
    Divider(Modifier.graphicsLayer(alpha = alpha, translationX = translationX))
    Column(modifier.graphicsLayer(alpha = alpha, translationX = translationX)) {
        Text(
            text = stringResource(id = R.string.choose_variant),
            fontSize = 16.sp,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Medium,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
        FlowRow(horizontalArrangement = Arrangement.spacedBy(8.dp)) {
            val variantList = product.productVariant
            repeat(variantList.size) { index ->
                val isSelected = variantList[index] == variant
                FilterChip(
                    selected = isSelected,
                    onClick = { onVariantClicked(variantList[index]) },
                    label = {
                        Text(
                            text = variantList[index].variantName,
                            style = TextStyle(
                                fontFamily = poppinsFamily,
                                fontWeight = FontWeight.Medium,
                                platformStyle = PlatformTextStyle(
                                    includeFontPadding = false,
                                ),
                            ),
                        )
                    }
                )
            }
        }
    }
}

@Composable
fun TitleSection(
    product: DetailProduct,
    variant: DetailProduct.ProductVariant,
    isWishlist: Boolean,
    onSharedClick: () -> Unit,
    onWishlistClick: () -> Unit,
    animState: AnimationState,
    modifier: Modifier = Modifier
) {
    val price = product.productPrice + variant.variantPrice
    val alpha = animState.sectionTitleAlpha.value
    val translationX = animState.sectionTitleTranslation.value
    Column(modifier.graphicsLayer(alpha = alpha, translationX = translationX)) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Text(
                text = price.toCurrencyFormat(),
                fontSize = 20.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.SemiBold,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                ),
                modifier = Modifier.weight(1f)
            )
            IconButton(
                onClick = { onSharedClick() },
                modifier = Modifier.size(24.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_share_24),
                    contentDescription = null
                )
            }
            Spacer(modifier = Modifier.width(16.dp))
            IconButton(
                onClick = { onWishlistClick() },
                modifier = Modifier.size(24.dp)
            ) {
                if (isWishlist) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_favorite_24),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.primary
                    )
                } else {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_favorite_border_24),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onSurface
                    )
                }
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = product.productName,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Normal,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            )
        )
        Spacer(modifier = Modifier.height(8.dp))
        Row(verticalAlignment = Alignment.CenterVertically) {
            Text(
                text = stringResource(id = R.string.total_sales, product.sale),
                fontSize = 12.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Normal,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                )
            )
            Spacer(modifier = Modifier.width(8.dp))
            Box(
                Modifier.border(
                    border = BorderStroke(1.dp, MaterialTheme.colorScheme.outline),
                    shape = RoundedCornerShape(4.dp),
                )
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.padding(4.dp, 2.dp, 8.dp, 2.dp)
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_star_24),
                        contentDescription = null,
                        modifier = Modifier.size(14.dp)
                    )
                    Spacer(modifier = Modifier.width(4.dp))
                    Text(
                        text = stringResource(
                            id = R.string.detail_rating_and_total_rating,
                            product.productRating,
                            product.totalRating
                        ),
                        fontSize = 12.sp,
                        style = TextStyle(
                            fontFamily = poppinsFamily,
                            fontWeight = FontWeight.Normal,
                            platformStyle = PlatformTextStyle(
                                includeFontPadding = false,
                            ),
                        )
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun ImageBanner(dataImage: List<String>, animState: AnimationState) {
    val pagerState = rememberPagerState(pageCount = {
        dataImage.size
    })
    val alpha = animState.imageAlpha.value
    val translationX = animState.imageTranslation.value
    Box(Modifier.graphicsLayer(alpha = alpha, translationX = translationX)) {
        HorizontalPager(state = pagerState) {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(dataImage[it])
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(id = R.drawable.image_placeholder),
                error = painterResource(id = R.drawable.image_placeholder),
                contentDescription = null,
                modifier = Modifier
                    .height(309.dp)
                    .fillMaxWidth()
            )
        }
        Row(
            Modifier
                .wrapContentHeight()
                .fillMaxWidth()
                .align(Alignment.BottomCenter)
                .padding(bottom = 8.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            repeat(pagerState.pageCount) { iteration ->
                if (pagerState.pageCount > 1) {
                    val color =
                        if (pagerState.currentPage == iteration) {
                            MaterialTheme.colorScheme.primary
                        } else {
                            MaterialTheme.colorScheme.outlineVariant
                        }
                    Box(
                        modifier = Modifier
                            .padding(8.dp)
                            .clip(CircleShape)
                            .background(color)
                            .size(8.dp)
                    )
                }
            }
        }
    }
}

@Composable
fun BottomButton(
    onClickBuyNow: () -> Unit,
    onClickAddCart: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier
            .fillMaxWidth()
            .wrapContentHeight()
    ) {
        Divider()
        Row(
            Modifier
                .padding(16.dp, 8.dp, 16.dp, 8.dp)
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            OutlinedButton(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                onClick = { onClickBuyNow() }
            ) {
                Text(
                    text = stringResource(id = R.string.buy_now),
                    fontSize = 14.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Medium,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    ),
                )
            }
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                onClick = { onClickAddCart() }
            ) {
                Text(
                    text = stringResource(id = R.string.add_to_cart),
                    fontSize = 14.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Medium,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    ),
                )
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailAppBar(onBackPressed: () -> Unit) {
    Column(Modifier.fillMaxWidth()) {
        TopAppBar(
            title = {
                Text(
                    stringResource(id = R.string.product_detail),
                    fontSize = 22.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Normal,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    )
                )
            },
            navigationIcon = {
                IconButton(onClick = { onBackPressed() }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_arrow_back_24),
                        contentDescription = null
                    )
                }
            }
        )
        Divider()
    }
}
