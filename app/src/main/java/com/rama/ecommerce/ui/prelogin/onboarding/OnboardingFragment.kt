package com.rama.ecommerce.ui.prelogin.onboarding

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.databinding.FragmentOnboardingBinding
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnboardingFragment :
    BaseFragment<FragmentOnboardingBinding>(FragmentOnboardingBinding::inflate) {

    private val viewModel: OnboardingViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    private lateinit var pager: ViewPager2
    private val pagerData = listOf(
        R.drawable.onboarding_image_1,
        R.drawable.onboarding_image_2,
        R.drawable.onboarding_image_3,
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setViewPager()
        setAction()
        checkNotificationPermission()
    }

    private fun checkNotificationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            val notificationPermission = Manifest.permission.POST_NOTIFICATIONS
            if (!isPermissionsGranted(notificationPermission)) {
                requestPermissionLauncher.launch(notificationPermission)
            }
        }
    }

    private fun isPermissionsGranted(permission: String) =
        ContextCompat.checkSelfPermission(
            requireActivity(),
            permission
        ) == PackageManager.PERMISSION_GRANTED

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { }

    private fun setAction() {
        binding.btnJoinOnboarding.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Onboarding: Join Now")
            viewModel.setFirstTimeRunApp(false)
            findNavController().navigate(R.id.action_onboardingFragment_to_registerFragment)
        }
        binding.btnSkipOnboarding.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Onboarding: Skip")
            viewModel.setFirstTimeRunApp(false)
            findNavController().navigate(R.id.action_onboardingFragment_to_loginFragment)
        }
        binding.btnNextOnboarding.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Onboarding: Next")
            pager.setCurrentItem(getNextItem(1), true)
        }
    }

    private fun setViewPager() {
        val adapter = OnboardingAdapter()
        adapter.submitList(pagerData)
        pager = binding.pagerOnboarding
        pager.adapter = adapter

        TabLayoutMediator(binding.tabLayout, pager) { _, _ -> }.attach()

        pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                val hide = position == (pagerData.size - 1)
                showNextButton(!hide)
            }
        })
    }

    private fun showNextButton(isShow: Boolean) {
        binding.btnNextOnboarding.isVisible = isShow
    }

    private fun getNextItem(i: Int): Int {
        return pager.currentItem + i
    }
}
