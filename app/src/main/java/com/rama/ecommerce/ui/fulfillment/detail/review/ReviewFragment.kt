package com.rama.ecommerce.ui.fulfillment.detail.review

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.databinding.FragmentReviewBinding
import com.rama.ecommerce.ui.fulfillment.detail.review.adapter.ReviewAdapter
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.extension.getErrorSubtitle
import com.rama.ecommerce.utils.extension.getErrorTitle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ReviewFragment : BaseFragment<FragmentReviewBinding>(FragmentReviewBinding::inflate) {

    private val viewModel: ReviewViewModel by viewModels()

    private val adapter = ReviewAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAppbar()
        setupRecyclerView()
        observeReviewProduct()
    }

    private fun setupRecyclerView() {
        binding.rvReview.adapter = adapter
        binding.rvReview.layoutManager = LinearLayoutManager(requireActivity())
    }

    private fun observeReviewProduct() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.reviewProductState.collect { result ->
                    if (result != null) {
                        showLoading(result is ResultState.Loading)
                        when (result) {
                            is ResultState.Loading -> {}
                            is ResultState.Success -> adapter.submitList(result.data)
                            is ResultState.Error -> showErrorView(result.error)
                        }
                    }
                }
            }
        }
    }

    private fun showErrorView(error: Throwable) {
        val title = error.getErrorTitle(requireActivity())
        val subtitle = error.getErrorSubtitle(requireActivity())
        val action = getString(R.string.refresh_error_action)

        binding.rvReview.isVisible = false
        with(binding.includeError) {
            tvErrorTitle.text = title
            tvErrorSubtitle.text = subtitle
            btnErrorAction.text = action

            btnErrorAction.setOnClickListener { viewModel.getListReview() }
            layoutError.isVisible = true
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.rvReview.isVisible = !isLoading
        binding.loadingReview.isVisible = isLoading
        binding.includeError.layoutError.isVisible = false
    }

    private fun setupAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.buyer_review)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "product_id"
    }
}
