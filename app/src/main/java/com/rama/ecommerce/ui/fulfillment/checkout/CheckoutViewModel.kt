package com.rama.ecommerce.ui.fulfillment.checkout

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.Cart
import com.rama.ecommerce.data.model.Fulfillment
import com.rama.ecommerce.data.model.Payment
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.FulfillmentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CheckoutViewModel @Inject constructor(
    private val fulfillmentRepository: FulfillmentRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _listData: MutableStateFlow<List<Cart>> = MutableStateFlow(ArrayList())
    val listData: StateFlow<List<Cart>> = _listData

    var paymentItem: Payment.PaymentItem? = null
    var totalPrice = 0

    private val _paymentState = MutableStateFlow<ResultState<Fulfillment>?>(null)
    val paymentState: StateFlow<ResultState<Fulfillment>?> = _paymentState

    init {
        setData(savedStateHandle.get<ArrayList<Cart>>(CheckoutFragment.ARG_DATA) ?: ArrayList())
    }

    private fun setData(data: ArrayList<Cart>) {
        _listData.value = data.map { it.copy(quantity = it.quantity ?: 1) }
    }

    fun updateDataQuantity(cart: Cart, isInsert: Boolean) {
        if (isInsert && cart.stock > cart.quantity!!) {
            updateQuantity(cart, 1)
        } else if (!isInsert && cart.quantity!! > 1) {
            updateQuantity(cart, -1)
        }
    }

    private fun updateQuantity(cart: Cart, number: Int) {
        _listData.update {
            it.map { data ->
                if (data.productId == cart.productId) {
                    data.copy(quantity = data.quantity?.plus(number))
                } else {
                    data
                }
            }
        }
    }

    fun makePayment() {
        if (paymentItem != null) {
            viewModelScope.launch {
                val data = listData.first()
                fulfillmentRepository.fulfillment(paymentItem!!.label, data).collect { result ->
                    _paymentState.value = result
                }
            }
        }
    }
}
