package com.rama.ecommerce.ui.fulfillment.checkout.payment.adapter

import androidx.recyclerview.widget.DiffUtil
import com.rama.ecommerce.data.model.Payment

object PaymentItemComparator : DiffUtil.ItemCallback<Payment.PaymentItem>() {
    override fun areItemsTheSame(
        oldItem: Payment.PaymentItem,
        newItem: Payment.PaymentItem
    ): Boolean = oldItem.label == newItem.label

    override fun areContentsTheSame(
        oldItem: Payment.PaymentItem,
        newItem: Payment.PaymentItem
    ): Boolean = oldItem == newItem
}
