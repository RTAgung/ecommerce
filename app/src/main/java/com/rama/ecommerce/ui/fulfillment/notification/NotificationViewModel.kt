package com.rama.ecommerce.ui.fulfillment.notification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.Notification
import com.rama.ecommerce.data.repository.NotificationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(
    private val notificationRepository: NotificationRepository
) : ViewModel() {

    fun getNotificationData(): Flow<List<Notification>> =
        notificationRepository.getNotificationData()

    fun readNotification(data: Notification) {
        viewModelScope.launch {
            notificationRepository.updateNotification(data.copy(isRead = true))
        }
    }
}
