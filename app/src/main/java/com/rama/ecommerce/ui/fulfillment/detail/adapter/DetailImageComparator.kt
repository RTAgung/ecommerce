package com.rama.ecommerce.ui.fulfillment.detail.adapter

import androidx.recyclerview.widget.DiffUtil

object DetailImageComparator : DiffUtil.ItemCallback<String>() {
    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean = oldItem == newItem
    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean =
        oldItem == newItem
}
