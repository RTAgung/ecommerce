package com.rama.ecommerce.ui.main

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.rama.ecommerce.data.repository.CartRepository
import com.rama.ecommerce.data.repository.NotificationRepository
import com.rama.ecommerce.data.repository.PreloginRepository
import com.rama.ecommerce.data.repository.WishlistRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val preloginRepository: PreloginRepository,
    private val wishlistRepository: WishlistRepository,
    private val cartRepository: CartRepository,
    private val notificationRepository: NotificationRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    var shouldMoveToTransaction =
        savedStateHandle.get<Boolean>(MainFragment.MOVE_TRANSACTION_BUNDLE_KEY) ?: false

    val user by lazy {
        runBlocking { getUserDataSession().first() }
    }

    fun getWishlistSize(): Flow<Int> = wishlistRepository.getWishlistDataSize()

    fun getCartSize(): Flow<Int> = cartRepository.getCartDataSize()

    fun getNotificationSize(): Flow<Int> = notificationRepository.getNotificationDataSize()

    private fun getUserDataSession() = preloginRepository.getUserDataSession()

    fun checkUserDataCompleted(): Boolean {
        if (!user.userName.isNullOrEmpty()) return true
        return false
    }

    fun checkUserHasLogin(): Boolean {
        if (user.accessToken.isNullOrEmpty() or user.refreshToken.isNullOrEmpty()) {
            return false
        }
        return true
    }
}
