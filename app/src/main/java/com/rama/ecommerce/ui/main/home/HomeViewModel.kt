package com.rama.ecommerce.ui.main.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.repository.PreloginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val preloginRepository: PreloginRepository
) : ViewModel() {
    fun makeLogout() {
        viewModelScope.launch {
            preloginRepository.logout()
        }
    }

    fun getAppTheme() = preloginRepository.getAppTheme()

    fun setAppTheme(isDarkTheme: Boolean) {
        runBlocking { preloginRepository.setAppTheme(isDarkTheme) }
    }

    fun getAppLanguage() = preloginRepository.getAppLanguage()

    fun setAppLanguage(isIdLanguage: Boolean) {
        runBlocking { preloginRepository.setAppLanguage(if (isIdLanguage) "id" else null) }
    }
}
