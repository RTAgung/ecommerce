package com.rama.ecommerce.ui.main.store.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import coil.load
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.Product
import com.rama.ecommerce.databinding.ProductItemGridLayoutBinding
import com.rama.ecommerce.databinding.ProductItemListLayoutBinding
import com.rama.ecommerce.utils.extension.toCurrencyFormat

class ProductAdapter(
    private val itemClickCallback: (Product) -> Unit
) : PagingDataAdapter<Product, RecyclerView.ViewHolder>(ProductComparator) {

    var viewType = ONE_COLUMN_VIEW_TYPE

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {
            (holder as ProductViewHolder<*>).bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == ONE_COLUMN_VIEW_TYPE) {
            val binding =
                ProductItemListLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return ProductViewHolder(binding, viewType, itemClickCallback)
        } else {
            val binding =
                ProductItemGridLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return ProductViewHolder(binding, viewType, itemClickCallback)
        }
    }

    class ProductViewHolder<T : ViewBinding>(
        private val binding: T,
        private val viewType: Int,
        private val itemClickCallback: (Product) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Product) {
            if (viewType == ONE_COLUMN_VIEW_TYPE) {
                with(binding as ProductItemListLayoutBinding) {
                    cardView.startAnimation(
                        AnimationUtils.loadAnimation(
                            itemView.context,
                            R.anim.anim_item_list
                        )
                    )
                    bindView(
                        data,
                        ivProductImage,
                        tvProductName,
                        tvProductPrice,
                        tvProductStore,
                        tvProductRatingAndSales
                    )
                }
            } else {
                with(binding as ProductItemGridLayoutBinding) {
                    cardView.startAnimation(
                        AnimationUtils.loadAnimation(
                            itemView.context,
                            if (absoluteAdapterPosition % 2 == 0) {
                                R.anim.anim_item_list
                            } else {
                                R.anim.anim_item_list_reversed
                            }
                        )
                    )
                    bindView(
                        data,
                        ivProductImage,
                        tvProductName,
                        tvProductPrice,
                        tvProductStore,
                        tvProductRatingAndSales
                    )
                }
            }
            itemView.setOnClickListener { itemClickCallback(data) }
        }

        private fun bindView(
            data: Product,
            productImage: ImageView,
            productName: TextView,
            productPrice: TextView,
            productStore: TextView,
            productRatingAndSales: TextView,
        ) {
            productImage.load(data.image) {
                crossfade(true)
                placeholder(R.drawable.image_placeholder)
                error(R.drawable.image_placeholder)
            }
            productName.text = data.productName
            productPrice.text = data.productPrice.toCurrencyFormat()
            productStore.text = data.store
            productRatingAndSales.text =
                itemView.resources.getString(
                    R.string.product_rating_and_sales,
                    data.productRating,
                    data.sale
                )
        }
    }

    companion object {
        const val ONE_COLUMN_VIEW_TYPE = 1
        const val MORE_COLUMN_VIEW_TYPE = 2
    }
}
