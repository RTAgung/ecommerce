package com.rama.ecommerce.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.repository.PreloginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val preloginRepository: PreloginRepository) :
    ViewModel() {
    fun checkUserAuthorization() = preloginRepository.checkUserAuthorization()

    fun getAppTheme() = preloginRepository.getAppTheme()

    fun getAppLanguage() = preloginRepository.getAppLanguage()

    fun makeLogout() {
        viewModelScope.launch { preloginRepository.logout() }
    }
}
