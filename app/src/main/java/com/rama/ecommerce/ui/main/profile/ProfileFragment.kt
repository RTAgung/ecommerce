package com.rama.ecommerce.ui.main.profile

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.databinding.FragmentProfileBinding
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.Helper
import com.rama.ecommerce.utils.PhotoLoaderManager
import com.rama.ecommerce.utils.extension.getErrorMessage
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.showSnackbar
import com.rama.ecommerce.utils.extension.toMultipartBody
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import okhttp3.RequestBody.Companion.toRequestBody

@AndroidEntryPoint
class ProfileFragment :
    BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {

    private val viewModel: ProfileViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    private var tempUri: Uri? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAppbar()
        setPrivacyPolicyText()
        setView()
        setAction()
        observeUpload()
        playAnimation()
    }

    private fun playAnimation() {
        val listLayout = listOf(
            binding.imageProfile,
            binding.layoutTextName,
            binding.btnFinish,
            binding.tvPrivacyPolicy,
        )
        val animSet = AnimatorSet()
        var animBefore: ObjectAnimator? = null
        listLayout.reversed().forEach { view ->
            val animAlpha = ObjectAnimator.ofFloat(view, View.ALPHA, 1f).apply {
                duration = 300
                interpolator = AccelerateInterpolator()
            }
            val animTranslation = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, 1f).apply {
                duration = 300
                interpolator = DecelerateInterpolator()
            }
            if (animBefore != null) {
                animSet.play(animAlpha).before(animBefore)
            } else {
                animSet.play(animAlpha)
            }
            animSet.play(animTranslation).with(animAlpha)
            animBefore = animAlpha
        }
        animSet.start()
    }

    private fun setAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = resources.getString(R.string.profile)
        toolbar.isTitleCentered = true
    }

    private fun setView() {
        binding.editTextName.doOnTextChanged { s, _, _, _ ->
            val text = s.toString()
            binding.btnFinish.isEnabled = text.isNotEmpty()
        }
        showImage()
    }

    private fun observeUpload() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uploadState.collect { resultState ->
                    if (resultState != null) {
                        when (resultState) {
                            is ResultState.Loading -> {
                                showUploadLoading(true)
                            }

                            is ResultState.Success -> {
                                showUploadLoading(false)
                                val isSuccessful = resultState.data
                                if (isSuccessful) {
                                    findNavController().navigate(
                                        R.id.action_profileFragment_to_mainFragment
                                    )
                                }
                            }

                            is ResultState.Error -> {
                                showUploadLoading(false)
                                val errorMessage = resultState.error.getErrorMessage()
                                binding.root.showSnackbar(errorMessage)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun showUploadLoading(isLoading: Boolean) {
        binding.loadingFinish.isInvisible = !isLoading
        binding.btnFinish.isInvisible = isLoading
    }

    private fun actionOpenCamera() {
        firebaseAnalytics.sendLogButtonClicked("Profile: Camera")
        tempUri = PhotoLoaderManager.buildNewUri(requireActivity())
        cameraIntentLauncher.launch(tempUri)
    }

    private val cameraIntentLauncher = registerForActivityResult(
        ActivityResultContracts.TakePicture()
    ) { isSuccess ->
        if (isSuccess) {
            viewModel.currentImageUri = tempUri
            showImage()
        }
    }

    private fun actionOpenGallery() {
        firebaseAnalytics.sendLogButtonClicked("Profile: Gallery")
        pickMediaLauncher.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
    }

    private val pickMediaLauncher =
        registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            if (uri != null) {
                viewModel.currentImageUri = uri
                showImage()
            }
        }

    private fun showImage() {
        viewModel.currentImageUri?.let {
            binding.imageProfile.setImageURI(it)
        }
    }

    private fun setAction() {
        binding.btnFinish.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Profile: Finish")
            uploadProfileData()
        }
        binding.imageProfile.setOnClickListener {
            val items = arrayOf(getString(R.string.camera), getString(R.string.gallery))

            MaterialAlertDialogBuilder(requireActivity())
                .setTitle(getString(R.string.choose_photo))
                .setBackground(
                    ContextCompat.getDrawable(
                        requireActivity(),
                        R.drawable.background_dialog
                    )
                )
                .setItems(items) { _, which ->
                    if (items[which] == getString(R.string.camera)) {
                        actionOpenCamera()
                    } else if (items[which] == getString(R.string.gallery)) {
                        actionOpenGallery()
                    }
                }
                .show()
        }
    }

    private fun uploadProfileData() {
        val imageFile = viewModel.currentImageUri?.let { uri ->
            PhotoLoaderManager.uriToFile(uri, requireActivity())
        }
        val name = binding.editTextName.text.toString()

        val nameRequestBody = name.toRequestBody()
        val imageMultipartBody = imageFile?.toMultipartBody("userImage")
        viewModel.uploadProfile(nameRequestBody, imageMultipartBody)
    }

    private fun setPrivacyPolicyText() {
        binding.tvPrivacyPolicy.text = Helper.getTncPrivacyPolicySpanText(requireActivity())
    }
}
