package com.rama.ecommerce.ui.fulfillment.checkout.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.Cart
import com.rama.ecommerce.databinding.CartItemLayoutBinding
import com.rama.ecommerce.ui.fulfillment.cart.adapter.CartComparator
import com.rama.ecommerce.ui.fulfillment.cart.adapter.CartItemCallback
import com.rama.ecommerce.utils.Constant
import com.rama.ecommerce.utils.Helper
import com.rama.ecommerce.utils.extension.toCurrencyFormat

class CheckoutAdapter(
    private val callback: CartItemCallback
) : ListAdapter<Cart, CheckoutAdapter.CheckoutViewHolder>(CartComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckoutViewHolder {
        val binding =
            CartItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CheckoutViewHolder(binding, callback)
    }

    override fun onBindViewHolder(holder: CheckoutViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class CheckoutViewHolder(
        private val binding: CartItemLayoutBinding,
        private val callback: CartItemCallback
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Cart) {
            with(binding) {
                btnDeleteCart.isVisible = false
                checkboxItemCart.isVisible = false
                layout.foreground = null

                ivCartImage.load(data.image) {
                    crossfade(true)
                    placeholder(R.drawable.image_placeholder)
                    error(R.drawable.image_placeholder)
                }
                tvCartName.text = data.productName
                tvCartVariant.text = data.variantName
                tvCartStock.text = getStockText(data.stock)
                tvCartPrice.text = data.productPrice.plus(data.variantPrice).toCurrencyFormat()
                tvCartQuantity.text = data.quantity.toString()

                btnPlusCart.setOnClickListener { callback.addQuantityCallback(data) }
                btnMinusCart.setOnClickListener { callback.removeQuantityCallback(data) }
            }
        }

        private fun getStockText(stock: Int): String {
            return if (stock >= Constant.THRESHOLD_STOCK) {
                binding.tvCartStock.setTextColor(
                    Helper.getColorTheme(
                        itemView.context,
                        com.google.android.material.R.attr.colorOnSurface
                    )
                )
                itemView.context.getString(R.string.total_stock, stock)
            } else {
                binding.tvCartStock.setTextColor(
                    Helper.getColorTheme(
                        itemView.context,
                        com.google.android.material.R.attr.colorError
                    )
                )
                itemView.context.getString(R.string.remaining_stock, stock)
            }
        }
    }
}
