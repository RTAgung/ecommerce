package com.rama.ecommerce.ui.fulfillment.detail.review.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.Review
import com.rama.ecommerce.databinding.ReviewItemLayoutBinding

class ReviewAdapter :
    ListAdapter<Review, ReviewAdapter.ReviewViewHolder>(ReviewComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        val binding =
            ReviewItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ReviewViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ReviewViewHolder(private val binding: ReviewItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Review) {
            with(binding) {
                itemLayout.startAnimation(
                    AnimationUtils.loadAnimation(
                        itemView.context,
                        R.anim.anim_item_list
                    )
                )
                ivReviewImage.load(data.userImage) {
                    crossfade(true)
                    placeholder(R.drawable.image_placeholder)
                    error(R.drawable.image_placeholder)
                }
                tvReviewName.text = data.userName
                tvReviewDesc.text = data.userReview
                ratingBarReview.rating = data.userRating.toFloat()
            }
        }
    }
}
