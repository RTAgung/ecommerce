package com.rama.ecommerce.ui.fulfillment.rating

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.Fulfillment
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.FulfillmentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RatingViewModel @Inject constructor(
    private val fulfillmentRepository: FulfillmentRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    val detailTransaction = savedStateHandle.get<Fulfillment>(RatingFragment.DATA_BUNDLE_KEY)
    val rating = savedStateHandle.get<Int>(RatingFragment.RATING_BUNDLE_KEY)
    val review = savedStateHandle.get<String>(RatingFragment.REVIEW_BUNDLE_KEY)

    private val _ratingState = MutableStateFlow<ResultState<Boolean>?>(null)
    val ratingState: StateFlow<ResultState<Boolean>?> = _ratingState

    fun sendRating(invoiceId: String, rating: Int?, review: String?) {
        viewModelScope.launch {
            fulfillmentRepository.rating(invoiceId, rating, review).collect { result ->
                _ratingState.value = result
            }
        }
    }
}
