package com.rama.ecommerce.ui.fulfillment.notification

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rama.ecommerce.R
import com.rama.ecommerce.databinding.FragmentNotificationBinding
import com.rama.ecommerce.ui.fulfillment.notification.adapter.NotificationAdapter
import com.rama.ecommerce.utils.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NotificationFragment :
    BaseFragment<FragmentNotificationBinding>(FragmentNotificationBinding::inflate) {

    private val viewModel: NotificationViewModel by viewModels()

    private val notificationAdapter: NotificationAdapter by lazy {
        NotificationAdapter { data ->
            viewModel.readNotification(data)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAppbar()
        setupRecyclerView()
        observeNotification()
    }

    private fun observeNotification() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.getNotificationData().collect { data ->
                    showIsDataEmpty(data.isEmpty())
                    notificationAdapter.submitList(data)
                }
            }
        }
    }

    private fun showIsDataEmpty(isEmpty: Boolean) {
        binding.rvNotification.isVisible = !isEmpty
        with(binding.includeError) {
            btnErrorAction.isVisible = false
            tvErrorTitle.text = getString(R.string.empty_error_title)
            tvErrorSubtitle.text = getString(R.string.empty_error_subtitle)
            layoutError.isVisible = isEmpty
        }
    }

    private fun setupRecyclerView() {
        binding.rvNotification.apply {
            adapter = notificationAdapter
            layoutManager = LinearLayoutManager(requireActivity())
            itemAnimator = null
            startAnimation(
                AnimationUtils.loadAnimation(
                    requireActivity(),
                    R.anim.anim_item_list
                )
            )
        }
    }

    private fun setupAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.notification)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }
}
