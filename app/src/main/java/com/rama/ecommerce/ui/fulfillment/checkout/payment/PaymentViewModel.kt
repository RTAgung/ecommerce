package com.rama.ecommerce.ui.fulfillment.checkout.payment

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.firebase.AppFirebaseRemoteConfig
import com.rama.ecommerce.data.model.Payment
import com.rama.ecommerce.data.model.ResultState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PaymentViewModel @Inject constructor(
    private val remoteConfig: AppFirebaseRemoteConfig
) : ViewModel() {

    private val _paymentState = MutableStateFlow<ResultState<List<Payment>>?>(null)
    val paymentState: StateFlow<ResultState<List<Payment>>?> = _paymentState

    init {
        getPayment()
        remoteConfig.onConfigUpdate { result ->
            _paymentState.value = result
        }
    }

    fun getPayment() {
        viewModelScope.launch {
            remoteConfig.fetchPaymentMethod().collect { resultState ->
                _paymentState.value = resultState
            }
        }
    }
}
