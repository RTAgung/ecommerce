package com.rama.ecommerce.ui.fulfillment.detail.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.slideInHorizontally
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.model.Review
import com.rama.ecommerce.utils.Constant
import com.rama.ecommerce.utils.compose.AppTheme
import com.rama.ecommerce.utils.compose.poppinsFamily
import com.rama.ecommerce.utils.extension.getErrorSubtitle
import com.rama.ecommerce.utils.extension.getErrorTitle
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ReviewFragmentCompose : Fragment() {

    private val viewModel: ReviewViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                AppTheme {
                    val resultState = viewModel.reviewProductState.collectAsState().value
                    ReviewFragmentScreen(
                        resultState = resultState,
                        onBackAction = {
                            findNavController().navigateUp()
                        },
                        onErrorAction = { viewModel.getListReview() }
                    )
                }
            }
        }
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "product_id"
    }
}

@Composable
fun ReviewFragmentScreen(
    resultState: ResultState<List<Review>>?,
    onBackAction: () -> Unit,
    onErrorAction: () -> Unit
) {
    var isVisible by remember { mutableStateOf(false) }

    Scaffold(
        topBar = { ReviewAppBar(onBackAction) }
    ) {
        Column(
            Modifier
                .padding(it)
                .fillMaxSize()
        ) {
            when (resultState) {
                is ResultState.Loading -> Loading(Modifier.fillMaxSize())
                is ResultState.Success -> {
                    ReviewList(resultState.data, isVisible)
                    isVisible = true
                }

                is ResultState.Error -> Error(
                    Modifier
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState()),
                    resultState.error,
                    onErrorAction
                )

                else -> {}
            }
        }
    }
}

@Composable
fun Error(modifier: Modifier = Modifier, error: Throwable, onErrorAction: () -> Unit) {
    val title = error.getErrorTitle(LocalContext.current)
    val subtitle = error.getErrorSubtitle(LocalContext.current)
    val action = stringResource(id = R.string.refresh_error_action)
    Column(
        modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.store_error_image),
            contentDescription = null,
            modifier = Modifier.size(128.dp)
        )
        Spacer(modifier = Modifier.height(8.dp))
        Text(
            text = title,
            fontSize = 32.sp,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Medium,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
        Spacer(modifier = Modifier.height(4.dp))
        Text(
            text = subtitle,
            fontSize = 16.sp,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Normal,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
        Spacer(modifier = Modifier.height(8.dp))
        Button(onClick = { onErrorAction() }) {
            Text(
                text = action,
                fontSize = 14.sp,
                style = TextStyle(
                    fontFamily = poppinsFamily,
                    fontWeight = FontWeight.Medium,
                    platformStyle = PlatformTextStyle(
                        includeFontPadding = false,
                    ),
                ),
            )
        }
    }
}

@Composable
fun Loading(modifier: Modifier = Modifier) {
    Column(
        modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun ReviewList(listData: List<Review>, isVisible: Boolean) {
    LazyColumn {
        items(listData) { data ->
            AnimatedVisibility(
                visible = isVisible,
                enter = slideInHorizontally() + fadeIn()
            ) {
                ReviewItem(data)
            }
        }
    }
}

@Composable
fun ReviewItem(data: Review, modifier: Modifier = Modifier) {
    Column(modifier) {
        Row(Modifier.padding(16.dp, 16.dp, 16.dp, 0.dp)) {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(data.userImage)
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(id = R.drawable.image_placeholder),
                error = painterResource(id = R.drawable.image_placeholder),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(36.dp)
                    .clip(CircleShape)
            )
            Column(Modifier.padding(start = 8.dp)) {
                Text(
                    text = data.userName,
                    fontSize = 12.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.SemiBold,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    ),
                )
                Row {
                    for (i in 1..Constant.MAX_RATING) {
                        val color =
                            if (i <= data.userRating) {
                                MaterialTheme.colorScheme.onSurfaceVariant
                            } else {
                                MaterialTheme.colorScheme.outlineVariant
                            }
                        Icon(
                            imageVector = Icons.Default.Star,
                            contentDescription = null,
                            tint = color,
                            modifier = Modifier.size(16.dp)
                        )
                    }
                }
            }
        }
        Text(
            text = data.userReview,
            modifier = Modifier.padding(16.dp, 8.dp, 16.dp, 0.dp),
            fontSize = 12.sp,
            style = TextStyle(
                fontFamily = poppinsFamily,
                fontWeight = FontWeight.Normal,
                platformStyle = PlatformTextStyle(
                    includeFontPadding = false,
                ),
            ),
        )
        Divider(modifier = Modifier.padding(top = 16.dp))
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ReviewAppBar(onBackAction: () -> Unit) {
    Column(Modifier.fillMaxWidth()) {
        TopAppBar(
            title = {
                Text(
                    stringResource(id = R.string.buyer_review),
                    fontSize = 22.sp,
                    style = TextStyle(
                        fontFamily = poppinsFamily,
                        fontWeight = FontWeight.Normal,
                        platformStyle = PlatformTextStyle(
                            includeFontPadding = false,
                        ),
                    )
                )
            },
            navigationIcon = {
                IconButton(onClick = { onBackAction() }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_arrow_back_24),
                        contentDescription = null
                    )
                }
            }
        )
        Divider()
    }
}

@Preview(showBackground = true)
@Composable
fun ReviewFragmentPreview() {
    ReviewFragmentScreen(
        resultState = ResultState.Error(Exception("Oops, something went wrong")),
        onBackAction = {},
        onErrorAction = {}
    )
}
