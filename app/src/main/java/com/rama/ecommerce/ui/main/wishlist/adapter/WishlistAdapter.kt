package com.rama.ecommerce.ui.main.wishlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import coil.load
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.Wishlist
import com.rama.ecommerce.databinding.ProductItemGridLayoutBinding
import com.rama.ecommerce.databinding.ProductItemListLayoutBinding
import com.rama.ecommerce.utils.extension.toCurrencyFormat

class WishlistAdapter(
    private val callback: WishlistCallback
) : ListAdapter<Wishlist, WishlistAdapter.WishlistViewHolder<ViewBinding>>(WishlistComparator) {

    var viewType = ONE_COLUMN_VIEW_TYPE

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WishlistViewHolder<ViewBinding> {
        if (viewType == ONE_COLUMN_VIEW_TYPE) {
            val binding =
                ProductItemListLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return WishlistViewHolder(binding, viewType, callback)
        } else {
            val binding =
                ProductItemGridLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            return WishlistViewHolder(binding, viewType, callback)
        }
    }

    override fun onBindViewHolder(holder: WishlistViewHolder<ViewBinding>, position: Int) {
        holder.bind(getItem(position))
    }

    class WishlistViewHolder<T : ViewBinding>(
        private val binding: T,
        private val viewType: Int,
        private val callback: WishlistCallback
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Wishlist) {
            if (viewType == ONE_COLUMN_VIEW_TYPE) {
                with(binding as ProductItemListLayoutBinding) {
                    cardView.startAnimation(
                        AnimationUtils.loadAnimation(
                            itemView.context,
                            R.anim.anim_item_list
                        )
                    )
                    bindView(
                        data,
                        ivProductImage,
                        tvProductName,
                        tvProductPrice,
                        tvProductStore,
                        tvProductRatingAndSales,
                        btnProductDeleteWishlist,
                        btnProductAddCart
                    )
                }
            } else {
                with(binding as ProductItemGridLayoutBinding) {
                    cardView.startAnimation(
                        AnimationUtils.loadAnimation(
                            itemView.context,
                            if (absoluteAdapterPosition % 2 == 0) {
                                R.anim.anim_item_list
                            } else {
                                R.anim.anim_item_list_reversed
                            }
                        )
                    )
                    bindView(
                        data,
                        ivProductImage,
                        tvProductName,
                        tvProductPrice,
                        tvProductStore,
                        tvProductRatingAndSales,
                        btnProductDeleteWishlist,
                        btnProductAddCart
                    )
                }
            }
            itemView.setOnClickListener { callback.itemClickCallback(data.productId) }
        }

        private fun bindView(
            data: Wishlist,
            productImage: ImageView,
            productName: TextView,
            productPrice: TextView,
            productStore: TextView,
            productRatingAndSales: TextView,
            buttonDelete: Button,
            buttonAddCart: Button
        ) {
            productImage.load(data.image) {
                crossfade(true)
                placeholder(R.drawable.image_placeholder)
                error(R.drawable.image_placeholder)
            }
            productName.text = data.productName
            productPrice.text = data.productPrice.plus(data.variantPrice).toCurrencyFormat()
            productStore.text = data.store
            productRatingAndSales.text =
                itemView.resources.getString(
                    R.string.product_rating_and_sales,
                    data.productRating,
                    data.sale
                )
            buttonDelete.isVisible = true
            buttonDelete.setOnClickListener { callback.deleteCallback(data) }
            buttonAddCart.isVisible = true
            buttonAddCart.setOnClickListener { callback.addCartCallback(data) }
        }
    }

    interface WishlistCallback {
        fun itemClickCallback(productId: String)
        fun deleteCallback(wishlist: Wishlist)
        fun addCartCallback(wishlist: Wishlist)
    }

    companion object {
        const val ONE_COLUMN_VIEW_TYPE = 1
        const val MORE_COLUMN_VIEW_TYPE = 2
    }
}
