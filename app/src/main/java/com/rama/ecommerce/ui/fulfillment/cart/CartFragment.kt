package com.rama.ecommerce.ui.fulfillment.cart

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import com.google.firebase.analytics.FirebaseAnalytics.Param
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.Cart
import com.rama.ecommerce.databinding.FragmentCartBinding
import com.rama.ecommerce.ui.fulfillment.cart.adapter.CartAdapter
import com.rama.ecommerce.ui.fulfillment.cart.adapter.CartItemCallback
import com.rama.ecommerce.ui.fulfillment.checkout.CheckoutFragment
import com.rama.ecommerce.ui.fulfillment.detail.DetailFragmentCompose
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class CartFragment : BaseFragment<FragmentCartBinding>(FragmentCartBinding::inflate) {

    private val viewModel: CartViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    private val cartAdapter: CartAdapter by lazy {
        CartAdapter(object : CartItemCallback {
            override fun itemClickCallback(productId: String) {
                moveToDetailProduct(productId)
            }

            override fun deleteItemCallback(cart: Cart) {
                firebaseAnalytics.sendLogButtonClicked("Cart: Delete")
                viewModel.deleteCart(cart)
                sendLogCart(arrayOf(cart), Event.REMOVE_FROM_CART)
            }

            override fun addQuantityCallback(cart: Cart) {
                firebaseAnalytics.sendLogButtonClicked("Cart: Add Quantity")
                viewModel.updateCartQuantity(cart, true)
            }

            override fun removeQuantityCallback(cart: Cart) {
                firebaseAnalytics.sendLogButtonClicked("Cart: Remove Quantity")
                viewModel.updateCartQuantity(cart, false)
            }

            override fun checkItemCallback(cart: Cart, isCheck: Boolean) {
                viewModel.updateCartChecked(isCheck, cart)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.includeContent.tvCartPrice.text = viewModel.totalPrice.toCurrencyFormat()
        setupAppbar()
        setupAdapter()
        setupAction()
        observeCart()
    }

    private fun moveToDetailProduct(productId: String) {
        findNavController().navigate(
            R.id.action_cartFragment_to_detailFragmentCompose,
            bundleOf(DetailFragmentCompose.BUNDLE_PRODUCT_ID_KEY to productId)
        )
    }

    private fun observeCart() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.getCartData().collect { data ->
                    showIsDataEmpty(data.isEmpty())
                    cartAdapter.submitList(data)
                    sendLogCart(data.toTypedArray(), Event.VIEW_CART)
                    setView(data)
                }
            }
        }
    }

    private fun setView(listCart: List<Cart>) {
        binding.includeContent.checkboxAllCart.isChecked = listCart.all {
            it.isChecked
        }
        binding.includeContent.btnDelete.isVisible = listCart.any { it.isChecked }
        binding.includeContent.btnBuy.isEnabled = listCart.any { it.isChecked }
        viewModel.totalPrice = listCart.filter { it.isChecked }.sumOf {
            (it.productPrice + it.variantPrice) * it.quantity!!
        }
        binding.includeContent.tvCartPrice.text = viewModel.totalPrice.toCurrencyFormat()
    }

    private fun showIsDataEmpty(isEmpty: Boolean) {
        binding.includeContent.layoutContent.isVisible = !isEmpty
        with(binding.includeError) {
            btnErrorAction.isVisible = false
            tvErrorTitle.text = getString(R.string.empty_error_title)
            tvErrorSubtitle.text = getString(R.string.empty_error_subtitle)
            layoutError.isVisible = isEmpty
        }
    }

    private fun setupAction() {
        binding.includeContent.btnDelete.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Cart: Delete All")
            viewModel.deleteCart()
            viewLifecycleOwner.lifecycleScope.launch {
                val listCart =
                    viewModel.getCartData().first().filter { it.isChecked }.toTypedArray()
                sendLogCart(listCart, Event.REMOVE_FROM_CART)
            }
        }
        binding.includeContent.checkboxAllCart.setOnClickListener {
            val isAllChecked = binding.includeContent.checkboxAllCart.isChecked
            viewModel.updateCartChecked(isAllChecked)
        }
        binding.includeContent.btnBuy.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Cart: Buy")
            val listData = runBlocking { viewModel.getCartData().first().filter { it.isChecked } }
            val bundle = Bundle().apply {
                putParcelableArrayList(CheckoutFragment.ARG_DATA, ArrayList<Cart>(listData))
            }
            findNavController().navigate(R.id.action_cartFragment_to_checkoutFragment, bundle)
        }
    }

    private fun sendLogCart(listCart: Array<Cart>, event: String) {
        firebaseAnalytics.logEvent(event) {
            val bundle = ArrayList<Bundle>()
            listCart.map { cart ->
                val itemBundle = Bundle().apply {
                    putString(Param.ITEM_ID, cart.productId)
                    putString(Param.ITEM_NAME, cart.productName)
                    putString(Param.ITEM_BRAND, cart.brand)
                    putString(Param.ITEM_VARIANT, cart.variantName)
                }
                bundle.add(itemBundle)
            }
            param(Param.ITEMS, bundle.toTypedArray())
        }
    }

    private fun setupAdapter() {
        binding.includeContent.rvCart.apply {
            adapter = cartAdapter
            layoutManager = LinearLayoutManager(requireActivity())
            itemAnimator = null
            startAnimation(
                AnimationUtils.loadAnimation(
                    requireActivity(),
                    R.anim.anim_item_list
                )
            )
        }
    }

    private fun setupAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.cart)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }
}
