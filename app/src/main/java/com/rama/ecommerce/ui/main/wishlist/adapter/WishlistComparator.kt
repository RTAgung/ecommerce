package com.rama.ecommerce.ui.main.wishlist.adapter

import androidx.recyclerview.widget.DiffUtil
import com.rama.ecommerce.data.model.Wishlist

object WishlistComparator : DiffUtil.ItemCallback<Wishlist>() {
    override fun areItemsTheSame(oldItem: Wishlist, newItem: Wishlist): Boolean =
        oldItem.productId == newItem.productId

    override fun areContentsTheSame(oldItem: Wishlist, newItem: Wishlist): Boolean =
        oldItem == newItem
}
