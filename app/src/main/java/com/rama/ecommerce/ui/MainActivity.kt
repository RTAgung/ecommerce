package com.rama.ecommerce.ui

import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.doOnEnd
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.splashscreen.SplashScreenViewProvider
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.rama.ecommerce.R
import com.rama.ecommerce.databinding.ActivityMainBinding
import com.rama.ecommerce.utils.Helper
import com.rama.ecommerce.utils.extension.isPreloginFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModels()

    private lateinit var navController: NavController
    private lateinit var navHostFragment: NavHostFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkAppTheme()
        checkAppLanguage()
        installSplashScreen().apply {
            setOnExitAnimationListener { screen ->
                animateSplashscreen(screen)
            }
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupNavController()
        observeLoginSession()
    }

    private fun animateSplashscreen(screen: SplashScreenViewProvider) {
        val zoomX = ObjectAnimator.ofFloat(
            screen.iconView,
            View.SCALE_X,
            1f,
            0.8f,
            4f
        )
        zoomX.duration = 800L
        zoomX.doOnEnd { screen.remove() }

        val zoomY = ObjectAnimator.ofFloat(
            screen.iconView,
            View.SCALE_Y,
            1f,
            0.8f,
            4f
        )
        zoomY.duration = 800L
        zoomY.doOnEnd { screen.remove() }

        val alpha = ObjectAnimator.ofFloat(
            screen.iconView,
            View.ALPHA,
            1f,
            0f
        )
        alpha.duration = 800
        alpha.doOnEnd { screen.remove() }

        alpha.start()
        zoomX.start()
        zoomY.start()
    }

    private fun checkAppLanguage() {
        runBlocking {
            val appLanguage = viewModel.getAppLanguage().first()
            Helper.setAppLanguage(appLanguage)
        }
    }

    private fun checkAppTheme() {
        runBlocking {
            val isDarkTheme = viewModel.getAppTheme().first()
            Helper.setAppTheme(this@MainActivity, isDarkTheme)
        }
    }

    private fun setupNavController() {
        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
    }

    private fun observeLoginSession() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.checkUserAuthorization().distinctUntilChanged().collect { isAuthorize ->
                    val currentFragment = navHostFragment.childFragmentManager.fragments[0]
                    if (!isAuthorize && !currentFragment.isPreloginFragment()) {
                        viewModel.makeLogout()
                        navController.navigate(R.id.action_global_prelogin_navigation)
                    }
                }
            }
        }
    }
}
