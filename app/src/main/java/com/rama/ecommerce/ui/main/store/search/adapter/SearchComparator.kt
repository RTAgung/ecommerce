package com.rama.ecommerce.ui.main.store.search.adapter

import androidx.recyclerview.widget.DiffUtil

object SearchComparator : DiffUtil.ItemCallback<String>() {
    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean =
        oldItem == newItem

    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean =
        oldItem == newItem
}
