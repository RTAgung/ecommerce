package com.rama.ecommerce.ui.fulfillment.rating

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isInvisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.databinding.FragmentRatingBinding
import com.rama.ecommerce.ui.main.MainFragment
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.extension.getErrorMessage
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.showSnackbar
import com.rama.ecommerce.utils.extension.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RatingFragment : BaseFragment<FragmentRatingBinding>(FragmentRatingBinding::inflate) {

    private val viewModel: RatingViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBackPresser()
        setupAppbar()
        setupView()
        setupAction()
        observeRating()
        playAnimation()
    }

    private fun playAnimation() {
        val listLayout = listOf(
            binding.feedbackLayout,
            binding.tvDetail,
            binding.detailLayout,
        )
        val animSet = AnimatorSet()
        var animBefore: ObjectAnimator? = null
        listLayout.reversed().forEach { view ->
            val animAlpha = ObjectAnimator.ofFloat(view, View.ALPHA, 1f).apply {
                duration = 400
                interpolator = AccelerateInterpolator()
            }
            val animTranslation = ObjectAnimator.ofFloat(view, View.TRANSLATION_X, 1f).apply {
                duration = 400
                interpolator = DecelerateInterpolator()
            }
            if (animBefore != null) {
                animSet.play(animAlpha).before(animBefore)
            } else {
                animSet.play(animAlpha)
            }
            animSet.play(animTranslation).with(animAlpha)
            animBefore = animAlpha
        }
        animSet.start()
    }

    private fun setupAction() {
        binding.btnFinish.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Rating: Finish")
            val rating: Int? = with(binding.ratingBarScore.rating.toInt()) {
                if (this == 0) {
                    null
                } else {
                    this
                }
            }
            val review: String? = with(binding.editTextReview.text.toString()) {
                this.ifEmpty { null }
            }
            val invoiceId = viewModel.detailTransaction!!.invoiceId
            viewModel.sendRating(invoiceId, rating, review)
        }
    }

    private fun observeRating() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.ratingState.collect { result ->
                    if (result != null) {
                        showLoading(result is ResultState.Loading)
                        when (result) {
                            is ResultState.Loading -> {}
                            is ResultState.Success -> if (result.data) actionToMovePage(true)
                            is ResultState.Error -> {
                                val message = result.error.getErrorMessage()
                                binding.root.showSnackbar(message)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.btnFinish.isInvisible = isLoading
        binding.loadingRating.isInvisible = !isLoading
    }

    private fun setupBackPresser() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    actionToMovePage()
                }
            }
        )
    }

    private fun actionToMovePage(isBtnFinishClicked: Boolean = false) {
        val backStackEntryCount: Int = parentFragmentManager.backStackEntryCount
        if (backStackEntryCount == 0) {
            findNavController().navigate(R.id.action_ratingFragment_to_mainFragment)
        } else {
            if (isBtnFinishClicked) {
                val bundle = Bundle().apply {
                    putBoolean(MainFragment.MOVE_TRANSACTION_BUNDLE_KEY, true)
                }
                findNavController().navigate(R.id.action_ratingFragment_to_mainFragment, bundle)
            } else {
                findNavController().navigateUp()
            }
        }
    }

    private fun setupAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.status)
        toolbar.isTitleCentered = true
    }

    private fun setupView() {
        val data = viewModel.detailTransaction
        with(binding) {
            tvTransactionId.text = data?.invoiceId
            tvTransactionStatus.text =
                if (data?.status == true) {
                    getString(R.string.success)
                } else {
                    getString(R.string.failed)
                }
            tvTransactionDate.text = data?.date
            tvTransactionTime.text = data?.time
            tvTransactionPaymentMethod.text = data?.payment
            tvTransactionTotalPayment.text = data?.total?.toCurrencyFormat()
            ratingBarScore.rating = viewModel.rating?.toFloat() ?: 0F
            editTextReview.setText(viewModel.review)
        }
    }

    companion object {
        const val RATING_BUNDLE_KEY = "rating_bundle_key"
        const val REVIEW_BUNDLE_KEY = "review_bundle_key"
        const val DATA_BUNDLE_KEY = "data_bundle_key"
    }
}
