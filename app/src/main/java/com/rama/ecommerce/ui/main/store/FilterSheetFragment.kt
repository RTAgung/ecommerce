package com.rama.ecommerce.ui.main.store

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.os.bundleOf
import androidx.core.view.isInvisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.setFragmentResult
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import com.google.firebase.analytics.FirebaseAnalytics.Param
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.databinding.FragmentFilterSheetBinding
import com.rama.ecommerce.utils.extension.sendLogButtonClicked

class FilterSheetFragment : BottomSheetDialogFragment() {

    private var sort: String? = null
    private var sortResId: Int? = null
    private var brand: String? = null
    private var brandResId: Int? = null
    private var lowest: String? = null
    private var highest: String? = null

    private var _binding: FragmentFilterSheetBinding? = null
    private val binding get() = _binding!!

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            sort = it.getString(ARG_SORT)
            brand = it.getString(ARG_BRAND)
            lowest = it.getString(ARG_LOWEST)
            highest = it.getString(ARG_HIGHEST)
        }
    }

    override fun onGetLayoutInflater(savedInstanceState: Bundle?): LayoutInflater {
        val inflater = super.onGetLayoutInflater(savedInstanceState)
        val contextThemeWrapper: Context =
            ContextThemeWrapper(requireContext(), R.style.Theme_Ecommerce)
        return inflater.cloneInContext(contextThemeWrapper)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFilterSheetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val modalBottomSheetBehavior = (dialog as BottomSheetDialog).behavior
        modalBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        setView()
        setAction()
    }

    private fun setAction() {
        binding.btnResetFilter.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Filter: Reset")
            actionResetFilter()
        }
        binding.btnShowFilter.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Filter: Show Products")
            actionReturnFilterData()
        }
        binding.editTextLowestPrice.doOnTextChanged { _, _, _, _ -> changeResetButtonVisibility() }
        binding.editTextHighestPrice.doOnTextChanged { _, _, _, _ -> changeResetButtonVisibility() }
    }

    private fun actionReturnFilterData() {
        lowest = binding.editTextLowestPrice.text.toString().let {
            it.ifEmpty { null }
        }
        highest = binding.editTextHighestPrice.text.toString().let {
            it.ifEmpty { null }
        }

        firebaseAnalytics.logEvent(Event.SELECT_ITEM) {
            val bundle = Bundle().apply {
                sort?.let { putString(Param.ITEM_CATEGORY, it) }
                brand?.let { putString(Param.ITEM_CATEGORY2, it) }
                lowest?.let { putString(Param.ITEM_CATEGORY3, it) }
                highest?.let { putString(Param.ITEM_CATEGORY4, it) }
            }
            param(Param.ITEMS, arrayOf(bundle))
        }

        val bundle = bundleOf(
            BUNDLE_SORT_KEY to sort,
            BUNDLE_SORT_RES_ID_KEY to sortResId,
            BUNDLE_BRAND_KEY to brand,
            BUNDLE_BRAND_RES_ID_KEY to brandResId,
            BUNDLE_LOWEST_KEY to lowest,
            BUNDLE_HIGHEST_KEY to highest
        )
        setFragmentResult(FRAGMENT_REQUEST_KEY, bundle)
        dismiss()
    }

    private fun actionResetFilter() {
        sort = null
        brand = null
        lowest = null
        highest = null
        sortResId = null
        brandResId = null
        binding.chipSortGroup.clearCheck()
        binding.chipBrandGroup.clearCheck()
        binding.editTextLowestPrice.setText("")
        binding.editTextHighestPrice.setText("")
    }

    private fun setView() {
        binding.editTextLowestPrice.setText(lowest ?: "")
        binding.editTextHighestPrice.setText(highest ?: "")
        setSortChipView()
        setBrandChipView()
        changeResetButtonVisibility()
    }

    private fun setBrandChipView() {
        val listBrandChips = mapOf(
            binding.chipBrandApple to R.string.apple,
            binding.chipBrandAsus to R.string.asus,
            binding.chipBrandDell to R.string.dell,
            binding.chipBrandLenovo to R.string.lenovo
        )

        listBrandChips.forEach { (chip, stringId) ->
            chip.setOnClickListener {
                val checkChipId = binding.chipBrandGroup.checkedChipId
                if (checkChipId == -1) {
                    brand = null
                    brandResId = null
                } else {
                    brand = chip.text.toString()
                    brandResId = stringId
                }
                changeResetButtonVisibility()
            }

            if (!brand.isNullOrEmpty() && brand == getString(stringId)) {
                chip.isChecked = true
                brandResId = stringId
            }
        }
    }

    private fun setSortChipView() {
        val listSortChips = mapOf(
            binding.chipSortReview to R.string.review,
            binding.chipSortSale to R.string.sale,
            binding.chipSortLowest to R.string.lowest_price,
            binding.chipSortHighest to R.string.highest_price
        )

        listSortChips.forEach { (chip, stringId) ->
            chip.setOnClickListener {
                val checkChipId = binding.chipSortGroup.checkedChipId
                if (checkChipId == -1) {
                    sort = null
                    sortResId = null
                } else {
                    sort = chip.text.toString()
                    sortResId = stringId
                }
                changeResetButtonVisibility()
            }

            if (!sort.isNullOrEmpty() && sort == getString(stringId)) {
                chip.isChecked = true
                sortResId = stringId
            }
        }
    }

    private fun changeResetButtonVisibility() {
        with(binding) {
            val isSortChipExisted = chipSortGroup.checkedChipId == -1
            val isBrandChipExisted = chipBrandGroup.checkedChipId == -1
            val isLowestPriceTextEmpty = editTextLowestPrice.text.toString().isEmpty()
            val isHighestPriceTextEmpty = editTextHighestPrice.text.toString().isEmpty()

            btnResetFilter.isInvisible =
                isSortChipExisted && isBrandChipExisted && isLowestPriceTextEmpty && isHighestPriceTextEmpty
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        private const val ARG_SORT = "arg_sort"
        private const val ARG_BRAND = "arg_brand"
        private const val ARG_LOWEST = "arg_lowest"
        private const val ARG_HIGHEST = "arg_highest"

        const val TAG = "FilterSheetFragment"
        const val FRAGMENT_REQUEST_KEY = "filter_sheet_fragment_request_key"

        const val BUNDLE_SORT_KEY = "sort"
        const val BUNDLE_SORT_RES_ID_KEY = "sort_res_id"
        const val BUNDLE_BRAND_KEY = "brand"
        const val BUNDLE_BRAND_RES_ID_KEY = "brand_res_id"
        const val BUNDLE_LOWEST_KEY = "lowest"
        const val BUNDLE_HIGHEST_KEY = "highest"

        @JvmStatic
        fun newInstance(sort: String?, brand: String?, lowest: String?, highest: String?) =
            FilterSheetFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_SORT, sort)
                    putString(ARG_BRAND, brand)
                    putString(ARG_LOWEST, lowest)
                    putString(ARG_HIGHEST, highest)
                }
            }
    }
}
