package com.rama.ecommerce.ui.fulfillment.checkout.payment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.Payment
import com.rama.ecommerce.databinding.PaymentSubitemLayoutBinding
import com.rama.ecommerce.utils.Constant
import com.rama.ecommerce.utils.Helper

class PaymentItemAdapter(
    private val itemClickCallback: (Payment.PaymentItem) -> Unit
) : ListAdapter<Payment.PaymentItem, PaymentItemAdapter.PaymentItemViewHolder>(PaymentItemComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentItemViewHolder {
        val binding =
            PaymentSubitemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PaymentItemViewHolder(binding, itemClickCallback)
    }

    override fun onBindViewHolder(holder: PaymentItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class PaymentItemViewHolder(
        private val binding: PaymentSubitemLayoutBinding,
        private val itemClickCallback: (Payment.PaymentItem) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Payment.PaymentItem) {
            binding.layoutPaymentSubitem.startAnimation(
                AnimationUtils.loadAnimation(
                    itemView.context,
                    R.anim.anim_item_list
                )
            )
            binding.ivPaymentLogo.load(data.image) {
                crossfade(true)
                placeholder(R.drawable.image_placeholder)
                error(R.drawable.image_placeholder)
            }
            binding.tvPaymentLabel.text = data.label
            if (!data.status) {
                with(binding.layoutPaymentSubitem) {
                    alpha = Constant.TRANSPARENT_ALPHA
                    isClickable = false
                    isFocusable = false
                    foreground = null
                    setBackgroundColor(
                        Helper.getColorTheme(
                            itemView.context,
                            com.google.android.material.R.attr.colorTertiaryContainer
                        )
                    )
                }
            } else {
                itemView.setOnClickListener { itemClickCallback(data) }
            }
        }
    }
}
