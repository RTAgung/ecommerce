package com.rama.ecommerce.ui.main.store.search.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rama.ecommerce.R
import com.rama.ecommerce.databinding.SearchItemLayoutBinding

class SearchProductAdapter(
    private val itemClickCallback: (String) -> Unit
) : ListAdapter<String, SearchProductAdapter.SearchProductViewHolder>(SearchComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchProductViewHolder {
        val binding =
            SearchItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchProductViewHolder(binding, itemClickCallback)
    }

    override fun onBindViewHolder(holder: SearchProductViewHolder, position: Int) {
        val data = getItem(position)
        holder.bind(data)
    }

    class SearchProductViewHolder(
        private val binding: SearchItemLayoutBinding,
        private val itemClickCallback: (String) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: String) {
            binding.searchItem.startAnimation(
                AnimationUtils.loadAnimation(
                    itemView.context,
                    R.anim.anim_item_list
                )
            )
            binding.tvSearchKeyword.text = data
            itemView.setOnClickListener { itemClickCallback(data) }
        }
    }
}
