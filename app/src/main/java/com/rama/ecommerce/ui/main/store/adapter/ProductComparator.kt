package com.rama.ecommerce.ui.main.store.adapter

import androidx.recyclerview.widget.DiffUtil
import com.rama.ecommerce.data.model.Product

object ProductComparator : DiffUtil.ItemCallback<Product>() {
    override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean =
        oldItem.productId == newItem.productId

    override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean =
        oldItem == newItem
}
