package com.rama.ecommerce.ui.fulfillment.cart.adapter

import com.rama.ecommerce.data.model.Cart

interface CartItemCallback {
    fun itemClickCallback(productId: String) {}
    fun deleteItemCallback(cart: Cart) {}
    fun addQuantityCallback(cart: Cart) {}
    fun removeQuantityCallback(cart: Cart) {}
    fun checkItemCallback(cart: Cart, isCheck: Boolean) {}
}
