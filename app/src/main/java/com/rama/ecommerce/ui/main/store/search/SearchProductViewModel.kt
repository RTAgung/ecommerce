package com.rama.ecommerce.ui.main.store.search

import androidx.lifecycle.ViewModel
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.StoreRepository
import com.rama.ecommerce.utils.Constant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class SearchProductViewModel @Inject constructor(
    private val storeRepository: StoreRepository
) : ViewModel() {

    private val _searchEditText: MutableStateFlow<String> = MutableStateFlow("")

    private val _searchQuery: Flow<String> = _searchEditText.debounce(Constant.DEBOUNCE_DURATION)

    val searchData: Flow<ResultState<List<String>>> = _searchQuery.flatMapLatest { query ->
        storeRepository.searchProducts(query)
    }

    fun getSearchData(query: String) {
        runBlocking {
            _searchEditText.value = query
        }
    }
}
