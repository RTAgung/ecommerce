package com.rama.ecommerce.ui.main.profile

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.PreloginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val preloginRepository: PreloginRepository
) : ViewModel() {

    var currentImageUri: Uri? = null

    private val _uploadState = MutableStateFlow<ResultState<Boolean>?>(null)
    val uploadState: StateFlow<ResultState<Boolean>?> = _uploadState

    fun uploadProfile(userName: RequestBody, userImage: MultipartBody.Part?) {
        viewModelScope.launch {
            preloginRepository.uploadProfile(userName, userImage).collect { resultState ->
                _uploadState.value = resultState
            }
        }
    }
}
