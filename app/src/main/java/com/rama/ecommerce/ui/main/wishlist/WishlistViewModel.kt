package com.rama.ecommerce.ui.main.wishlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.Wishlist
import com.rama.ecommerce.data.repository.CartRepository
import com.rama.ecommerce.data.repository.WishlistRepository
import com.rama.ecommerce.ui.main.wishlist.adapter.WishlistAdapter
import com.rama.ecommerce.utils.extension.toCart
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(
    private val cartRepository: CartRepository,
    private val wishlistRepository: WishlistRepository
) : ViewModel() {

    var recyclerViewType = WishlistAdapter.ONE_COLUMN_VIEW_TYPE

    fun getWishlistData(): Flow<List<Wishlist>> = wishlistRepository.getWishlistData()

    fun deleteWishlist(wishlist: Wishlist) {
        viewModelScope.launch {
            wishlistRepository.deleteWishlist(wishlist)
        }
    }

    fun insertCart(wishlist: Wishlist): Boolean {
        val cart = wishlist.toCart()
        val isStockReady = runBlocking { cartRepository.isStockReady(cart) }
        return if (isStockReady) {
            viewModelScope.launch {
                cartRepository.insertCart(cart)
            }
            true
        } else {
            false
        }
    }
}
