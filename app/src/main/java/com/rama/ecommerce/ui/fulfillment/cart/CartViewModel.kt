package com.rama.ecommerce.ui.fulfillment.cart

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.Cart
import com.rama.ecommerce.data.repository.CartRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val cartRepository: CartRepository
) : ViewModel() {

    var totalPrice = 0

    fun getCartData(): Flow<List<Cart>> = cartRepository.getCartData()

    fun deleteCart(cart: Cart? = null) {
        viewModelScope.launch {
            if (cart != null) {
                cartRepository.deleteCart(cart)
            } else {
                val filteredCart = getCartData().first().filter { it.isChecked }
                filteredCart.map { cart ->
                    cartRepository.deleteCart(cart)
                }
            }
        }
    }

    fun updateCartQuantity(cart: Cart, isInsert: Boolean) {
        viewModelScope.launch {
            if (isInsert && cart.stock > cart.quantity!!) {
                cartRepository.updateCartQuantity(cart, true)
            } else if (!isInsert && cart.quantity!! > 1) {
                cartRepository.updateCartQuantity(cart, false)
            }
        }
    }

    fun updateCartChecked(isChecked: Boolean, cart: Cart? = null) {
        viewModelScope.launch {
            if (cart != null) {
                cartRepository.updateCartChecked(isChecked, cart)
            } else {
                val listCart = getCartData().first().filter {
                    it.isChecked != isChecked
                }
                listCart.map { cart ->
                    cartRepository.updateCartChecked(isChecked, cart)
                }
            }
        }
    }
}
