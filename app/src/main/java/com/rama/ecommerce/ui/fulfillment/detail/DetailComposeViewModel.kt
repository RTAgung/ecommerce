package com.rama.ecommerce.ui.fulfillment.detail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.DetailProduct
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.CartRepository
import com.rama.ecommerce.data.repository.StoreRepository
import com.rama.ecommerce.data.repository.WishlistRepository
import com.rama.ecommerce.utils.extension.toCart
import com.rama.ecommerce.utils.extension.toWishlist
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class DetailComposeViewModel @Inject constructor(
    private val storeRepository: StoreRepository,
    private val wishlistRepository: WishlistRepository,
    private val cartRepository: CartRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val _composeState = MutableStateFlow(DetailComposeState())
    val composeState = _composeState.asStateFlow()

    val productId: String = savedStateHandle[DetailFragment.BUNDLE_PRODUCT_ID_KEY] ?: ""

    init {
        initState()
        getDetailProduct()
    }

    private fun initState() {
        _composeState.update {
            it.copy(productId = productId)
        }
    }

    fun getDetailProduct() {
        viewModelScope.launch {
            storeRepository.detailProducts(productId).collect { resultState ->
                _composeState.update {
                    if (resultState is ResultState.Success) {
                        it.copy(
                            resultState = resultState,
                            detailProduct = resultState.data,
                            productVariant = resultState.data.productVariant[0],
                            isWishlist = checkExistWishlist()
                        )
                    } else {
                        it.copy(resultState = resultState)
                    }
                }
            }
        }
    }

    private fun checkExistWishlist(): Boolean {
        return runBlocking {
            wishlistRepository.checkExistWishlistData(productId)
        }
    }

    fun insertWishlist() {
        viewModelScope.launch {
            val product = _composeState.value.detailProduct
            val variant = _composeState.value.productVariant
            wishlistRepository.insertWishlist(product!!.toWishlist(variant!!))
        }
        _composeState.update {
            it.copy(isWishlist = true)
        }
    }

    fun deleteWishlist() {
        viewModelScope.launch {
            val product = _composeState.value.detailProduct
            val variant = _composeState.value.productVariant
            wishlistRepository.deleteWishlist(product!!.toWishlist(variant!!))
        }
        _composeState.update {
            it.copy(isWishlist = false)
        }
    }

    fun updateVariant(variant: DetailProduct.ProductVariant) {
        _composeState.update {
            it.copy(productVariant = variant)
        }
    }

    fun insertCart(): Boolean {
        val product = composeState.value.detailProduct!!
        val variant = composeState.value.productVariant!!
        val cart = product.toCart(variant)
        val isStockReady = runBlocking { cartRepository.isStockReady(cart) }
        return if (isStockReady) {
            viewModelScope.launch {
                cartRepository.insertCart(cart)
            }
            true
        } else {
            false
        }
    }
}

data class DetailComposeState(
    val productId: String = "",
    val isWishlist: Boolean = false,
    val detailProduct: DetailProduct? = null,
    val productVariant: DetailProduct.ProductVariant? = null,
    val resultState: ResultState<DetailProduct>? = null
)
