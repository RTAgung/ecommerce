package com.rama.ecommerce.ui.prelogin.onboarding

import androidx.lifecycle.ViewModel
import com.rama.ecommerce.data.repository.PreloginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class OnboardingViewModel @Inject constructor(
    private val preloginRepository: PreloginRepository
) : ViewModel() {

    fun setFirstTimeRunApp(isFirstTime: Boolean = true) {
        runBlocking {
            preloginRepository.setFirstTimeRunApp(isFirstTime)
        }
    }
}
