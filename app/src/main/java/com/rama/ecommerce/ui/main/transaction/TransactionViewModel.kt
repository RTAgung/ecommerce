package com.rama.ecommerce.ui.main.transaction

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.model.Transaction
import com.rama.ecommerce.data.repository.FulfillmentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(
    private val fulfillmentRepository: FulfillmentRepository
) : ViewModel() {

    private val _transactionState = MutableStateFlow<ResultState<List<Transaction>>?>(null)
    val transactionState: StateFlow<ResultState<List<Transaction>>?> = _transactionState

    init {
        getListTransaction()
    }

    fun getListTransaction() {
        viewModelScope.launch {
            fulfillmentRepository.getTransaction().collect { resultState ->
                _transactionState.value = resultState
            }
        }
    }
}
