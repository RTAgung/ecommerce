package com.rama.ecommerce.ui.fulfillment.cart.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.Cart
import com.rama.ecommerce.databinding.CartItemLayoutBinding
import com.rama.ecommerce.utils.Constant
import com.rama.ecommerce.utils.Helper
import com.rama.ecommerce.utils.extension.toCurrencyFormat

class CartAdapter(
    private val callback: CartItemCallback
) : ListAdapter<Cart, CartAdapter.CartViewHolder>(CartComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val binding =
            CartItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CartViewHolder(binding, callback)
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class CartViewHolder(
        private val binding: CartItemLayoutBinding,
        private val callback: CartItemCallback
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Cart) {
            with(binding) {
                ivCartImage.load(data.image) {
                    crossfade(true)
                    placeholder(R.drawable.image_placeholder)
                    error(R.drawable.image_placeholder)
                }
                tvCartName.text = data.productName
                tvCartVariant.text = data.variantName
                tvCartStock.text = getStockText(data.stock)
                tvCartPrice.text = data.productPrice.plus(data.variantPrice).toCurrencyFormat()
                tvCartQuantity.text = data.quantity.toString()
                checkboxItemCart.isChecked = data.isChecked

                btnDeleteCart.setOnClickListener { callback.deleteItemCallback(data) }
                btnPlusCart.setOnClickListener { callback.addQuantityCallback(data) }
                btnMinusCart.setOnClickListener { callback.removeQuantityCallback(data) }
                checkboxItemCart.setOnClickListener {
                    callback.checkItemCallback(data, !data.isChecked)
                }
            }
            itemView.setOnClickListener { callback.itemClickCallback(data.productId) }
        }

        private fun getStockText(stock: Int): String {
            return if (stock >= Constant.THRESHOLD_STOCK) {
                binding.tvCartStock.setTextColor(
                    Helper.getColorTheme(
                        itemView.context,
                        com.google.android.material.R.attr.colorOnSurface
                    )
                )
                itemView.context.getString(R.string.total_stock, stock)
            } else {
                binding.tvCartStock.setTextColor(
                    Helper.getColorTheme(
                        itemView.context,
                        com.google.android.material.R.attr.colorError
                    )
                )
                itemView.context.getString(R.string.remaining_stock, stock)
            }
        }
    }
}
