package com.rama.ecommerce.ui.main.transaction

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.databinding.FragmentTransactionBinding
import com.rama.ecommerce.ui.fulfillment.rating.RatingFragment
import com.rama.ecommerce.ui.main.transaction.adapter.TransactionAdapter
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.extension.getErrorSubtitle
import com.rama.ecommerce.utils.extension.getErrorTitle
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.toFulfillment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.HttpException
import retrofit2.Response
import java.net.HttpURLConnection

@AndroidEntryPoint
class TransactionFragment :
    BaseFragment<FragmentTransactionBinding>(FragmentTransactionBinding::inflate) {

    private val viewModel: TransactionViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    private val transactionAdapter: TransactionAdapter by lazy {
        TransactionAdapter { data ->
            firebaseAnalytics.sendLogButtonClicked("Transaction: Review")
            val bundle = Bundle().apply {
                putParcelable(RatingFragment.DATA_BUNDLE_KEY, data.toFulfillment())
                putInt(RatingFragment.RATING_BUNDLE_KEY, data.rating)
                putString(RatingFragment.REVIEW_BUNDLE_KEY, data.review)
            }
            Navigation.findNavController(requireActivity(), R.id.nav_host_fragment).navigate(
                R.id.action_mainFragment_to_ratingFragment,
                bundle
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        observeTransaction()
    }

    private fun observeTransaction() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.transactionState.collect { result ->
                    if (result != null) {
                        showLoading(result is ResultState.Loading)
                        when (result) {
                            is ResultState.Loading -> {}
                            is ResultState.Success -> {
                                if (result.data.isEmpty()) {
                                    showErrorView(
                                        HttpException(
                                            Response.error<String>(
                                                HttpURLConnection.HTTP_NOT_FOUND,
                                                "".toResponseBody()
                                            )
                                        )
                                    )
                                } else {
                                    transactionAdapter.submitList(result.data)
                                }
                            }

                            is ResultState.Error -> showErrorView(result.error)
                        }
                    }
                }
            }
        }
    }

    private fun showErrorView(error: Throwable) {
        val title = error.getErrorTitle(requireActivity())
        val subtitle = error.getErrorSubtitle(requireActivity())
        val action = getString(R.string.refresh_error_action)

        binding.rvTransaction.isVisible = false
        with(binding.includeError) {
            tvErrorTitle.text = title
            tvErrorSubtitle.text = subtitle
            btnErrorAction.text = action

            btnErrorAction.setOnClickListener { viewModel.getListTransaction() }
            layoutError.isVisible = true
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.loadingTransaction.isVisible = isLoading
        binding.rvTransaction.isVisible = !isLoading
        binding.includeError.layoutError.isVisible = false
    }

    private fun setupRecyclerView() {
        binding.rvTransaction.apply {
            adapter = transactionAdapter
            layoutManager = LinearLayoutManager(requireActivity())
        }
    }
}
