package com.rama.ecommerce.ui.main.transaction.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.Transaction
import com.rama.ecommerce.databinding.TransactionItemLayoutBinding
import com.rama.ecommerce.utils.extension.toCurrencyFormat

class TransactionAdapter(private val onItemClick: (Transaction) -> Unit) :
    ListAdapter<Transaction, TransactionAdapter.TransactionViewHolder>(TransactionComparator) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TransactionViewHolder {
        val binding =
            TransactionItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TransactionViewHolder(binding, onItemClick)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class TransactionViewHolder(
        private val binding: TransactionItemLayoutBinding,
        private val onItemClick: (Transaction) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Transaction) {
            with(binding) {
                itemLayout.startAnimation(
                    AnimationUtils.loadAnimation(
                        itemView.context,
                        R.anim.anim_item_list
                    )
                )
                tvTransactionDate.text = data.date
                ivTransactionImage.load(data.image) {
                    crossfade(true)
                    placeholder(R.drawable.image_placeholder)
                    error(R.drawable.image_placeholder)
                }
                tvTransactionName.text = data.name
                tvTransactionTotalItem.text =
                    itemView.context.getString(R.string.total_item, data.items.size)
                tvTransactionTotalShopping.text = data.total.toCurrencyFormat()
                if (data.rating == 0 || data.review.isEmpty()) {
                    btnReview.setOnClickListener { onItemClick(data) }
                } else {
                    btnReview.isVisible = false
                }
            }
        }
    }
}
