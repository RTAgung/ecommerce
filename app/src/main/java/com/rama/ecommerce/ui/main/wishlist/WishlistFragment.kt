package com.rama.ecommerce.ui.main.wishlist

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.Wishlist
import com.rama.ecommerce.databinding.FragmentWishlistBinding
import com.rama.ecommerce.ui.fulfillment.detail.DetailFragmentCompose
import com.rama.ecommerce.ui.main.wishlist.adapter.WishlistAdapter
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class WishlistFragment : BaseFragment<FragmentWishlistBinding>(FragmentWishlistBinding::inflate) {

    private val viewModel: WishlistViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    private val wishlistAdapter: WishlistAdapter by lazy {
        WishlistAdapter(object : WishlistAdapter.WishlistCallback {
            override fun itemClickCallback(productId: String) {
                moveToDetailProduct(productId)
            }

            override fun deleteCallback(wishlist: Wishlist) {
                firebaseAnalytics.sendLogButtonClicked("Wishlist: Delete")
                viewModel.deleteWishlist(wishlist)
                binding.root.showSnackbar(getString(R.string.wishlist_remove_successful))
            }

            override fun addCartCallback(wishlist: Wishlist) {
                firebaseAnalytics.sendLogButtonClicked("Wishlist: Add Cart")
                actionAddCart(wishlist)
            }
        })
    }

    private fun actionAddCart(wishlist: Wishlist) {
        if (viewModel.insertCart(wishlist)) {
            sendLogAddToCart(wishlist)
            binding.root.showSnackbar(getString(R.string.cart_insert_successful))
        } else {
            binding.root.showSnackbar(getString(R.string.stock_is_unavailable))
        }
    }

    private fun sendLogAddToCart(wishlist: Wishlist) {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
            val bundleProduct = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, wishlist.productId)
                putString(FirebaseAnalytics.Param.ITEM_NAME, wishlist.productName)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, wishlist.brand)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, wishlist.variantName)
            }
            param(FirebaseAnalytics.Param.ITEMS, arrayOf(bundleProduct))
        }
    }

    private fun moveToDetailProduct(productId: String) {
        Navigation.findNavController(requireActivity(), R.id.nav_host_fragment).navigate(
            R.id.action_mainFragment_to_detailFragmentCompose,
            bundleOf(DetailFragmentCompose.BUNDLE_PRODUCT_ID_KEY to productId)
        )
    }

    private lateinit var gridLayoutManager: GridLayoutManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
        setupAction()
        observeWishlist()
    }

    private fun observeWishlist() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.getWishlistData().collect { data ->
                    showIsDataEmpty(data.isEmpty())
                    binding.includeContent.tvWishlistTotalItem.text =
                        getString(R.string.total_item, data.size)
                    wishlistAdapter.submitList(data)
                }
            }
        }
    }

    private fun showIsDataEmpty(isEmpty: Boolean) {
        binding.includeContent.layoutContent.isVisible = !isEmpty
        with(binding.includeError) {
            btnErrorAction.isVisible = false
            tvErrorTitle.text = getString(R.string.empty_error_title)
            tvErrorSubtitle.text = getString(R.string.empty_error_subtitle)
            layoutError.isVisible = isEmpty
        }
    }

    private fun setupAction() {
        binding.includeContent.btnChangeRecyclerView.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Wishlist: Change Recycler View")
            viewModel.recyclerViewType =
                if (viewModel.recyclerViewType == WishlistAdapter.ONE_COLUMN_VIEW_TYPE) {
                    WishlistAdapter.MORE_COLUMN_VIEW_TYPE
                } else {
                    WishlistAdapter.ONE_COLUMN_VIEW_TYPE
                }
            changeRecyclerViewType()
            setImageBtnChangeRecyclerView()
        }
    }

    private fun setupView() {
        setAdapter()
        setImageBtnChangeRecyclerView()
        changeRecyclerViewType()
    }

    private fun changeRecyclerViewType() {
        if (viewModel.recyclerViewType == WishlistAdapter.ONE_COLUMN_VIEW_TYPE) {
            wishlistAdapter.viewType = WishlistAdapter.ONE_COLUMN_VIEW_TYPE
            gridLayoutManager.spanCount = 1
        } else {
            wishlistAdapter.viewType = WishlistAdapter.MORE_COLUMN_VIEW_TYPE
            gridLayoutManager.spanCount = 2
        }
    }

    private fun setImageBtnChangeRecyclerView() {
        binding.includeContent.btnChangeRecyclerView.setImageResource(
            if (viewModel.recyclerViewType == WishlistAdapter.ONE_COLUMN_VIEW_TYPE) {
                R.drawable.ic_grid_view_24
            } else {
                R.drawable.ic_format_list_bulleted_24
            }
        )
    }

    private fun setAdapter() {
        gridLayoutManager = GridLayoutManager(requireActivity(), 1)

        binding.includeContent.rvWishlist.adapter = wishlistAdapter
        binding.includeContent.rvWishlist.layoutManager = gridLayoutManager

        wishlistAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                binding.includeContent.rvWishlist.scrollToPosition(0)
            }
        })
    }
}
