package com.rama.ecommerce.ui.fulfillment.detail

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.google.android.material.chip.Chip
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import com.google.firebase.analytics.FirebaseAnalytics.Param
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.DetailProduct
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.databinding.FragmentDetailBinding
import com.rama.ecommerce.ui.fulfillment.checkout.CheckoutFragment
import com.rama.ecommerce.ui.fulfillment.detail.adapter.DetailAdapter
import com.rama.ecommerce.ui.fulfillment.detail.review.ReviewFragment
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.extension.getErrorSubtitle
import com.rama.ecommerce.utils.extension.getErrorTitle
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.showSnackbar
import com.rama.ecommerce.utils.extension.toCart
import com.rama.ecommerce.utils.extension.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.util.Locale

@AndroidEntryPoint
class DetailFragment : BaseFragment<FragmentDetailBinding>(FragmentDetailBinding::inflate) {

    private val viewModel: DetailViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupWishlist()
        setupAppbar()
        observeDetailProduct()
        setupAction()
    }

    private fun setupWishlist() {
        viewModel.isWishlist = viewModel.checkExistWishlist()
        setWishlistIcon()
    }

    private fun setWishlistIcon() {
        binding.includeContent.ivActionWishlist.setImageResource(
            if (viewModel.isWishlist) {
                R.drawable.ic_favorite_24
            } else {
                R.drawable.ic_favorite_border_24
            }
        )
    }

    private fun setupAction() {
        binding.includeContent.ivActionShare.setOnClickListener { actionShare() }
        binding.includeContent.ivActionWishlist.setOnClickListener { actionWishlist() }
        binding.includeContent.btnSeeAll.setOnClickListener { actionOpenReview() }
        binding.includeContent.btnBuy.setOnClickListener { actionCheckout() }
        binding.includeContent.btnToCart.setOnClickListener { actionAddCart() }
    }

    private fun actionCheckout() {
        firebaseAnalytics.sendLogButtonClicked("Detail: Buy Now")
        if (viewModel.detailProduct != null && viewModel.productVariant != null) {
            val data = viewModel.detailProduct!!.toCart(viewModel.productVariant!!)
            val bundle = Bundle().apply {
                putParcelableArrayList(CheckoutFragment.ARG_DATA, arrayListOf(data))
            }
            findNavController().navigate(R.id.action_detailFragment_to_checkoutFragment, bundle)
        }
    }

    private fun actionAddCart() {
        firebaseAnalytics.sendLogButtonClicked("Detail: Add Cart")
        if (viewModel.insertCart()) {
            sendLogWishlistOrCart(Event.ADD_TO_CART)
            binding.root.showSnackbar(getString(R.string.cart_insert_successful))
        } else {
            binding.root.showSnackbar(getString(R.string.stock_is_unavailable))
        }
    }

    private fun actionWishlist() {
        firebaseAnalytics.sendLogButtonClicked("Detail: Wishlist")
        if (viewModel.isWishlist) {
            viewModel.deleteWishlist()
            binding.root.showSnackbar(getString(R.string.wishlist_remove_successful))
        } else {
            viewModel.insertWishlist()
            sendLogWishlistOrCart(Event.ADD_TO_WISHLIST)
            binding.root.showSnackbar(getString(R.string.wishlist_insert_successful))
        }
        setWishlistIcon()
    }

    private fun sendLogWishlistOrCart(event: String) {
        firebaseAnalytics.logEvent(event) {
            val bundleProduct = Bundle().apply {
                putString(Param.ITEM_ID, viewModel.detailProduct!!.productId)
                putString(Param.ITEM_NAME, viewModel.detailProduct!!.productName)
                putString(Param.ITEM_BRAND, viewModel.detailProduct!!.brand)
                putString(Param.ITEM_VARIANT, viewModel.productVariant!!.variantName)
            }
            param(Param.ITEMS, arrayOf(bundleProduct))
        }
    }

    private fun actionOpenReview() {
        firebaseAnalytics.sendLogButtonClicked("Detail: Review")
        findNavController().navigate(
            R.id.action_detailFragment_to_reviewFragment,
            bundleOf(ReviewFragment.BUNDLE_PRODUCT_ID_KEY to viewModel.productId)
        )
    }

    private fun actionShare() {
        firebaseAnalytics.sendLogButtonClicked("Detail: Share")
        val sendIntent: Intent = Intent().apply {
            val text = """
                Name : ${viewModel.detailProduct?.productName}
                Price : ${viewModel.detailProduct?.productPrice?.toCurrencyFormat()}
                Link : https://ecommerce.rama.com/products/${viewModel.productId}
            """.trimIndent()
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, text)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }

    private fun setupAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.product_detail)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun observeDetailProduct() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.detailProductState.collect { result ->
                    if (result != null) {
                        showLoading(result is ResultState.Loading)
                        when (result) {
                            is ResultState.Loading -> {}
                            is ResultState.Success -> setData(result.data)
                            is ResultState.Error -> showErrorView(result.error)
                        }
                    }
                }
            }
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.includeContent.layoutContent.isVisible = !isLoading
        binding.loadingDetail.isVisible = isLoading
        binding.includeError.layoutError.isVisible = false
    }

    private fun showErrorView(error: Throwable) {
        val title = error.getErrorTitle(requireActivity())
        val subtitle = error.getErrorSubtitle(requireActivity())
        val action = getString(R.string.refresh_error_action)

        binding.includeContent.layoutContent.isVisible = false
        with(binding.includeError) {
            tvErrorTitle.text = title
            tvErrorSubtitle.text = subtitle
            btnErrorAction.text = action

            btnErrorAction.setOnClickListener { viewModel.getDetailProduct() }
            layoutError.isVisible = true
        }
    }

    private fun setData(data: DetailProduct) {
        sendLogViewItem(data)
        viewModel.detailProduct = data
        if (viewModel.productVariant == null) {
            viewModel.productVariant = data.productVariant[0]
        }

        setView(data)
    }

    private fun sendLogViewItem(data: DetailProduct) {
        firebaseAnalytics.logEvent(Event.VIEW_ITEM) {
            val bundleProduct = Bundle().apply {
                putString(Param.ITEM_ID, data.productId)
                putString(Param.ITEM_NAME, data.productName)
                putString(Param.ITEM_BRAND, data.brand)
            }
            param(Param.ITEMS, arrayOf(bundleProduct))
        }
    }

    private fun setView(data: DetailProduct) {
        with(binding.includeContent) {
            setImageView(data.image)
            tvDetailPrice.text =
                data.productPrice.plus(viewModel.productVariant!!.variantPrice).toCurrencyFormat()
            tvDetailName.text = data.productName
            tvDetailSale.text = getString(R.string.total_sales, data.sale)
            chipRating.text =
                getString(
                    R.string.detail_rating_and_total_rating,
                    data.productRating,
                    data.totalRating
                )
            setChipVariants(data.productVariant, data.productPrice)
            tvDetailDescription.text = data.description
            tvDetailSatisfaction.text =
                getString(R.string.text_satisfaction, data.totalSatisfaction)
            tvDetailRatingReview.text =
                getString(R.string.total_rating_and_review, data.totalRating, data.totalReview)
            tvDetailRating.text = String.format(Locale.getDefault(), "%.1f", data.productRating)
        }
    }

    private fun setImageView(image: List<String>) {
        val adapter = DetailAdapter()
        adapter.submitList(image)
        val pager = binding.includeContent.pagerDetail
        pager.adapter = adapter

        if (image.size > 1) {
            TabLayoutMediator(
                binding.includeContent.tabLayout,
                pager
            ) { _, _ -> }.attach()
        }
    }

    private fun setChipVariants(productVariant: List<DetailProduct.ProductVariant>, price: Int) {
        binding.includeContent.chipVariantGroup.removeAllViews()
        productVariant.forEach { variant ->
            val chip = Chip(binding.includeContent.chipVariantGroup.context)
            chip.setTextAppearanceResource(R.style.TextAppearance_Medium)
            chip.text = variant.variantName
            chip.isCheckable = true
            chip.isChecked = variant.variantName == viewModel.productVariant!!.variantName

            chip.setOnClickListener {
                binding.includeContent.tvDetailPrice.text =
                    price.plus(variant.variantPrice).toCurrencyFormat()
                viewModel.productVariant = variant
            }

            binding.includeContent.chipVariantGroup.addView(chip)
        }
    }

    companion object {
        const val BUNDLE_PRODUCT_ID_KEY = "product_id"
    }
}
