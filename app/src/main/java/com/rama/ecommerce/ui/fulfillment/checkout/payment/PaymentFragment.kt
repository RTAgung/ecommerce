package com.rama.ecommerce.ui.fulfillment.checkout.payment

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.divider.MaterialDividerItemDecoration
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import com.google.firebase.analytics.FirebaseAnalytics.Param
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.databinding.FragmentPaymentBinding
import com.rama.ecommerce.ui.fulfillment.checkout.payment.adapter.PaymentAdapter
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.Constant
import com.rama.ecommerce.utils.extension.getErrorSubtitle
import com.rama.ecommerce.utils.extension.getErrorTitle
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PaymentFragment : BaseFragment<FragmentPaymentBinding>(FragmentPaymentBinding::inflate) {

    private val viewModel: PaymentViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    private val paymentAdapter: PaymentAdapter by lazy {
        PaymentAdapter { paymentItem ->
            firebaseAnalytics.logEvent(Event.ADD_PAYMENT_INFO) {
                param(Param.PAYMENT_TYPE, paymentItem.label)
            }
            val bundle = Bundle().apply {
                putParcelable(BUNDLE_PAYMENT_KEY, paymentItem)
            }
            setFragmentResult(REQUEST_KEY, bundle)
            findNavController().navigateUp()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAppbar()
        setupRecyclerView()
        observePayment()
    }

    private fun setupRecyclerView() {
        val divider = MaterialDividerItemDecoration(requireActivity(), LinearLayoutManager.VERTICAL)
        divider.isLastItemDecorated = false
        divider.dividerThickness = Constant.DIVIDER_THICKNESS

        binding.rvPayment.apply {
            adapter = paymentAdapter
            layoutManager = LinearLayoutManager(requireActivity())
            addItemDecoration(divider)
        }
    }

    private fun observePayment() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.paymentState.collect { result ->
                    if (result != null) {
                        showLoading(result is ResultState.Loading)
                        when (result) {
                            is ResultState.Loading -> {}
                            is ResultState.Success -> paymentAdapter.submitList(result.data)
                            is ResultState.Error -> showErrorView(result.error)
                        }
                    }
                }
            }
        }
    }

    private fun showErrorView(error: Throwable) {
        val title = error.getErrorTitle(requireActivity())
        val subtitle = error.getErrorSubtitle(requireActivity())
        val action = getString(R.string.refresh_error_action)

        binding.rvPayment.isVisible = false
        with(binding.includeError) {
            tvErrorTitle.text = title
            tvErrorSubtitle.text = subtitle
            btnErrorAction.text = action

            btnErrorAction.setOnClickListener { viewModel.getPayment() }
            layoutError.isVisible = true
        }
    }

    private fun showLoading(isLoading: Boolean) {
        binding.rvPayment.isVisible = !isLoading
        binding.loadingPayment.isVisible = isLoading
        binding.includeError.layoutError.isVisible = false
    }

    private fun setupAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.product_detail)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    companion object {
        const val REQUEST_KEY = "payment_request_key"
        const val BUNDLE_PAYMENT_KEY = "payment_bundle_key"
    }
}
