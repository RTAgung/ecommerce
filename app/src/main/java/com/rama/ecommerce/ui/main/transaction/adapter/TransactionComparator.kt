package com.rama.ecommerce.ui.main.transaction.adapter

import androidx.recyclerview.widget.DiffUtil
import com.rama.ecommerce.data.model.Transaction

object TransactionComparator : DiffUtil.ItemCallback<Transaction>() {
    override fun areItemsTheSame(oldItem: Transaction, newItem: Transaction): Boolean =
        oldItem.invoiceId == newItem.invoiceId

    override fun areContentsTheSame(oldItem: Transaction, newItem: Transaction): Boolean =
        oldItem == newItem
}
