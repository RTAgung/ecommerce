package com.rama.ecommerce.ui.fulfillment.checkout

import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.view.isInvisible
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import com.google.firebase.analytics.FirebaseAnalytics.Param
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.logEvent
import com.google.firebase.ktx.Firebase
import com.rama.ecommerce.R
import com.rama.ecommerce.data.model.Cart
import com.rama.ecommerce.data.model.Fulfillment
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.databinding.FragmentCheckoutBinding
import com.rama.ecommerce.ui.fulfillment.cart.adapter.CartItemCallback
import com.rama.ecommerce.ui.fulfillment.checkout.adapter.CheckoutAdapter
import com.rama.ecommerce.ui.fulfillment.checkout.payment.PaymentFragment
import com.rama.ecommerce.ui.fulfillment.rating.RatingFragment
import com.rama.ecommerce.utils.BaseFragment
import com.rama.ecommerce.utils.extension.getErrorMessage
import com.rama.ecommerce.utils.extension.parcelable
import com.rama.ecommerce.utils.extension.sendLogButtonClicked
import com.rama.ecommerce.utils.extension.showSnackbar
import com.rama.ecommerce.utils.extension.toCurrencyFormat
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class CheckoutFragment : BaseFragment<FragmentCheckoutBinding>(FragmentCheckoutBinding::inflate) {

    private val viewModel: CheckoutViewModel by viewModels()

    private val firebaseAnalytics: FirebaseAnalytics by lazy {
        Firebase.analytics
    }

    private val checkoutAdapter: CheckoutAdapter by lazy {
        CheckoutAdapter(object : CartItemCallback {
            override fun addQuantityCallback(cart: Cart) {
                firebaseAnalytics.sendLogButtonClicked("Checkout: Add Quantity")
                viewModel.updateDataQuantity(cart, true)
            }

            override fun removeQuantityCallback(cart: Cart) {
                firebaseAnalytics.sendLogButtonClicked("Checkout: Remove Quantity")
                viewModel.updateDataQuantity(cart, false)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvCheckoutPrice.text = viewModel.totalPrice.toCurrencyFormat()
        setupAppbar()
        setupAdapter()
        setupAction()
        setPaymentView()
        observeData()
        observePayment()
    }

    private fun observePayment() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.paymentState.collect { result ->
                    if (result != null) {
                        showLoading(result is ResultState.Loading)
                        when (result) {
                            is ResultState.Loading -> {}
                            is ResultState.Success -> {
                                sendLogPurchase(result.data)
                                actionToRating(result.data)
                            }

                            is ResultState.Error -> {
                                val message = result.error.getErrorMessage()
                                binding.root.showSnackbar(message)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun sendLogPurchase(data: Fulfillment) {
        firebaseAnalytics.logEvent(Event.PURCHASE) {
            val listCart = runBlocking { viewModel.listData.first() }
            val bundle = ArrayList<Bundle>()
            listCart.map { cart ->
                val itemBundle = Bundle().apply {
                    putString(Param.ITEM_ID, cart.productId)
                    putString(Param.ITEM_NAME, cart.productName)
                    putString(Param.ITEM_BRAND, cart.brand)
                    putString(Param.ITEM_VARIANT, cart.variantName)
                }
                bundle.add(itemBundle)
            }
            param(Param.ITEMS, bundle.toTypedArray())
            param(Param.CURRENCY, "IDR")
            param(Param.VALUE, data.total.toLong())
            param(Param.START_DATE, data.date)
            param(Param.TRANSACTION_ID, data.invoiceId)
        }
    }

    private fun sendLogBeginCheckout(listCart: List<Cart>) {
        firebaseAnalytics.logEvent(Event.BEGIN_CHECKOUT) {
            val bundle = ArrayList<Bundle>()
            listCart.map { cart ->
                val itemBundle = Bundle().apply {
                    putString(Param.ITEM_ID, cart.productId)
                    putString(Param.ITEM_NAME, cart.productName)
                    putString(Param.ITEM_BRAND, cart.brand)
                    putString(Param.ITEM_VARIANT, cart.variantName)
                }
                bundle.add(itemBundle)
            }
            param(Param.ITEMS, bundle.toTypedArray())
            param(Param.CURRENCY, "IDR")
            param(Param.VALUE, viewModel.totalPrice.toLong())
        }
    }

    private fun actionToRating(data: Fulfillment) {
        val bundle = Bundle().apply {
            putParcelable(RatingFragment.DATA_BUNDLE_KEY, data)
        }
        findNavController().navigate(R.id.action_checkoutFragment_to_ratingFragment, bundle)
    }

    private fun showLoading(isLoading: Boolean) {
        with(binding) {
            btnBuy.isInvisible = isLoading
            loadingBuy.isInvisible = !isLoading
        }
    }

    private fun observeData() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.listData.collect { data ->
                    checkoutAdapter.submitList(data)
                    setPrice(data)
                    sendLogBeginCheckout(data)
                }
            }
        }
        setFragmentResultListener(PaymentFragment.REQUEST_KEY) { _, bundle ->
            viewModel.paymentItem =
                bundle.parcelable(PaymentFragment.BUNDLE_PAYMENT_KEY)
            setPaymentView()
        }
    }

    private fun setPaymentView() {
        if (viewModel.paymentItem != null) {
            binding.ivCheckoutPayment.load(viewModel.paymentItem!!.image) {
                crossfade(true)
                placeholder(R.drawable.image_placeholder)
                error(R.drawable.image_placeholder)
            }
            binding.tvCheckoutPayment.text = viewModel.paymentItem!!.label
            binding.btnBuy.isEnabled = true
        }
        binding.cardPayment.startAnimation(
            AnimationUtils.loadAnimation(
                requireActivity(),
                R.anim.anim_item_list
            )
        )
    }

    private fun setPrice(data: List<Cart>) {
        viewModel.totalPrice = data.sumOf {
            (it.productPrice + it.variantPrice) * it.quantity!!
        }
        binding.tvCheckoutPrice.text = viewModel.totalPrice.toCurrencyFormat()
    }

    private fun setupAction() {
        binding.cardPayment.setOnClickListener {
            findNavController().navigate(R.id.action_checkoutFragment_to_paymentFragment)
        }
        binding.btnBuy.setOnClickListener {
            firebaseAnalytics.sendLogButtonClicked("Checkout: Buy")
            viewModel.makePayment()
        }
    }

    private fun setupAdapter() {
        binding.rvCheckout.apply {
            adapter = checkoutAdapter
            layoutManager = object : LinearLayoutManager(requireActivity()) {
                override fun canScrollVertically(): Boolean {
                    return false
                }
            }
            itemAnimator = null
            startAnimation(
                AnimationUtils.loadAnimation(
                    requireActivity(),
                    R.anim.anim_item_list
                )
            )
        }
    }

    private fun setupAppbar() {
        val toolbar = binding.appBarLayout.toolbar
        toolbar.title = getString(R.string.checkout)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24)
        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    companion object {
        const val ARG_DATA = "arg_data"
    }
}
