package com.rama.ecommerce.ui.fulfillment.cart.adapter

import androidx.recyclerview.widget.DiffUtil
import com.rama.ecommerce.data.model.Cart

object CartComparator : DiffUtil.ItemCallback<Cart>() {
    override fun areItemsTheSame(oldItem: Cart, newItem: Cart): Boolean =
        oldItem.productId == newItem.productId

    override fun areContentsTheSame(oldItem: Cart, newItem: Cart): Boolean =
        oldItem == newItem
}
