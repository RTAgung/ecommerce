package com.rama.ecommerce.ui.fulfillment.detail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.DetailProduct
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.CartRepository
import com.rama.ecommerce.data.repository.StoreRepository
import com.rama.ecommerce.data.repository.WishlistRepository
import com.rama.ecommerce.utils.extension.toCart
import com.rama.ecommerce.utils.extension.toWishlist
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val storeRepository: StoreRepository,
    private val wishlistRepository: WishlistRepository,
    private val cartRepository: CartRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    val productId: String = savedStateHandle[DetailFragment.BUNDLE_PRODUCT_ID_KEY] ?: ""

    var isWishlist: Boolean = false
    var detailProduct: DetailProduct? = null
    var productVariant: DetailProduct.ProductVariant? = null

    private val _detailProductState = MutableStateFlow<ResultState<DetailProduct>?>(null)
    val detailProductState: StateFlow<ResultState<DetailProduct>?> = _detailProductState

    init {
        getDetailProduct()
    }

    fun getDetailProduct() {
        viewModelScope.launch {
            storeRepository.detailProducts(productId).collect { resultState ->
                _detailProductState.value = resultState
            }
        }
    }

    fun checkExistWishlist(): Boolean {
        return runBlocking {
            wishlistRepository.checkExistWishlistData(productId)
        }
    }

    fun insertWishlist() {
        if (detailProduct != null) {
            viewModelScope.launch {
                wishlistRepository.insertWishlist(detailProduct!!.toWishlist(productVariant!!))
            }
            isWishlist = true
        }
    }

    fun deleteWishlist() {
        if (detailProduct != null) {
            viewModelScope.launch {
                wishlistRepository.deleteWishlist(detailProduct!!.toWishlist(productVariant!!))
            }
            isWishlist = false
        }
    }

    fun insertCart(): Boolean {
        return if (detailProduct != null) {
            val cart = detailProduct!!.toCart(productVariant!!)
            val isStockReady = runBlocking { cartRepository.isStockReady(cart) }
            if (isStockReady) {
                viewModelScope.launch {
                    cartRepository.insertCart(cart)
                }
                true
            } else {
                false
            }
        } else {
            false
        }
    }
}
