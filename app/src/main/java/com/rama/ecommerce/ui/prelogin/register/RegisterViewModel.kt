package com.rama.ecommerce.ui.prelogin.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.PreloginRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val preloginRepository: PreloginRepository
) : ViewModel() {

    private val _registerState = MutableStateFlow<ResultState<Boolean>?>(null)
    val registerState: StateFlow<ResultState<Boolean>?> = _registerState

    fun makeRegister(email: String, password: String) {
        viewModelScope.launch {
            preloginRepository.register(email, password).collect { resultState ->
                _registerState.value = resultState
            }
        }
    }
}
