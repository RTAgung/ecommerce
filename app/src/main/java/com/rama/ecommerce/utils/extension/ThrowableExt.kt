package com.rama.ecommerce.utils.extension

import android.content.Context
import com.google.gson.Gson
import com.rama.ecommerce.R
import com.rama.ecommerce.data.source.remote.response.ErrorResponse
import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection

fun Throwable.getErrorMessage(): String {
    var message = this.message
    if (this is HttpException) {
        val errorResponse =
            Gson().fromJson(
                this.response()?.errorBody()?.string(),
                ErrorResponse::class.java
            ) ?: ErrorResponse()
        errorResponse.message?.let { message = it }
    }
    return message.toString()
}

fun Throwable.getErrorTitle(context: Context): String {
    return when (this) {
        is HttpException -> if (response()?.code() == HttpURLConnection.HTTP_NOT_FOUND) {
            context.getString(R.string.empty_error_title)
        } else {
            code().toString()
        }

        is IOException -> context.getString(R.string.connection_error_title)
        else -> context.getString(R.string.error)
    }
}

fun Throwable.getErrorSubtitle(context: Context): String {
    return when (this) {
        is HttpException -> if (response()?.code() == HttpURLConnection.HTTP_NOT_FOUND) {
            context.getString(R.string.empty_error_subtitle)
        } else {
            response()?.message().toString()
        }

        is IOException -> context.getString(R.string.connection_error_subtitle)
        else -> message.toString()
    }
}

fun Throwable.getErrorAction(context: Context): String {
    return when (this) {
        is HttpException -> if (response()?.code() == HttpURLConnection.HTTP_NOT_FOUND) {
            context.getString(R.string.reset_error_action)
        } else {
            context.getString(R.string.refresh_error_action)
        }

        else -> context.getString(R.string.refresh_error_action)
    }
}
