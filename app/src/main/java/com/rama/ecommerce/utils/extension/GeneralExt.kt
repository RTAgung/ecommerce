package com.rama.ecommerce.utils.extension

import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.TIRAMISU
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.rama.ecommerce.R
import com.rama.ecommerce.ui.prelogin.login.LoginFragment
import com.rama.ecommerce.ui.prelogin.onboarding.OnboardingFragment
import com.rama.ecommerce.ui.prelogin.register.RegisterFragment
import java.text.NumberFormat
import java.util.Locale

fun View.showSnackbar(message: String) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_SHORT)
    val textSnackbar =
        snackbar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
    textSnackbar.typeface = ResourcesCompat.getFont(
        this.context,
        R.font.poppins_regular
    )
    snackbar.show()
}

fun Number.toCurrencyFormat(lang: String = "in", country: String = "ID"): String {
    val localId = Locale(lang, country)
    val formatter = NumberFormat.getCurrencyInstance(localId)
    formatter.maximumFractionDigits = 0
    return formatter.format(this)
}

inline fun <reified T : Parcelable> Bundle.parcelable(key: String): T? = when {
    SDK_INT >= TIRAMISU -> getParcelable(key, T::class.java)
    else -> {
        @Suppress("DEPRECATION")
        getParcelable(key) as? T
    }
}

fun FirebaseAnalytics.sendLogButtonClicked(buttonName: String) {
    this.logEvent("button_click", bundleOf("button_name" to buttonName))
}

fun Fragment.isPreloginFragment(): Boolean {
    return this is OnboardingFragment || this is LoginFragment || this is RegisterFragment
}
