package com.rama.ecommerce.utils

object FormValidation {
    fun isEmailValid(text: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(text).matches()
    }

    fun isPasswordValid(text: String): Boolean {
        return text.length >= Constant.MINIMUM_PASSWORD
    }
}
