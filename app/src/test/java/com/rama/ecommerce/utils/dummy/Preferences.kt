package com.rama.ecommerce.utils.dummy

import com.rama.ecommerce.data.model.User

val dummyUserPreferences = User(
    "testName",
    "testImage.png",
    "testAccessToken",
    "testRefreshToken",
    0
)
