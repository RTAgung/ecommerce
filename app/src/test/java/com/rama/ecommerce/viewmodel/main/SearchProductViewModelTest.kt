package com.rama.ecommerce.viewmodel.main

import app.cash.turbine.test
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.StoreRepository
import com.rama.ecommerce.ui.main.store.search.SearchProductViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.response.dummySearchResponse
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class SearchProductViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: SearchProductViewModel
    private var storeRepository: StoreRepository = mock()

    @Before
    fun setup() {
        viewModel = SearchProductViewModel(
            storeRepository
        )
    }

    @Test
    fun getSearchData_thenReturnSearchData() = runTest {
        val query = "query"
        whenever(storeRepository.searchProducts(query))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(dummySearchResponse.data!!)
                )
            )

        viewModel.searchData.test {
            viewModel.getSearchData(query)
            assertEquals(ResultState.Loading, awaitItem())
            assertEquals(ResultState.Success(dummySearchResponse.data!!), awaitItem())
        }
    }
}
