package com.rama.ecommerce.viewmodel

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.repository.PreloginRepository
import com.rama.ecommerce.ui.MainViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class MainViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: MainViewModel
    private var preloginRepository: PreloginRepository = mock()

    @Before
    fun setup() {
        viewModel = MainViewModel(preloginRepository)
    }

    @Test
    fun checkUserAuthorization_thenReturnTrue() = runTest {
        whenever(preloginRepository.checkUserAuthorization())
            .thenReturn(flowOf(true))

        val actualData = viewModel.checkUserAuthorization().first()

        assertTrue(actualData)
    }

    @Test
    fun getAppTheme_thenReturnTrue() = runTest {
        whenever(preloginRepository.getAppTheme())
            .thenReturn(flowOf(true))

        val actualData = viewModel.getAppTheme().first()

        assertTrue(actualData)
    }

    @Test
    fun getAppLanguage_thenReturnIdLanguage() = runTest {
        whenever(preloginRepository.getAppLanguage())
            .thenReturn(flowOf("id"))

        val actualData = viewModel.getAppLanguage().first()

        assertEquals("id", actualData)
    }

    @Test
    fun makeLogout_thenVerify() = runTest {
        viewModel.makeLogout()
        verify(preloginRepository).logout()
    }
}
