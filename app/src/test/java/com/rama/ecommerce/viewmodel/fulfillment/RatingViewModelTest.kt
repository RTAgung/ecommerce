package com.rama.ecommerce.viewmodel.fulfillment

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.FulfillmentRepository
import com.rama.ecommerce.ui.fulfillment.rating.RatingFragment
import com.rama.ecommerce.ui.fulfillment.rating.RatingViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.response.dummyFulfillmentResponse
import com.rama.ecommerce.utils.extension.toFulfillment
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class RatingViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: RatingViewModel
    private var fulfillmentRepository: FulfillmentRepository = mock()
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(
            mapOf(
                RatingFragment.DATA_BUNDLE_KEY to dummyFulfillmentResponse.data!!.toFulfillment(),
                RatingFragment.RATING_BUNDLE_KEY to 5,
                RatingFragment.REVIEW_BUNDLE_KEY to "review"
            )
        )

    @Before
    fun setup() {
        viewModel = RatingViewModel(
            fulfillmentRepository, savedStateHandle
        )
    }

    @Test
    fun sendRatingSuccess_thenReturnTrue() = runTest {
        val invoiceId = "invoiceId"
        val rating = 5
        val review = "review"

        whenever(fulfillmentRepository.rating(invoiceId, rating, review))
            .thenReturn(flowOf(ResultState.Loading, ResultState.Success(true)))

        viewModel.sendRating(invoiceId, rating, review)
        advanceUntilIdle()

        assertEquals(
            ResultState.Success(true),
            viewModel.ratingState.value
        )
    }
}
