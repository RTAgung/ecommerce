package com.rama.ecommerce.viewmodel.prelogin

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.PreloginRepository
import com.rama.ecommerce.ui.prelogin.login.LoginViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class LoginViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: LoginViewModel
    private var preloginRepository: PreloginRepository = mock()

    @Before
    fun setup() {
        viewModel = LoginViewModel(preloginRepository)
    }

    @Test
    fun getFirstTimeRunApp_thenReturnTrue() = runTest {
        whenever(preloginRepository.getFirstTimeRunApp())
            .thenReturn(flowOf(true))

        val actualData = viewModel.getFirstTimeRunApp().first()

        assertTrue(actualData)
    }

    @Test
    fun makeLoginSuccess_thenReturnTrue() = runTest {
        val email = "email"
        val password = "password"
        whenever(preloginRepository.login(email, password))
            .thenReturn(flowOf(ResultState.Loading, ResultState.Success(true)))

        viewModel.makeLogin(email, password)
        advanceUntilIdle()

        assertEquals(ResultState.Success(true), viewModel.loginState.value)
    }
}
