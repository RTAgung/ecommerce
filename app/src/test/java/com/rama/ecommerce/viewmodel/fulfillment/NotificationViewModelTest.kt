package com.rama.ecommerce.viewmodel.fulfillment

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.repository.NotificationRepository
import com.rama.ecommerce.ui.fulfillment.notification.NotificationViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.dummyNotificationList
import com.rama.ecommerce.utils.extension.toNotification
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class NotificationViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: NotificationViewModel
    private var notificationRepository: NotificationRepository = mock()

    @Before
    fun setup() {
        viewModel = NotificationViewModel(
            notificationRepository
        )
    }

    @Test
    fun getNotificationData_thenReturnNotificationData() = runTest {
        whenever(notificationRepository.getNotificationData())
            .thenReturn(flowOf(dummyNotificationList.map { it.toNotification() }))

        val actualData = viewModel.getNotificationData().first()

        assertEquals(
            dummyNotificationList.map { it.toNotification() },
            actualData
        )
    }

    @Test
    fun readNotification_thenVerify() = runTest {
        val notification = dummyNotificationList[0].toNotification()

        viewModel.readNotification(notification)
        verify(notificationRepository).updateNotification(notification.copy(isRead = true))
    }
}
