package com.rama.ecommerce.viewmodel.fulfillment

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.StoreRepository
import com.rama.ecommerce.ui.fulfillment.detail.review.ReviewFragment
import com.rama.ecommerce.ui.fulfillment.detail.review.ReviewViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.response.dummyProductsReviewResponse
import com.rama.ecommerce.utils.extension.toReview
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ReviewViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: ReviewViewModel
    private var storeRepository: StoreRepository = mock()
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(mapOf(ReviewFragment.BUNDLE_PRODUCT_ID_KEY to "productId"))

    @Before
    fun setup() {
        whenever(storeRepository.reviewProducts("productId"))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(dummyProductsReviewResponse.data!!.map { it.toReview() })
                )
            )
        viewModel = ReviewViewModel(
            storeRepository, savedStateHandle
        )
    }

    @Test
    fun getListReview_thenReturnReviewData() = runTest {
        whenever(storeRepository.reviewProducts("productId"))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(dummyProductsReviewResponse.data!!.map { it.toReview() })
                )
            )

        viewModel.getListReview()
        advanceUntilIdle()

        assertEquals(
            ResultState.Success(dummyProductsReviewResponse.data!!.map { it.toReview() }),
            viewModel.reviewProductState.value
        )
    }
}
