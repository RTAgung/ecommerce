package com.rama.ecommerce.viewmodel.main

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.repository.CartRepository
import com.rama.ecommerce.data.repository.WishlistRepository
import com.rama.ecommerce.ui.main.wishlist.WishlistViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.dummyWishlistList
import com.rama.ecommerce.utils.extension.toCart
import com.rama.ecommerce.utils.extension.toWishlist
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class WishlistViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: WishlistViewModel
    private var cartRepository: CartRepository = mock()
    private var wishlistRepository: WishlistRepository = mock()

    @Before
    fun setup() {
        viewModel = WishlistViewModel(
            cartRepository, wishlistRepository
        )
    }

    @Test
    fun getWishlistData_thenReturnWishlistData() = runTest {
        whenever(wishlistRepository.getWishlistData())
            .thenReturn(flowOf(dummyWishlistList.map { it.toWishlist() }))

        val actualData = viewModel.getWishlistData().first()

        assertEquals(
            dummyWishlistList.map { it.toWishlist() },
            actualData
        )
    }

    @Test
    fun deleteWishlist_thenVerify() = runTest {
        viewModel.deleteWishlist(dummyWishlistList[0].toWishlist())
        verify(wishlistRepository).deleteWishlist(dummyWishlistList[0].toWishlist())
    }

    @Test
    fun insertCartSuccess_thenReturnTrue() = runTest {
        val wishlist = dummyWishlistList[0].toWishlist()
        val cart = wishlist.toCart()
        whenever(cartRepository.isStockReady(cart))
            .thenReturn(true)

        val actualData = viewModel.insertCart(wishlist)
        verify(cartRepository).insertCart(cart)

        assertTrue(actualData)
    }

    @Test
    fun insertCartFailed_thenReturnFalse() = runTest {
        val wishlist = dummyWishlistList[0].toWishlist()
        val cart = wishlist.toCart()
        whenever(cartRepository.isStockReady(cart))
            .thenReturn(false)

        val actualData = viewModel.insertCart(wishlist)

        assertFalse(actualData)
    }
}
