package com.rama.ecommerce.viewmodel.fulfillment

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.firebase.AppFirebaseRemoteConfig
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.ui.fulfillment.checkout.payment.PaymentViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.response.dummyPaymentList
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class PaymentViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: PaymentViewModel
    private var remoteConfig: AppFirebaseRemoteConfig = mock()

    @Before
    fun setup() {
        whenever(remoteConfig.fetchPaymentMethod())
            .thenReturn(flowOf(ResultState.Loading, ResultState.Success(dummyPaymentList)))
        viewModel = PaymentViewModel(
            remoteConfig
        )
    }

    @Test
    fun getPayment_thenReturnPaymentData() = runTest {
        whenever(remoteConfig.fetchPaymentMethod())
            .thenReturn(flowOf(ResultState.Loading, ResultState.Success(dummyPaymentList)))

        viewModel.getPayment()
        advanceUntilIdle()

        assertEquals(
            ResultState.Success(dummyPaymentList),
            viewModel.paymentState.value
        )
    }
}
