package com.rama.ecommerce.viewmodel.main

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.PreloginRepository
import com.rama.ecommerce.ui.main.profile.ProfileViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import okhttp3.RequestBody.Companion.toRequestBody
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProfileViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: ProfileViewModel
    private var preloginRepository: PreloginRepository = mock()

    @Before
    fun setup() {
        viewModel = ProfileViewModel(
            preloginRepository
        )
    }

    @Test
    fun uploadProfileSuccess_thenReturnTrue() = runTest {
        val userName = "name".toRequestBody()
        val userImage = null
        whenever(preloginRepository.uploadProfile(userName, userImage))
            .thenReturn(flowOf(ResultState.Loading, ResultState.Success(true)))

        viewModel.uploadProfile(userName, userImage)
        advanceUntilIdle()

        assertEquals(ResultState.Success(true), viewModel.uploadState.value)
    }
}
