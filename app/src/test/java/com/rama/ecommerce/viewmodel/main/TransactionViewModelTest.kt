package com.rama.ecommerce.viewmodel.main

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.FulfillmentRepository
import com.rama.ecommerce.ui.main.transaction.TransactionViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.response.dummyTransactionResponse
import com.rama.ecommerce.utils.extension.toTransaction
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class TransactionViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: TransactionViewModel
    private var fulfillmentRepository: FulfillmentRepository = mock()

    @Before
    fun setup() {
        whenever(fulfillmentRepository.getTransaction())
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(dummyTransactionResponse.data!!.map { it.toTransaction() })
                )
            )
        viewModel = TransactionViewModel(
            fulfillmentRepository
        )
    }

    @Test
    fun getListTransaction_thenReturnTransactionData() = runTest {
        whenever(fulfillmentRepository.getTransaction())
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(dummyTransactionResponse.data!!.map { it.toTransaction() })
                )
            )

        viewModel = TransactionViewModel(fulfillmentRepository)

        viewModel.getListTransaction()
        advanceUntilIdle()

        assertEquals(
            ResultState.Success(dummyTransactionResponse.data!!.map { it.toTransaction() }),
            viewModel.transactionState.value
        )
    }
}
