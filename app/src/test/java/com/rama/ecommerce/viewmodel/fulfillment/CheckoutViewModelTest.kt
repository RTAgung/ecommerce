package com.rama.ecommerce.viewmodel.fulfillment

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.Payment
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.FulfillmentRepository
import com.rama.ecommerce.ui.fulfillment.checkout.CheckoutFragment
import com.rama.ecommerce.ui.fulfillment.checkout.CheckoutViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.dummyCartList
import com.rama.ecommerce.utils.dummy.response.dummyFulfillmentResponse
import com.rama.ecommerce.utils.extension.toCart
import com.rama.ecommerce.utils.extension.toFulfillment
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class CheckoutViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: CheckoutViewModel
    private var fulfillmentRepository: FulfillmentRepository = mock()
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(mapOf(CheckoutFragment.ARG_DATA to dummyCartList.map { it.toCart() }))

    @Before
    fun setup() {
        viewModel = CheckoutViewModel(
            fulfillmentRepository, savedStateHandle
        )
    }

    @Test
    fun getCartData_thenReturnCartData() = runTest {
        val actualData = viewModel.listData.first()

        assertEquals(
            dummyCartList.map { it.toCart() },
            actualData
        )
    }

    @Test
    fun updateDataQuantity_withInsert_thenReturnUpdatedData() {
        var listCart = dummyCartList.map { it.toCart() }
        val cart = dummyCartList[0].toCart()

        viewModel.updateDataQuantity(cart, true)
        listCart = listCart.map {
            if (it == cart) {
                it.copy(quantity = it.quantity!!.plus(1))
            } else {
                it
            }
        }

        assertEquals(
            listCart,
            viewModel.listData.value
        )
    }

    @Test
    fun updateDataQuantity_withRemove_thenReturnUpdatedData() {
        var listCart = dummyCartList.map { it.toCart() }
        val cart = dummyCartList[0].toCart()

        viewModel.updateDataQuantity(cart, false)
        listCart = listCart.map {
            if (it == cart) {
                it.copy(quantity = it.quantity!!.plus(-1))
            } else {
                it
            }
        }

        assertEquals(
            listCart,
            viewModel.listData.value
        )
    }

    @Test
    fun makePaymentSuccess_thenReturnFulfillmentData() = runTest {
        val paymentItem = Payment.PaymentItem("image", "label", true)
        val data = dummyCartList.map { it.toCart() }
        viewModel.paymentItem = paymentItem

        whenever(fulfillmentRepository.fulfillment(paymentItem.label, data))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(dummyFulfillmentResponse.data!!.toFulfillment())
                )
            )

        viewModel.makePayment()
        advanceUntilIdle()

        assertEquals(
            ResultState.Success(dummyFulfillmentResponse.data!!.toFulfillment()),
            viewModel.paymentState.value
        )
    }
}
