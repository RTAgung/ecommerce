package com.rama.ecommerce.viewmodel.main

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.repository.PreloginRepository
import com.rama.ecommerce.ui.main.home.HomeViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class HomeViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: HomeViewModel
    private var preloginRepository: PreloginRepository = mock()

    @Before
    fun setup() {
        viewModel = HomeViewModel(
            preloginRepository
        )
    }

    @Test
    fun makeLogout_thenVerify() = runTest {
        viewModel.makeLogout()
        verify(preloginRepository).logout()
    }

    @Test
    fun getAppTheme_thenReturnTrue() = runTest {
        whenever(preloginRepository.getAppTheme())
            .thenReturn(flowOf(true))

        val actualData = viewModel.getAppTheme().first()

        assertTrue(actualData)
    }

    @Test
    fun setAppTheme_thenVerify() = runTest {
        viewModel.setAppTheme(true)
        verify(preloginRepository).setAppTheme(true)
    }

    @Test
    fun getAppLanguage_thenReturnTrue() = runTest {
        whenever(preloginRepository.getAppLanguage())
            .thenReturn(flowOf("id"))

        val actualData = viewModel.getAppLanguage().first()

        assertEquals("id", actualData)
    }

    @Test
    fun setAppLanguage_thenVerify() = runTest {
        viewModel.setAppLanguage(true)
        verify(preloginRepository).setAppLanguage("id")
    }
}
