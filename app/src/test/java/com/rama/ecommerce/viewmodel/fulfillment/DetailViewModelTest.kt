package com.rama.ecommerce.viewmodel.fulfillment

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.CartRepository
import com.rama.ecommerce.data.repository.StoreRepository
import com.rama.ecommerce.data.repository.WishlistRepository
import com.rama.ecommerce.ui.fulfillment.detail.DetailFragment
import com.rama.ecommerce.ui.fulfillment.detail.DetailViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.response.dummyProductsDetailResponse
import com.rama.ecommerce.utils.extension.toCart
import com.rama.ecommerce.utils.extension.toDetailProduct
import com.rama.ecommerce.utils.extension.toWishlist
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class DetailViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: DetailViewModel
    private var storeRepository: StoreRepository = mock()
    private var wishlistRepository: WishlistRepository = mock()
    private var cartRepository: CartRepository = mock()
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(mapOf(DetailFragment.BUNDLE_PRODUCT_ID_KEY to "productId"))

    @Before
    fun setup() {
        whenever(storeRepository.detailProducts("productId"))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(dummyProductsDetailResponse.data!!.toDetailProduct())
                )
            )
        viewModel = DetailViewModel(
            storeRepository, wishlistRepository, cartRepository, savedStateHandle
        )
    }

    @Test
    fun getDetailProduct_thenReturnDetailProduct() = runTest {
        whenever(storeRepository.detailProducts("productId"))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(dummyProductsDetailResponse.data!!.toDetailProduct())
                )
            )

        viewModel.getDetailProduct()
        advanceUntilIdle()

        assertEquals(
            ResultState.Success(dummyProductsDetailResponse.data!!.toDetailProduct()),
            viewModel.detailProductState.value
        )
    }

    @Test
    fun checkExistWishlist_thenReturnTrue() = runTest {
        whenever(wishlistRepository.checkExistWishlistData("productId"))
            .thenReturn(true)

        val actualData = viewModel.checkExistWishlist()

        assertTrue(actualData)
    }

    @Test
    fun insertWishlist_thenVerify() = runTest {
        val detailProduct = dummyProductsDetailResponse.data!!.toDetailProduct()
        val productVariant = detailProduct.productVariant[0]
        viewModel.detailProduct = detailProduct
        viewModel.productVariant = productVariant

        viewModel.insertWishlist()

        verify(wishlistRepository).insertWishlist(detailProduct.toWishlist(productVariant))
    }

    @Test
    fun deleteWishlist_thenVerify() = runTest {
        val detailProduct = dummyProductsDetailResponse.data!!.toDetailProduct()
        val productVariant = detailProduct.productVariant[0]
        viewModel.detailProduct = detailProduct
        viewModel.productVariant = productVariant

        viewModel.deleteWishlist()

        verify(wishlistRepository).deleteWishlist(detailProduct.toWishlist(productVariant))
    }

    @Test
    fun insertCart_thenReturnTrue() = runTest {
        val detailProduct = dummyProductsDetailResponse.data!!.toDetailProduct()
        val productVariant = detailProduct.productVariant[0]
        val cart = detailProduct.toCart(productVariant)
        viewModel.detailProduct = detailProduct
        viewModel.productVariant = productVariant

        whenever(cartRepository.isStockReady(cart)).thenReturn(true)

        viewModel.insertCart()

        verify(cartRepository).insertCart(cart)
    }
}
