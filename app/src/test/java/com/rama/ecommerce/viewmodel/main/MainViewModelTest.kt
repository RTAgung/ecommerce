package com.rama.ecommerce.viewmodel.main

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.User
import com.rama.ecommerce.data.repository.CartRepository
import com.rama.ecommerce.data.repository.NotificationRepository
import com.rama.ecommerce.data.repository.PreloginRepository
import com.rama.ecommerce.data.repository.WishlistRepository
import com.rama.ecommerce.ui.main.MainFragment
import com.rama.ecommerce.ui.main.MainViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.dummyCartList
import com.rama.ecommerce.utils.dummy.dummyNotificationList
import com.rama.ecommerce.utils.dummy.dummyUserPreferences
import com.rama.ecommerce.utils.dummy.dummyWishlistList
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class MainViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: MainViewModel
    private var preloginRepository: PreloginRepository = mock()
    private var wishlistRepository: WishlistRepository = mock()
    private var cartRepository: CartRepository = mock()
    private var notificationRepository: NotificationRepository = mock()
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(mapOf(MainFragment.MOVE_TRANSACTION_BUNDLE_KEY to false))

    @Before
    fun setup() {
        viewModel = MainViewModel(
            preloginRepository,
            wishlistRepository,
            cartRepository,
            notificationRepository,
            savedStateHandle
        )
    }

    @Test
    fun getUserDataSession_thenReturnUserData() = runTest {
        whenever(preloginRepository.getUserDataSession())
            .thenReturn(flowOf(dummyUserPreferences))

        val actualData = viewModel.user

        assertEquals(dummyUserPreferences, actualData)
    }

    @Test
    fun checkUserDataCompleted_thenReturnTrue() {
        whenever(preloginRepository.getUserDataSession())
            .thenReturn(flowOf(dummyUserPreferences))

        val actualData = viewModel.checkUserDataCompleted()

        assertTrue(actualData)
    }

    @Test
    fun checkUserHasLogin_thenReturnFalse() {
        whenever(preloginRepository.getUserDataSession())
            .thenReturn(flowOf(User()))

        val actualData = viewModel.checkUserHasLogin()

        assertFalse(actualData)
    }

    @Test
    fun getWishlistSize_thenReturn6() = runTest {
        whenever(wishlistRepository.getWishlistDataSize())
            .thenReturn(flowOf(dummyWishlistList.size))

        val actualData = viewModel.getWishlistSize().first()

        assertEquals(6, actualData)
    }

    @Test
    fun getCartSize_thenReturn6() = runTest {
        whenever(cartRepository.getCartDataSize())
            .thenReturn(flowOf(dummyCartList.size))

        val actualData = viewModel.getCartSize().first()

        assertEquals(6, actualData)
    }

    @Test
    fun getNotificationSize_thenReturn6() = runTest {
        whenever(notificationRepository.getNotificationDataSize())
            .thenReturn(flowOf(dummyNotificationList.size))

        val actualData = viewModel.getNotificationSize().first()

        assertEquals(6, actualData)
    }
}
