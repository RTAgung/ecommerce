package com.rama.ecommerce.viewmodel.fulfillment

import androidx.lifecycle.SavedStateHandle
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.CartRepository
import com.rama.ecommerce.data.repository.StoreRepository
import com.rama.ecommerce.data.repository.WishlistRepository
import com.rama.ecommerce.ui.fulfillment.detail.DetailComposeState
import com.rama.ecommerce.ui.fulfillment.detail.DetailComposeViewModel
import com.rama.ecommerce.ui.fulfillment.detail.DetailFragment
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.response.dummyProductsDetailResponse
import com.rama.ecommerce.utils.extension.toCart
import com.rama.ecommerce.utils.extension.toDetailProduct
import com.rama.ecommerce.utils.extension.toWishlist
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class DetailComposeViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: DetailComposeViewModel
    private var storeRepository: StoreRepository = mock()
    private var wishlistRepository: WishlistRepository = mock()
    private var cartRepository: CartRepository = mock()
    private var savedStateHandle: SavedStateHandle =
        SavedStateHandle(mapOf(DetailFragment.BUNDLE_PRODUCT_ID_KEY to "productId"))

    @Before
    fun setup() {
        whenever(storeRepository.detailProducts("productId"))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(dummyProductsDetailResponse.data!!.toDetailProduct())
                )
            )
        runBlocking {
            whenever(wishlistRepository.checkExistWishlistData(("productId")))
                .thenReturn(false)
        }
        viewModel = DetailComposeViewModel(
            storeRepository, wishlistRepository, cartRepository, savedStateHandle
        )
    }

    @Test
    fun getDetailProduct_thenReturnDetailProduct() = runTest {
        val dummy = dummyProductsDetailResponse.data!!.toDetailProduct()
        whenever(storeRepository.detailProducts("productId"))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(dummy)
                )
            )
        whenever(wishlistRepository.checkExistWishlistData(("productId")))
            .thenReturn(false)

        viewModel.getDetailProduct()
        advanceUntilIdle()

        val actualData = viewModel.composeState.value
        val expectedData = DetailComposeState(
            productId = "productId",
            isWishlist = false,
            detailProduct = dummy,
            productVariant = dummy.productVariant[0],
            resultState = ResultState.Success(dummy)
        )

        assertEquals(
            expectedData,
            actualData
        )
    }

    @Test
    fun insertWishlist_thenVerify() = runTest {
        val detailProduct = dummyProductsDetailResponse.data!!.toDetailProduct()
        val productVariant = detailProduct.productVariant[0]

        whenever(storeRepository.detailProducts("productId"))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(detailProduct)
                )
            )
        whenever(wishlistRepository.checkExistWishlistData(("productId")))
            .thenReturn(false)

        viewModel = DetailComposeViewModel(
            storeRepository, wishlistRepository, cartRepository, savedStateHandle
        )

        viewModel.insertWishlist()

        verify(wishlistRepository).insertWishlist(detailProduct.toWishlist(productVariant))
    }

    @Test
    fun deleteWishlist_thenVerify() = runTest {
        val detailProduct = dummyProductsDetailResponse.data!!.toDetailProduct()
        val productVariant = detailProduct.productVariant[0]

        whenever(storeRepository.detailProducts("productId"))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(detailProduct)
                )
            )
        whenever(wishlistRepository.checkExistWishlistData(("productId")))
            .thenReturn(false)

        viewModel = DetailComposeViewModel(
            storeRepository, wishlistRepository, cartRepository, savedStateHandle
        )

        viewModel.deleteWishlist()

        verify(wishlistRepository).deleteWishlist(detailProduct.toWishlist(productVariant))
    }

    @Test
    fun insertCart_thenReturnTrue() = runTest {
        val detailProduct = dummyProductsDetailResponse.data!!.toDetailProduct()
        val productVariant = detailProduct.productVariant[0]
        val cart = detailProduct.toCart(productVariant)

        whenever(storeRepository.detailProducts("productId"))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(detailProduct)
                )
            )
        whenever(wishlistRepository.checkExistWishlistData(("productId")))
            .thenReturn(false)

        viewModel = DetailComposeViewModel(
            storeRepository, wishlistRepository, cartRepository, savedStateHandle
        )

        whenever(cartRepository.isStockReady(cart)).thenReturn(true)
        viewModel.insertCart()

        verify(cartRepository).insertCart(cart)
    }

    @Test
    fun updateVariant_thenReturnUpdatedData() = runTest {
        val detailProduct = dummyProductsDetailResponse.data!!.toDetailProduct()

        whenever(storeRepository.detailProducts("productId"))
            .thenReturn(
                flowOf(
                    ResultState.Loading,
                    ResultState.Success(detailProduct)
                )
            )
        whenever(wishlistRepository.checkExistWishlistData(("productId")))
            .thenReturn(false)

        viewModel = DetailComposeViewModel(
            storeRepository, wishlistRepository, cartRepository, savedStateHandle
        )

        val productVariant = detailProduct.productVariant[1]
        viewModel.updateVariant(productVariant)

        val actualData = viewModel.composeState.value.productVariant

        assertEquals(productVariant, actualData)
    }
}
