package com.rama.ecommerce.viewmodel.prelogin

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.model.ResultState
import com.rama.ecommerce.data.repository.PreloginRepository
import com.rama.ecommerce.ui.prelogin.register.RegisterViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class RegisterViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: RegisterViewModel
    private var preloginRepository: PreloginRepository = mock()

    @Before
    fun setup() {
        viewModel = RegisterViewModel(preloginRepository)
    }

    @Test
    fun makeRegisterSuccess_thenReturnTrue() = runTest {
        val email = "email"
        val password = "password"
        whenever(preloginRepository.register(email, password))
            .thenReturn(flowOf(ResultState.Loading, ResultState.Success(true)))

        viewModel.makeRegister(email, password)
        advanceUntilIdle()

        assertEquals(ResultState.Success(true), viewModel.registerState.value)
    }
}
