package com.rama.ecommerce.viewmodel.prelogin

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.rama.ecommerce.data.repository.PreloginRepository
import com.rama.ecommerce.ui.prelogin.onboarding.OnboardingViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class OnboardingViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: OnboardingViewModel
    private var preloginRepository: PreloginRepository = mock()

    @Before
    fun setup() {
        viewModel = OnboardingViewModel(preloginRepository)
    }

    @Test
    fun setFirstTimeRunApp_thenVerify() = runTest {
        viewModel.setFirstTimeRunApp(false)
        verify(preloginRepository).setFirstTimeRunApp(false)
    }
}
