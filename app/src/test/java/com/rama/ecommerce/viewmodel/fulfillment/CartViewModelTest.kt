package com.rama.ecommerce.viewmodel.fulfillment

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rama.ecommerce.data.repository.CartRepository
import com.rama.ecommerce.ui.fulfillment.cart.CartViewModel
import com.rama.ecommerce.utils.MainDispatcherRule
import com.rama.ecommerce.utils.dummy.dummyCartList
import com.rama.ecommerce.utils.extension.toCart
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class CartViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var viewModel: CartViewModel
    private var cartRepository: CartRepository = mock()

    @Before
    fun setup() {
        viewModel = CartViewModel(
            cartRepository
        )
    }

    @Test
    fun getCartData_thenReturnCartData() = runTest {
        whenever(cartRepository.getCartData())
            .thenReturn(flowOf(dummyCartList.map { it.toCart() }))

        val actualData = viewModel.getCartData().first()

        assertEquals(
            dummyCartList.map { it.toCart() },
            actualData
        )
    }

    @Test
    fun deleteOneCart_thenVerify() = runTest {
        val cart = dummyCartList[0].toCart()
        viewModel.deleteCart(cart)
        verify(cartRepository).deleteCart(cart)
    }

    @Test
    fun deleteManyCart_thenVerify() = runTest {
        val listCheckedCart = dummyCartList.map { it.copy(isChecked = (true)).toCart() }
        whenever(cartRepository.getCartData())
            .thenReturn(flowOf(listCheckedCart))

        viewModel.deleteCart()

        listCheckedCart.map { cart ->
            verify(cartRepository).deleteCart(cart)
        }
    }

    @Test
    fun updateCartQuantity_whenInsertCart_thenVerify() = runTest {
        val cart = dummyCartList[0].toCart()
        val isInsert = true

        viewModel.updateCartQuantity(cart, isInsert)

        verify(cartRepository).updateCartQuantity(cart, isInsert)
    }

    @Test
    fun updateCartQuantity_whenRemoveCart_thenVerify() = runTest {
        val cart = dummyCartList[0].toCart()
        val isInsert = false

        viewModel.updateCartQuantity(cart, isInsert)

        verify(cartRepository).updateCartQuantity(cart, isInsert)
    }

    @Test
    fun updateManyCartChecked_thenVerify() = runTest {
        val isChecked = true
        val listCheckedCart = dummyCartList.map { it.toCart() }
        whenever(cartRepository.getCartData())
            .thenReturn(flowOf(listCheckedCart))

        viewModel.updateCartChecked(isChecked)

        listCheckedCart.map { cart ->
            verify(cartRepository).updateCartChecked(isChecked, cart)
        }
    }

    @Test
    fun updateOneCartChecked_thenVerify() = runTest {
        val cart = dummyCartList[0].toCart()
        val isChecked = true

        viewModel.updateCartChecked(isChecked, cart)
        verify(cartRepository).updateCartChecked(isChecked, cart)
    }
}
